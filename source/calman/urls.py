#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""moc URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.conf import global_settings

from calculation import urls as cproc_urls
from study import urls as study_urls
from reports import urls as reports_urls
from website import urls as website_urls


urlpatterns = [
    url(r'^', include(website_urls)),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^calculation/', include(cproc_urls)),
    url(r'^study/', include(study_urls)),
    url(r'^reports/', include(reports_urls)),
]

if global_settings.DEBUG:
    import debug_toolbar
    urlpatterns.append(url(r'^__debug__/', include(debug_toolbar.urls)))
