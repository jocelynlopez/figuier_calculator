#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django import forms


class TorsorWidget(forms.widgets.MultiWidget):

    def __init__(self, attrs=None):
        widgets = [
            forms.TextInput(attrs={"class": "form-control mech-torsor-input mech-torsor-FX",
                                   "placeholder": "Fx (%s)" % self.units[0],
                                   "id": "TO%s_FX" % self.numberID}),
            forms.TextInput(attrs={"class": "form-control mech-torsor-input mech-torsor-FY",
                                   "placeholder": "Fy (%s)" % self.units[0],
                                   "id": "TO%s_FY" % self.numberID}),
            forms.TextInput(attrs={"class": "form-control mech-torsor-input mech-torsor-FZ",
                                   "placeholder": "Fz (%s)" % self.units[0],
                                   "id": "TO%s_FZ" % self.numberID}),

            forms.TextInput(attrs={"class": "form-control mech-torsor-input mech-torsor-MX",
                                   "placeholder": "Mx (%s)" % self.units[1],
                                   "id": "TO%s_MX" % self.numberID}),
            forms.TextInput(attrs={"class": "form-control mech-torsor-input mech-torsor-MY",
                                   "placeholder": "My (%s)" % self.units[1],
                                   "id": "TO%s_MY" % self.numberID}),
            forms.TextInput(attrs={"class": "form-control mech-torsor-input mech-torsor-MZ",
                                   "placeholder": "Mz (%s)" % self.units[1],
                                   "id": "TO%s_MZ" % self.numberID}),

            forms.Select(attrs={"class": "form-control c-select mech-torsor-PT"}),
        ]
        super(TorsorWidget, self).__init__(widgets, attrs)

    def decompress(self, value):
        if value:
            return value.split(':')
        return [None, None, None, None, None, None, None]
