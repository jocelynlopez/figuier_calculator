
function UpdateOptionSelectCS()
{
  var nb_cs = $('.cs').length;
  var nb_point = $('.point').length;
  var selectsCSonPT = document.getElementsByClassName("mech-point-CS");
  for(var i = 0; i < nb_point ;i++){
    var nb_cs_option = selectsCSonPT[i].options.length;
    var activeOption = parseInt(selectsCSonPT[i].value,10);
    var max_cs = Math.max(nb_cs, nb_cs_option);
    for(var j = 0; j < max_cs ;j++){
      if (j+1 > nb_cs_option & j+1 <= nb_cs) {
        selectsCSonPT[i].options[j] = new Option('R'+(j+1), j+1, true, false);
      }
      else if (j+1 <= nb_cs_option & j+1 > nb_cs) {
        // selectsCSonPT[i].options[j].hidden = true;
        selectsCSonPT[i].remove(j);
      }
      else if (j+1 <= nb_cs_option & j+1 <= nb_cs) {
        // selectsCSonPT[i].options[j].hidden = false;
        selectsCSonPT[i].options[j] = new Option('R'+(j+1), j+1, true, false);
      }
    }
    if (activeOption > nb_cs){
      selectsCSonPT[i].value = "0";
    }
    else{
      selectsCSonPT[i].value = activeOption;
    }
  }
}

function UpdateOptionSelectPoint()
{
  var nb_point = $('.point').length;
  var nb_torsor = $('.torsor').length;
  var selectsPTonTorsor = document.getElementsByClassName("mech-torsor-PT");
  for(var i = 0; i < nb_torsor ;i++){
    var nb_pt_option = selectsPTonTorsor[i].options.length;
    var activeOption = parseInt(selectsPTonTorsor[i].value,10);
    var max_pt = Math.max(nb_point, nb_pt_option);
    for(var j = 0; j < max_pt ;j++){
      if (j+1 > nb_pt_option & j+1 <= nb_point) {
        selectsPTonTorsor[i].options[j] = new Option('O'+(j+1), j+1, true, false);
      }
      else if (j+1 <= nb_pt_option & j+1 > nb_point) {
        // selectsPTonTorsor[i].options[j].hidden = true;
        selectsPTonTorsor[i].remove(j);
      }
      else if (j+1 <= nb_pt_option & j+1 <= nb_point) {
        // selectsPTonTorsor[i].options[j].hidden = false;
        selectsPTonTorsor[i].options[j] = new Option('O'+(j+1), j+1, true, false);
      }
    }
    if (activeOption > nb_point){
      selectsPTonTorsor[i].value = "0";
    }
    else{
      selectsPTonTorsor[i].value = activeOption;
    }
  }
}

function MechanicalPointItemHtml(num)
{
  var html =    `<div class="custom-testimonials item point">`;
    html = html + `<div class="mech-point-container">`;
      html = html + `<span class="mech-point-name">${num}</span>`;
      html = html + `<input  class="form-control mech-point-input mech-point-X" placeholder="x (mm)" id="PT${num}_X" type="text" />`;
      html = html + `<input  class="form-control mech-point-input mech-point-Y" placeholder="y (mm)" id="PT${num}_X" type="text" />`;
      html = html + `<input  class="form-control mech-point-input mech-point-Z" placeholder="z (mm)" id="PT${num}_X" type="text" />`;
      html = html + `<select class="form-control c-select mech-point-CS" id="PT${num}_CS"></select>`;
    html = html + `</div>`;
  html = html + `</div>`;
  return html
}


function MechanicalTorsorItemHtml(num)
{
  var html =    `<div class="classic-testimonials item torsor">`;
    html = html + `<div class="mech-torsor-container">`;
      html = html + `<span class="mech-torsor-name">${num}</span>`;
      html = html + `<input  class="form-control mech-torsor-input mech-torsor-FX" placeholder="Fx (N)" id="TO${num}_FX" type="text" />`;
      html = html + `<input  class="form-control mech-torsor-input mech-torsor-MX" placeholder="Mx (N.m)" id="TO${num}_MX" type="text" />`;
      html = html + `<input  class="form-control mech-torsor-input mech-torsor-FY" placeholder="Fy (N)" id="TO${num}_FY" type="text" />`;
      html = html + `<input  class="form-control mech-torsor-input mech-torsor-MY" placeholder="My (N.m)" id="TO${num}_MY" type="text" />`;
      html = html + `<input  class="form-control mech-torsor-input mech-torsor-FZ" placeholder="Fz (N)" id="TO${num}_FZ" type="text" />`;
      html = html + `<input  class="form-control mech-torsor-input mech-torsor-MZ" placeholder="Mz (N.m)" id="TO${num}_MZ" type="text" />`;
      html = html + `<select class="form-control c-select mech-torsor-PT" id="TO${num}_PT"></select>`;
    html = html + `</div>`;
  html = html + `</div>`;
  return html
}

function MechanicalCSItemHtml(num)
{
  var html = `<div class="custom-testimonials item cs">`;
    html = html + `<div class="mech-cs-container">`;
      html = html + `<div class="mech-cs-name-x">${num}</div>`;
      html = html + `<div class="mech-cs-name-y">${num}</div>`;
      html = html + `<div class="mech-cs-name-z">${num}</div>`;
      html = html + `<input  class="form-control mech-cs-input mech-cs-XiX0" placeholder="X${num}.X0" id="CS${num}_XX" type="text" />`;
      html = html + `<input  class="form-control mech-cs-input mech-cs-YiX0" placeholder="Y${num}.X0" id="CS${num}_YX" type="text" />`;
      html = html + `<input  class="form-control mech-cs-input mech-cs-ZiX0" placeholder="Z${num}.X0" id="CS${num}_ZX" type="text" />`;
      html = html + `<input  class="form-control mech-cs-input mech-cs-XiY0" placeholder="X${num}.Y0" id="CS${num}_XY" type="text" />`;
      html = html + `<input  class="form-control mech-cs-input mech-cs-YiY0" placeholder="Y${num}.Y0" id="CS${num}_YY" type="text" />`;
      html = html + `<input  class="form-control mech-cs-input mech-cs-ZiY0" placeholder="Z${num}.Y0" id="CS${num}_ZY" type="text" />`;
      html = html + `<input  class="form-control mech-cs-input mech-cs-XiZ0" placeholder="X${num}.Z0" id="CS${num}_XZ" type="text" />`;
      html = html + `<input  class="form-control mech-cs-input mech-cs-YiZ0" placeholder="Y${num}.Z0" id="CS${num}_YZ" type="text" />`;
      html = html + `<input  class="form-control mech-cs-input mech-cs-ZiZ0" placeholder="Z${num}.Z0" id="CS${num}_ZZ" type="text" />`;
    html = html + `</div>`;
  html = html + `</div>`;
  return html
}

////------- Mechanics Carousel
$('.cs-carousel').each(function(){
  var owl = jQuery(this),
    itemsNum = $(this).attr('data-appeared-items'),
    sliderNavigation = $(this).attr('data-navigation');

  if ( sliderNavigation == 'false' || sliderNavigation == '0' ) {
    var returnSliderNavigation = false
  }else {
    var returnSliderNavigation = true
  }
  if( itemsNum == 1) {
    var deskitemsNum = 1;
    var desksmallitemsNum = 1;
    var tabletitemsNum = 1;
  }
  else if (itemsNum >= 2 && itemsNum < 4) {
    var deskitemsNum = itemsNum;
    var desksmallitemsNum = itemsNum - 1;
    var tabletitemsNum = itemsNum - 1;
  }
  else if (itemsNum >= 4 && itemsNum < 8) {
    var deskitemsNum = itemsNum -1;
    var desksmallitemsNum = itemsNum - 2;
    var tabletitemsNum = itemsNum - 3;
  }
  else {
    var deskitemsNum = itemsNum -3;
    var desksmallitemsNum = itemsNum - 6;
    var tabletitemsNum = itemsNum - 8;
  }
  owl.owlCarousel({
    slideSpeed : 300,
    stopOnHover: true,
    autoPlay: false,
    navigation : returnSliderNavigation,
    pagination: false,
    lazyLoad : true,
    items : itemsNum,
    itemsDesktop : [1000,deskitemsNum],
    itemsDesktopSmall : [900,desksmallitemsNum],
    itemsTablet: [600,tabletitemsNum],
    itemsMobile : false,
    transitionStyle : "goDown",
  });
});

$('.point-carousel').each(function(){
  var owl = jQuery(this),
    itemsNum = $(this).attr('data-appeared-items'),
    sliderNavigation = $(this).attr('data-navigation');

  if ( sliderNavigation == 'false' || sliderNavigation == '0' ) {
    var returnSliderNavigation = false
  }else {
    var returnSliderNavigation = true
  }
  if( itemsNum == 1) {
    var deskitemsNum = 1;
    var desksmallitemsNum = 1;
    var tabletitemsNum = 1;
  }
  else if (itemsNum >= 2 && itemsNum < 4) {
    var deskitemsNum = itemsNum;
    var desksmallitemsNum = itemsNum - 1;
    var tabletitemsNum = itemsNum - 1;
  }
  else if (itemsNum >= 4 && itemsNum < 8) {
    var deskitemsNum = itemsNum -1;
    var desksmallitemsNum = itemsNum - 2;
    var tabletitemsNum = itemsNum - 3;
  }
  else {
    var deskitemsNum = itemsNum -3;
    var desksmallitemsNum = itemsNum - 6;
    var tabletitemsNum = itemsNum - 8;
  }
  owl.owlCarousel({
    slideSpeed : 300,
    stopOnHover: true,
    autoPlay: false,
    navigation : returnSliderNavigation,
    pagination: false,
    lazyLoad : true,
    items : itemsNum,
    itemsDesktop : [1000,deskitemsNum],
    itemsDesktopSmall : [900,desksmallitemsNum],
    itemsTablet: [600,tabletitemsNum],
    itemsMobile : false,
    transitionStyle : "goDown",
  });
});

$('.torsor-carousel').each(function(){
  var owl = jQuery(this),
    itemsNum = $(this).attr('data-appeared-items'),
    sliderNavigation = $(this).attr('data-navigation');

  if ( sliderNavigation == 'false' || sliderNavigation == '0' ) {
    var returnSliderNavigation = false
  }else {
    var returnSliderNavigation = true
  }
  if( itemsNum == 1) {
    var deskitemsNum = 1;
    var desksmallitemsNum = 1;
    var tabletitemsNum = 1;
  }
  else if (itemsNum >= 2 && itemsNum < 4) {
    var deskitemsNum = itemsNum;
    var desksmallitemsNum = itemsNum - 1;
    var tabletitemsNum = itemsNum - 1;
  }
  else if (itemsNum >= 4 && itemsNum < 8) {
    var deskitemsNum = itemsNum -1;
    var desksmallitemsNum = itemsNum - 2;
    var tabletitemsNum = itemsNum - 3;
  }
  else {
    var deskitemsNum = itemsNum -3;
    var desksmallitemsNum = itemsNum - 6;
    var tabletitemsNum = itemsNum - 8;
  }
  owl.owlCarousel({
    slideSpeed : 300,
    stopOnHover: true,
    autoPlay: false,
    navigation : returnSliderNavigation,
    pagination: false,
    lazyLoad : true,
    items : itemsNum,
    itemsDesktop : [1000,deskitemsNum],
    itemsDesktopSmall : [900,desksmallitemsNum],
    itemsTablet: [1000,tabletitemsNum],
    itemsMobile : [1000,tabletitemsNum],
    transitionStyle : "goDown",
  });
});
