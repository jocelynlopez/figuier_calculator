#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django import forms
from mechanics.widgets import TorsorWidget


class TorsorField(forms.fields.MultiValueField):

    widget = TorsorWidget

    def __init__(self, fields=(), *args, **kwargs):
        name = kwargs.pop('name')
        ftype = kwargs.pop('type')
        desc = kwargs.pop('desc')
        units = kwargs.pop('units')
        numID = kwargs.pop('numberID')

        self.name = name
        self.type = ftype
        self.desc = desc
        self.units = units
        self.numberID = numID

        self.widget.name = name
        self.widget.type = ftype
        self.widget.desc = desc
        self.widget.units = units
        self.widget.numberID = numID

        super(TorsorField, self).__init__(fields, *args, **kwargs)
        fields = (
            forms.FloatField(),
            forms.FloatField(),
            forms.FloatField(),

            forms.FloatField(),
            forms.FloatField(),
            forms.FloatField(),

            forms.ChoiceField(),

        )

    def compress(self, data_list):
        return ':'.join(data_list)
