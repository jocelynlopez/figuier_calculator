#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np


class Vecteur(dict):

    def copy(self):
        return Vecteur(self.x, self.y, self.z)

    def __abs__(self):
        return Vecteur(abs(self.x), abs(self.y), abs(self.z))

    def __add__(a, b):
        return Vecteur(a.x + b.x, a.y + b.y, a.z + b.z)

    def __neg__(self):
        return Vecteur(-self.x, -self.y, -self.z)

    def __pos__(self):
        return self

    def __sub__(a, b):
        return Vecteur(a.x - b.x, a.y - b.y, a.z - b.z)

    def __mul__(a, b):
        if isinstance(b, Vecteur):
            return Vecteur(a.y * b.z - a.z * b.y,
                           a.z * b.x - a.x * b.z,
                           a.x * b.y - a.y * b.x)
        else:
            return Vecteur(a.x * b, a.y * b, a.z * b)

    def __rmul__(a, b):
        if isinstance(b, Vecteur):
            return Vecteur(a.y * b.z - a.z * b.y,
                           a.z * b.x - a.x * b.z,
                           a.x * b.y - a.y * b.x)
        else:
            return Vecteur(b * a.x, b * a.y, b * a.z)

    def __matmul__(a, b):
        return a.x * b.x + a.y * b.y + a.z * b.z

    def __str__(self):
        nb_max = max([len(str(int(x))) for x in self.values()]) + 2
        return "| %*.1f |\n| %*.1f |\n| %*.1f |" % \
            (nb_max, self.x, nb_max, self.y, nb_max, self.z)

    def reconstruct(d):
        return Vecteur(d["x"], d["y"], d["z"])

    def __init__(self, x, y, z):

        #  Fill vector
        self["x"] = x
        self["y"] = y
        self["z"] = z
        # Shortcuts
        self.x = x
        self.y = y
        self.z = z

    def getlist(self):
        return [self.x, self.y, self.z]

    def getarray(self):
        return np.array(self.getlist())


class CS(dict):

    def copy(self):
        return CS(self.orig, self.u, self.v, self.w)

    def __str__(self):
        return "| %3.3f %3.3f %3.3f |\n| %3.3f %3.3f %3.3f |\n| %3.3f %3.3f %3.3f |%s" %\
            (self.u.x, self.u.y, self.u.z,
             self.v.x, self.v.y, self.v.z,
             self.w.x, self.w.y, self.w.z,
             self.orig.getlist())

    def __init__(self, orig, u, v, w):

        if isinstance(orig, list) or isinstance(orig, tuple):
            self["orig"] = Vecteur(*orig)
            self.orig = Vecteur(*orig)
        else:
            self["orig"] = orig
            self.orig = orig

        if isinstance(u, list) or isinstance(u, tuple):
            self["u"] = Vecteur(*u)
            self.u = Vecteur(*u)
        else:
            self["u"] = u
            self.u = u

        if isinstance(v, list) or isinstance(v, tuple):
            self["v"] = Vecteur(*v)
            self.v = Vecteur(*v)
        else:
            self["v"] = v
            self.v = v

        if isinstance(w, list) or isinstance(w, tuple):
            self["w"] = Vecteur(*w)
            self.w = Vecteur(*w)
        else:
            self["w"] = w
            self.w = w

    def validate(self):
        # TODO : check normality and perpendicularity
        raise NotImplementedError

    def getlist(self):
        return [self.u.getlist(), self.v.getlist(), self.w.getlist()]

    def getarray(self):
        return np.array(self.getlist())


class Torseur(dict):

    def __abs__(self):
        return Torseur(abs(self.F), abs(self.M), self.cs)

    def __add__(a, b):
        b = b.change_cs(a.cs)
        F = Vecteur(a.F.x + b.F.x, a.F.y + b.F.y, a.F.z + b.F.z)
        M = Vecteur(a.M.x + b.M.x, a.M.y + b.M.y, a.M.z + b.M.z)
        return Torseur(F, M, a.cs)

    def __neg__(self):
        return Torseur(-self.F, -self.M, self.cs)

    def __pos__(self):
        return self

    def __sub__(a, b):
        return a + (-b)

    def __str__(self):
        nb_Fmax = max([len(str(int(x))) for x in self.F.values()]) + 2
        nb_Mmax = max([len(str(int(x))) for x in self.M.values()]) + 2
        s_cs = str(tuple(self.cs.orig.getlist()))
        return "| %*.1f   %*.1f |\n| %*.1f   %*.1f |\n| %*.1f   %*.1f |%s" %\
            (nb_Fmax, self["F"]["x"], nb_Mmax, self["M"]["x"], nb_Fmax, self["F"][
             "y"], nb_Mmax, self["M"]["y"], nb_Fmax, self["F"]["z"], nb_Mmax, self["M"]["z"], s_cs)

    def __init__(self, F, M, cs=None):
        #  Fill vector
        if isinstance(F, Vecteur):
            self["F"] = F
        else:
            self["F"] = Vecteur(**F)

        if isinstance(M, Vecteur):
            self["M"] = M
        else:
            self["M"] = Vecteur(**M)

        if cs is None:
            self["cs"] = CS(Vecteur(0, 0, 0),
                            Vecteur(1, 0, 0),
                            Vecteur(0, 1, 0),
                            Vecteur(0, 0, 1))
        else:
            self["cs"] = cs
        # Shortuts
        self.F = F
        self.M = M
        self.cs = self["cs"]

    def move_orig(self, orig):
        if isinstance(orig, list) or isinstance(orig, tuple):
            orig = Vecteur(*orig)
        M = self.M + (self.cs.orig - orig) * self.F
        cs = CS(orig, self.cs.u, self.cs.v, self.cs.w)
        return Torseur(self.F, M, cs)

    def change_cs(self, cs):
        newself = self.move_orig(cs.orig)

        oldcs = newself.cs.getarray()
        newcs = cs.getarray()

        oldF = newself.F.getarray()
        oldM = newself.M.getarray()

        F = Vecteur(*list((np.linalg.inv(oldcs) @ newcs) @ oldF))
        M = Vecteur(*list((np.linalg.inv(oldcs) @ newcs) @ oldM))
        return Torseur(F, M, cs)

    def reconstruct(d):
        return Torseur(d["F"], d["M"], d["cs"])
