#!/usr/bin/env python
# -*- coding: utf-8 -*-

import unittest
import json
from core.mechanics import Vecteur, CS, Torseur
import io


class VecteurTest(unittest.TestCase):
    """Set of tests for vector class representing a mathematical vector."""

    def setUp(self):
        """Create initial vector."""
        self.a = Vecteur(1, 6, 3)
        self.b = Vecteur(8, 0, 5)
        out = self.a.__str__()

    def test_copy(self):
        """Test vector copy."""
        self.assertDictEqual(self.a.copy(), self.a)

    def test_addition(self):
        """Test vector's addition and it's commutativity."""
        expect = Vecteur(9, 6, 8)
        self.assertDictEqual(self.a + self.b, expect)
        self.assertDictEqual(self.b + self.a, expect)

    def test_substraction(self):
        """Test vecteur's substraction from left and right."""
        expect = Vecteur(-7, 6, -2)
        self.assertDictEqual(self.a - self.b, expect)

    def test_negative(self):
        """Test a negative vector."""
        expect = Vecteur(-1, -6, -3)
        self.assertDictEqual(-self.a, expect)

    def test_positive(self):
        """Test a positive vector."""
        expect = Vecteur(1, 6, 3)
        self.assertDictEqual(+self.a, expect)

    def test_dot_product(self):
        """Test dot product and it's commutativity."""
        expect = 23
        self.assertEqual(self.a @ self.b, expect)
        self.assertEqual(self.b @ self.a, expect)

    def test_product(self):
        """Test vector product."""
        expect = Vecteur(30, 19, -48)
        self.assertDictEqual(self.a * self.b, expect)
        expect2 = Vecteur(2, 12, 6)
        self.assertDictEqual(self.a * 2, expect2)
        expect3 = Vecteur(-30, -19, 48)
        self.assertDictEqual(self.b * self.a, expect3)

    def test_commutativity(self):
        """Test vector product."""
        self.assertDictEqual(2 * self.a, self.a * 2)

    def test_JSON_de_serialization(self):
        """Test serialization and deserialization in JSON format."""
        expect = {"x": 1, "y": 6, "z": 3}
        fp = io.StringIO()
        json.dump(self.a, fp)
        deserialized = json.loads(fp.getvalue())
        self.assertDictEqual(expect, deserialized)

    def test_reconstruction(self):
        """Test reconstruction of the vector with associated dictionnary."""
        # reconstructed = Vecteur(dict(self.a))
        reconstructed = Vecteur.reconstruct(dict(self.a))
        self.assertDictEqual(self.a, reconstructed)


class CSTest(unittest.TestCase):
    """Set of tests for CS class representing mechanical coordinate system."""

    def setUp(self):
        """Create initial CS."""
        cs1_orig = (0, 0, 0)
        cs1_u = (1, 0, 0)
        cs1_v = (0, 1, 0)
        cs1_w = (0, 0, 1)

        cs2_orig = (1, 8, 4)
        cs2_u = (0, -1, 0)
        cs2_v = (0, 0, -1)
        cs2_w = (1, 0, 0)

        self.cs1 = CS(cs1_orig, cs1_u, cs1_v, cs1_w)
        self.cs2 = CS(cs2_orig, cs2_u, cs2_v, cs2_w)
        self.cs3 = CS(cs1_orig, cs2_u, cs2_v, cs2_w)
        self.cs4 = CS(cs2_orig, cs1_u, cs1_v, cs1_w)
        out = self.cs1.__str__()

    def test_copy(self):
        """Test vector copy."""
        self.assertDictEqual(self.cs1.copy(), self.cs1)


class TorseurTest(unittest.TestCase):
    """Set of tests for Torseur class representing mechanical torseur."""

    def setUp(self):
        """Create initial Torseur."""
        self.Fa = Vecteur(1, 6, 3)
        self.Ma = Vecteur(-3, 1, 8)

        self.Fb = Vecteur(8, 0, 5)
        self.Mb = Vecteur(2, 0, 5)

        cs1_orig = (0, 0, 0)
        cs1_u = (1, 0, 0)
        cs1_v = (0, 1, 0)
        cs1_w = (0, 0, 1)

        cs2_orig = (1, 8, 4)
        cs2_u = (0, -1, 0)
        cs2_v = (0, 0, -1)
        cs2_w = (1, 0, 0)

        self.cs1 = CS(cs1_orig, cs1_u, cs1_v, cs1_w)
        self.cs2 = CS(cs2_orig, cs2_u, cs2_v, cs2_w)
        self.cs3 = CS(cs1_orig, cs2_u, cs2_v, cs2_w)
        self.cs4 = CS(cs2_orig, cs1_u, cs1_v, cs1_w)

        self.a1 = Torseur(self.Fa, self.Ma, self.cs1)
        self.a2 = Torseur(self.Fa, self.Ma, self.cs2)
        self.a3 = Torseur(self.Fa, self.Ma, self.cs3)
        self.a4 = Torseur(self.Fa, self.Ma, self.cs4)

        self.b1 = Torseur(self.Fb, self.Mb, self.cs1)
        self.b2 = Torseur(self.Fb, self.Mb, self.cs2)
        self.b3 = Torseur(self.Fb, self.Mb, self.cs3)
        self.b4 = Torseur(self.Fb, self.Mb, self.cs4)
        out = self.a1.__str__()

    def test_init(self):
        Fa = {'x': 1, 'y': 6, 'z': 3}
        Ma = {'x': -3, 'y': 1, 'z': 8}
        a1_alternative = Torseur(Fa, Ma)
        self.assertDictEqual(self.a1, a1_alternative)

    def test_abs(self):
        F = Vecteur(1, 6, 3)
        M = Vecteur(3, 1, 8)
        expect = Torseur(F, M, self.cs1)
        self.assertDictEqual(abs(self.a1), expect)

    def test_negative(self):
        """Test a negative torsor."""
        F = Vecteur(-1, -6, -3)
        M = Vecteur(3, -1, -8)
        expect = Torseur(F, M, self.cs1)
        self.assertDictEqual(-self.a1, expect)

    def test_negative(self):
        """Test a negative torsor."""
        F = Vecteur(1, 6, 3)
        M = Vecteur(-3, 1, 8)
        expect = Torseur(F, M, self.cs1)
        self.assertDictEqual(+self.a1, expect)

    def test_addition_same_cs(self):
        """Test Torseur's addition with same cs."""
        F = Vecteur(9, 6, 8)
        M = Vecteur(-1, 1, 13)
        expect = Torseur(F, M, self.cs1)
        self.assertDictEqual(self.a1 + self.b1, expect)

    def test_substraction_same_cs(self):
        """Test Torseur's substraction with same cs."""
        F = Vecteur(-7, 6, -2)
        M = Vecteur(-5, 1, 3)
        expect = Torseur(F, M, self.cs1)
        self.assertDictEqual(+self.a1 - self.b1, expect)

    def test_addition_diff_cs_orig(self):
        """Test Torseur's addition with different cs origin."""
        F = Vecteur(9, 6, 8)
        M = Vecteur(39, 28, -51)
        expect = Torseur(F, M, self.cs1)
        self.assertDictEqual(self.a1 + self.b4, expect)

    def test_substraction_diff_cs_orig(self):
        """Test Torseur's addition with different cs origin."""
        F = Vecteur(-7, 6, -2)
        M = Vecteur(-45, -26, 67)
        expect = Torseur(F, M, self.cs1)
        self.assertDictEqual(self.a1 - self.b4, expect)

    def test_move_orig(self):
        b4_moved = self.b4.move_orig(self.cs1.orig)
        b4bis_moved = self.b4.move_orig((0, 0, 0))
        F = Vecteur(8, 0, 5)
        M = Vecteur(42, 27, -59)
        expect = Torseur(F, M, self.cs1)
        self.assertDictEqual(b4_moved, expect)
        self.assertDictEqual(b4bis_moved, expect)

    def test_change_cs(self):
        F = Vecteur(3, -1, -6)
        M = Vecteur(8, 3, -1)
        expect = Torseur(F, M, self.cs1)
        a3_changed = self.a3.change_cs(self.cs1)
        self.assertDictEqual(a3_changed, expect)

    def test_addition_diff_cs(self):
        """Test Torseur's addition with different cs."""
        F = Vecteur(6, -2, 3)
        M = Vecteur(-62, -41, -19)
        expect = Torseur(F, M, self.cs1)
        self.assertDictEqual(self.a1 + self.b2, expect)

    def test_substraction_diff_cs(self):
        """Test Torseur's addition with different cs."""
        F = Vecteur(-4, 14, 3)
        M = Vecteur(56, 43, 35)
        expect = Torseur(F, M, self.cs1)
        self.assertDictEqual(self.a1 - self.b2, expect)

    def test_JSON_de_serialization(self):
        """Test serialization and deserialization in JSON format."""
        expect = self.a1
        fp = io.StringIO()
        json.dump(self.a1, fp)
        deserialized = json.loads(fp.getvalue())
        self.assertDictEqual(expect, deserialized)

    def test_reconstruction(self):
        """Test reconstruction of the torseur with associated dictionnary."""
        reconstructed = Torseur.reconstruct(dict(self.a1))
        self.assertDictEqual(self.a1, reconstructed)

if __name__ == '__main__':
    unittest.main()
