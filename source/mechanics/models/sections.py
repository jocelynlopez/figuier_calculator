#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import models
from django.utils.translation import ugettext as _
from django.core.validators import MinValueValidator, MaxValueValidator


class SectionShapeType(models.Model):
    """Section Shape type."""

    class SectionShapeTypeManager(models.Manager):

        def get_by_natural_key(self, SHAPE_TYPE):
            return self.get(SHAPE_TYPE=SHAPE_TYPE)

    objects = SectionShapeTypeManager()

    SHAPE_TYPE = models.IntegerField(null=False, blank=False,
                                     default=None, choices=None,
                                     error_messages=None,
                                     help_text=None,
                                     verbose_name=None,
                                     validators=[MinValueValidator(1), MinValueValidator(201)],
                                     unique=True)

    PDL_SCRIPT = models.TextField(null=False, blank=True,
                                  default='', choices=None,
                                  error_messages=None,
                                  help_text=None,
                                  verbose_name=None)

    IS_SYMMETRIC = models.BooleanField(null=False, blank=False,
                                       default=None,
                                       error_messages=None,
                                       help_text=None,
                                       verbose_name=None)

    def natural_key(self):
        return (self.SHAPE_TYPE,)

    def __str__(self):
        return self.NUMBER_ID


class SectionCategory(models.Model):

    class SectionCategoryManager(models.Manager):

        def get_by_natural_key(self, title):
            return self.get(title=title)

    objects = SectionCategoryManager()

    def natural_key(self):
        return (self.title,)

    title = models.CharField(_('title'), max_length=250, unique=True)
    parent = models.ForeignKey('self', blank=True, null=True, related_name='child',
                               on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = "Section's categories"
        ordering = ['title']

    def __str__(self):
        p_list = self._recurse_for_parents(self)
        p_list.append(self.title)
        return self.get_separator().join(p_list)

    def _recurse_for_parents(self, cat_obj):
        p_list = []
        if cat_obj.parent_id:
            p = cat_obj.parent
            p_list.append(p.title)
            more = self._recurse_for_parents(p)
            p_list.extend(more)
        if cat_obj == self and p_list:
            p_list.reverse()
        return p_list

    def get_separator(self):
        return ' :: '


class SectionCatalog(models.Model):
    """Section Catalog."""

    class SectionCatalogManager(models.Manager):

        def get_by_natural_key(self, TITLE):
            return self.get(TITLE=TITLE)

    objects = SectionCatalogManager()

    TITLE = models.CharField(max_length=250,
                             null=False, blank=False,
                             default=None, choices=None,
                             error_messages=None,
                             help_text=_("Nom du catalogue"),
                             verbose_name=None,
                             unique=True)

    DESCRIPTION = models.CharField(max_length=250,
                                   null=False, blank=False,
                                   default=None, choices=None,
                                   error_messages=None,
                                   help_text=_("Description du catalogue"),
                                   verbose_name=None)

    LOCATION = models.CharField(max_length=250,
                                null=False, blank=False,
                                default=None, choices=None,
                                error_messages=None,
                                help_text=_("Lieu d'application du catalogue"),
                                verbose_name=None)

    CATEGORY = models.ForeignKey(SectionCategory)

    def natural_key(self):
        return (self.TITLE,)

    def __str__(self):
        return self.TITLE


class SectionShape(models.Model):
    """Section shape"""

    class SectionShapeManager(models.Manager):

        def get_by_natural_key(self, TITLE):
            return self.get(TITLE=TITLE)

    objects = SectionShapeManager()

    TITLE = models.CharField(max_length=250,
                             null=False, blank=False,
                             default=None, choices=None,
                             error_messages=None,
                             help_text=_("Nom de la forme de section"),
                             verbose_name=None,
                             unique=True)

    DESCRIPTION = models.CharField(max_length=250,
                                   null=False, blank=False,
                                   default=None, choices=None,
                                   error_messages=None,
                                   help_text=_("Description de la forme de section"),
                                   verbose_name=None)

    SHAPE_TYPE = models.ForeignKey(SectionShapeType)
    CATALOG = models.ManyToManyField(SectionCatalog)

    def natural_key(self):
        return (self.TITLE,)

    def __str__(self):
        return self.TITLE


HELP_TEXT = {
    'DIM1': _("Premier composant numérique du nom de la section"),
    'DIM2': _("Deuxième composant numérique du nom de la section"),
    'DIM3': _("Troisième composant numérique du nom de la section"),
    'MASS': _("Masse de l'unité de longueur du profilé [kg/m]"),
    'SURF': _("Surface de peinture (périmètre de la barre) [m]"),
    'H': _("Hauteur de la section [m]"),
    'B': _("Largeur de la section [m]"),
    'EA': _("Epaisseur de l'âme [m]"),
    'ES': _("Epaisseur de l'aile [m]"),
    'RA': _("Rayon de l'arrondissement (âme) [m]"),
    'RS': _("Rayon de l'arrondissement (aile) [m]"),
    'GAP': _("Distance entre les profilés dans les sections complexes [m]"),
    'SX': _("Aire de section transversale [m^2]"),
    'SY': _("Aire de cisaillement en Y réduite de la section transversale pour calculer \
        les déformations angulaires moyennes XY [m^2]"),
    'SZ': _("Aire de cisaillement en Z réduite de la section transversale pour calculer \
        les déformations angulaires moyennes XZ [m^2]"),
    'IX': _("Moment d'inertie en torsion [m^4]"),
    'IY': _("Moment d'inertie en flexion par rapport à Y [m^4]"),
    'IZ': _("Moment d'inertie en flexion par rapport à Z [m^4]"),
    'IOMEGA': _("Moment d'inertie sectoriel (pour les parois minces)"),
    'VY': _("Distance extrême Z du centre de gravité dans le sens positif de l'axe Y [m]"),
    'VPY': _("Distance extrême Z du centre de gravité dans le sens négatif de l'axe Y [m]"),
    'VZ': _("Distance extrême Y du centre de gravité dans le sens positif de l'axe Z [m]"),
    'VPZ': _("Distance extrême Y du centre de gravité dans le sens négatif de l'axe Z [m]"),
    'MSY': _("Facteur de résistance plastique à la flexion par rapport à l'axe Y"),
    'MSZ': _("Facteur de résistance plastique à la flexion par rapport à l'axe Z"),
    'TORS': _("Facteur de résistance en torsion"),
    'SHEAR_Qy': _("Facteur de résistance au cisaillement (Qy)"),
    'SHEAR_Qz': _("Facteur de résistance au cisaillement (Qz)"),
    'GAMMA': _("Angle de rotation du repère local par rapport au repère global (moments d'inertie max de la section)"),
    'SYMBOL': _("Description vectorielle de la forme de la section"),
    'A_1': _("Angle additionnel"),
    'A_2': _("Angle additionnel"),
    'HAS_MATS': _("Indicateur de la présence des matériaux affectés au profilé"),
    'MATERIALS': _("Liste des matériaux affectés au profilé"),
    'POINTS': _("Liste des points caractéristiques dans la section"),
    'IS_THIN': _("Contrôle utilisé pour signaler les profilés à parois minces"),
    'B_2': _("AISC k design // Largeur de la section - deuxième aile (I asymétrique)"),
    'ES_2': _("AISC k detailng // Largeur de la deuxième aile (I asymétrique)"),
    'P1_L': _("AISC T // Longueur de la dalle P1 (section en croix)"),
    'P1_T': _("Epaisseur de la dalle P1 (section en croix)"),
    'P2_L': _("Longueur de la dalle P2 (section en croix)"),
    'P2_T': _("Epaisseur de la dalle P2 (section en croix)"),
    'P3_L': _("Longueur de la dalle P3 (section en croix)"),
    'P3_T': _("Epaisseur de la dalle P3 (section en croix)"),
    'P4_L': _("Longueur de la dalle P4 (section en croix)"),
    'P4_T': _("Epaisseur de la dalle P4 (section en croix)"),
    'NAME': _("Nom de la section"),
    'NAME_REVIT': None,
    'IYn': None,
    'IYb': None,
}

ERROR_MESSAGES = {
    'DEFAULT': {'null': None, 'blank': None, 'invalid': None, 'invalid_choice': None, 'unique': None,
                'unique_for_date': None},
}


class Section(models.Model):
    """Section 1D."""

    class Meta:
        unique_together = ("CATALOG", "NAME")

    def __str__(self):
        return self.NAME

    NAME = models.CharField(max_length=250,
                            null=False, blank=False,
                            default=None, choices=None,
                            error_messages=None,
                            help_text=HELP_TEXT['NAME'],
                            verbose_name=None)

    NAME_REVIT = models.CharField(max_length=250,
                                  null=False, blank=False,
                                  default=None, choices=None,
                                  error_messages=None,
                                  help_text=HELP_TEXT['NAME_REVIT'],
                                  verbose_name=None)

    CATEGORY = models.ForeignKey(SectionCategory)
    SHAPE = models.ForeignKey(SectionShape)
    CATALOG = models.ForeignKey(SectionCatalog)

    DIM1 = models.IntegerField(null=False, blank=False,
                               default=None, choices=None,
                               error_messages=None,
                               help_text=HELP_TEXT['DIM1'],
                               verbose_name=None)

    DIM2 = models.IntegerField(null=True, blank=True,
                               default=None, choices=None,
                               error_messages=None,
                               help_text=HELP_TEXT['DIM2'],
                               verbose_name=None)

    DIM3 = models.IntegerField(null=True, blank=True,
                               default=None, choices=None,
                               error_messages=None,
                               help_text=HELP_TEXT['DIM3'],
                               verbose_name=None)

    MASS = models.FloatField(null=True, blank=True,
                             default=None, choices=None,
                             error_messages=None,
                             help_text=HELP_TEXT['MASS'],
                             verbose_name=None,
                             validators=[MinValueValidator(0)])

    SURF = models.FloatField(null=True, blank=True,
                             default=None, choices=None,
                             error_messages=None,
                             help_text=HELP_TEXT['SURF'],
                             verbose_name=None,
                             validators=[MinValueValidator(0)])

    H = models.FloatField(null=False, blank=False,
                          default=None, choices=None,
                          error_messages=None,
                          help_text=HELP_TEXT['H'],
                          verbose_name=None,
                          validators=[MinValueValidator(0)])

    B = models.FloatField(null=False, blank=False,
                          default=None, choices=None,
                          error_messages=None,
                          help_text=HELP_TEXT['B'],
                          verbose_name=None,
                          validators=[MinValueValidator(0)])

    EA = models.FloatField(null=False, blank=False,
                           default=None, choices=None,
                           error_messages=None,
                           help_text=HELP_TEXT['EA'],
                           verbose_name=None,
                           validators=[MinValueValidator(0)])

    ES = models.FloatField(null=True, blank=True,
                           default=None, choices=None,
                           error_messages=None,
                           help_text=HELP_TEXT['ES'],
                           verbose_name=None,
                           validators=[MinValueValidator(0)])

    RA = models.FloatField(null=True, blank=True,
                           default=None, choices=None,
                           error_messages=None,
                           help_text=HELP_TEXT['RA'],
                           verbose_name=None,
                           validators=[MinValueValidator(0)])

    RS = models.FloatField(null=True, blank=True,
                           default=None, choices=None,
                           error_messages=None,
                           help_text=HELP_TEXT['RS'],
                           verbose_name=None,
                           validators=[MinValueValidator(0)])

    GAP = models.FloatField(null=True, blank=True,
                            default=None, choices=None,
                            error_messages=None,
                            help_text=HELP_TEXT['GAP'],
                            verbose_name=None,
                            validators=[MinValueValidator(0)])

    SX = models.FloatField(null=False, blank=False,
                           default=None, choices=None,
                           error_messages=None,
                           help_text=HELP_TEXT['SX'],
                           verbose_name=None,
                           validators=[MinValueValidator(0)])

    SY = models.FloatField(null=True, blank=True,
                           default=None, choices=None,
                           error_messages=None,
                           help_text=HELP_TEXT['SY'],
                           verbose_name=None,
                           validators=[MinValueValidator(0)])

    SZ = models.FloatField(null=True, blank=True,
                           default=None, choices=None,
                           error_messages=None,
                           help_text=HELP_TEXT['SZ'],
                           verbose_name=None,
                           validators=[MinValueValidator(0)])

    IX = models.FloatField(null=False, blank=False,
                           default=None, choices=None,
                           error_messages=None,
                           help_text=HELP_TEXT['IX'],
                           verbose_name=None,
                           validators=[MinValueValidator(0)])

    IY = models.FloatField(null=False, blank=False,
                           default=None, choices=None,
                           error_messages=None,
                           help_text=HELP_TEXT['IY'],
                           verbose_name=None,
                           validators=[MinValueValidator(0)])

    IZ = models.FloatField(null=False, blank=False,
                           default=None, choices=None,
                           error_messages=None,
                           help_text=HELP_TEXT['IZ'],
                           verbose_name=None,
                           validators=[MinValueValidator(0)])

    IOMEGA = models.FloatField(null=True, blank=True,
                               default=None, choices=None,
                               error_messages=None,
                               help_text=HELP_TEXT['IOMEGA'],
                               verbose_name=None,
                               validators=[MinValueValidator(0)])

    VY = models.FloatField(null=False, blank=False,
                           default=None, choices=None,
                           error_messages=None,
                           help_text=HELP_TEXT['VY'],
                           verbose_name=None,
                           validators=[MinValueValidator(0)])

    VPY = models.FloatField(null=False, blank=False,
                            default=None, choices=None,
                            error_messages=None,
                            help_text=HELP_TEXT['VPY'],
                            verbose_name=None,
                            validators=[MinValueValidator(0)])

    VZ = models.FloatField(null=False, blank=False,
                           default=None, choices=None,
                           error_messages=None,
                           help_text=HELP_TEXT['VZ'],
                           verbose_name=None,
                           validators=[MinValueValidator(0)])

    VPZ = models.FloatField(null=False, blank=False,
                            default=None, choices=None,
                            error_messages=None,
                            help_text=HELP_TEXT['VPZ'],
                            verbose_name=None,
                            validators=[MinValueValidator(0)])

    MSY = models.FloatField(null=True, blank=True,
                            default=None, choices=None,
                            error_messages=None,
                            help_text=HELP_TEXT['MSY'],
                            verbose_name=None,
                            validators=[MinValueValidator(0)])

    MSZ = models.FloatField(null=True, blank=True,
                            default=None, choices=None,
                            error_messages=None,
                            help_text=HELP_TEXT['MSZ'],
                            verbose_name=None,
                            validators=[MinValueValidator(0)])

    TORS = models.FloatField(null=True, blank=True,
                             default=None, choices=None,
                             error_messages=None,
                             help_text=HELP_TEXT['TORS'],
                             verbose_name=None,
                             validators=[MinValueValidator(0)])

    SHEAR_Qy = models.FloatField(null=True, blank=True,
                                 default=None, choices=None,
                                 error_messages=None,
                                 help_text=HELP_TEXT['SHEAR_Qy'],
                                 verbose_name=None,
                                 validators=[MinValueValidator(0)])

    SHEAR_Qz = models.FloatField(null=True, blank=True,
                                 default=None, choices=None,
                                 error_messages=None,
                                 help_text=HELP_TEXT['SHEAR_Qz'],
                                 verbose_name=None,
                                 validators=[MinValueValidator(0)])

    GAMMA = models.FloatField(null=True, blank=True,
                              default=None, choices=None,
                              error_messages=None,
                              help_text=HELP_TEXT['GAMMA'],
                              verbose_name=None,
                              validators=[MinValueValidator(-360), MaxValueValidator(360)])

    # Exemple de valeur : '0.003#17-0: 17-99: 83-99: 83-0: 17-0'
    SYMBOL = models.CharField(max_length=250,
                              null=True, blank=True,
                              default=None, choices=None,
                              error_messages=None,
                              help_text=HELP_TEXT['SYMBOL'],
                              verbose_name=None)

    A_1 = models.FloatField(null=True, blank=True,
                            default=None, choices=None,
                            error_messages=None,
                            help_text=HELP_TEXT['A_1'],
                            verbose_name=None,
                            validators=[MinValueValidator(-360), MaxValueValidator(360)])

    A_2 = models.FloatField(null=True, blank=True,
                            default=None, choices=None,
                            error_messages=None,
                            help_text=HELP_TEXT['A_2'],
                            verbose_name=None,
                            validators=[MinValueValidator(-360), MaxValueValidator(360)])

    HAS_MATS = models.BooleanField(null=False, blank=False,
                                   default=None,
                                   error_messages=None,
                                   help_text=HELP_TEXT['HAS_MATS'],
                                   verbose_name=None)

    # TODO: A modifier par un champ ManyToMany une fois que la class Materials
    # sera créer
    MATERIALS = models.CharField(max_length=250,
                                 null=True, blank=True,
                                 default=None, choices=None,
                                 error_messages=None,
                                 help_text=HELP_TEXT['MATERIALS'],
                                 verbose_name=None)

    # TODO: Aucune idée du type de valeur..., par défaut CharField on vera par la suite...
    POINTS = models.CharField(max_length=250,
                              null=True, blank=True,
                              default=None, choices=None,
                              error_messages=None,
                              help_text=HELP_TEXT['POINTS'],
                              verbose_name=None)

    IS_THIN = models.BooleanField(null=False, blank=False,
                                  default=None,
                                  error_messages=None,
                                  help_text=HELP_TEXT['IS_THIN'],
                                  verbose_name=None)

    B_2 = models.FloatField(null=True, blank=True,
                            default=None, choices=None,
                            error_messages=None,
                            help_text=HELP_TEXT['B_2'],
                            verbose_name=None,
                            validators=[MinValueValidator(0)])

    ES_2 = models.FloatField(null=True, blank=True,
                             default=None, choices=None,
                             error_messages=None,
                             help_text=HELP_TEXT['ES_2'],
                             verbose_name=None,
                             validators=[MinValueValidator(0)])

    P1_L = models.FloatField(null=True, blank=True,
                             default=None, choices=None,
                             error_messages=None,
                             help_text=HELP_TEXT['P1_L'],
                             verbose_name=None,
                             validators=[MinValueValidator(0)])

    P1_T = models.FloatField(null=True, blank=True,
                             default=None, choices=None,
                             error_messages=None,
                             help_text=HELP_TEXT['P1_T'],
                             verbose_name=None,
                             validators=[MinValueValidator(0)])

    P2_L = models.FloatField(null=True, blank=True,
                             default=None, choices=None,
                             error_messages=None,
                             help_text=HELP_TEXT['P2_L'],
                             verbose_name=None,
                             validators=[MinValueValidator(0)])

    P2_T = models.FloatField(null=True, blank=True,
                             default=None, choices=None,
                             error_messages=None,
                             help_text=HELP_TEXT['P2_T'],
                             verbose_name=None,
                             validators=[MinValueValidator(0)])

    P3_L = models.FloatField(null=True, blank=True,
                             default=None, choices=None,
                             error_messages=None,
                             help_text=HELP_TEXT['P3_L'],
                             verbose_name=None,
                             validators=[MinValueValidator(0)])

    P3_T = models.FloatField(null=True, blank=True,
                             default=None, choices=None,
                             error_messages=None,
                             help_text=HELP_TEXT['P3_T'],
                             verbose_name=None,
                             validators=[MinValueValidator(0)])

    P4_L = models.FloatField(null=True, blank=True,
                             default=None, choices=None,
                             error_messages=None,
                             help_text=HELP_TEXT['P4_L'],
                             verbose_name=None,
                             validators=[MinValueValidator(0)])

    P4_T = models.FloatField(null=True, blank=True,
                             default=None, choices=None,
                             error_messages=None,
                             help_text=HELP_TEXT['P4_T'],
                             verbose_name=None,
                             validators=[MinValueValidator(0)])

    # Unknow field
    IYn = models.FloatField(null=True, blank=True,
                            default=None, choices=None,
                            error_messages=None,
                            help_text=HELP_TEXT['IYn'],
                            verbose_name=None,
                            validators=[MinValueValidator(0)])
    # Unknow field
    IYb = models.FloatField(null=True, blank=True,
                            default=None, choices=None,
                            error_messages=None,
                            help_text=HELP_TEXT['IYb'],
                            verbose_name=None,
                            validators=[MinValueValidator(0)])
