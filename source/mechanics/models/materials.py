#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import models
from django.utils.translation import ugettext as _
from django.core.validators import MinValueValidator, MaxValueValidator


class MaterialCategory(models.Model):

    class MaterialCategoryManager(models.Manager):

        def get_by_natural_key(self, title):
            return self.get(title=title)

    objects = MaterialCategoryManager()

    def natural_key(self):
        return (self.title,)

    title = models.CharField(_('title'), max_length=250, unique=True)
    parent = models.ForeignKey('self', blank=True, null=True, related_name='child',
                               on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = "Material's categories"
        ordering = ['title']

    def __str__(self):
        p_list = self._recurse_for_parents(self)
        p_list.append(self.title)
        return self.get_separator().join(p_list)

    def _recurse_for_parents(self, cat_obj):
        p_list = []
        if cat_obj.parent_id:
            p = cat_obj.parent
            p_list.append(p.title)
            more = self._recurse_for_parents(p)
            p_list.extend(more)
        if cat_obj == self and p_list:
            p_list.reverse()
        return p_list

    def get_separator(self):
        return ' :: '


class MaterialCatalog(models.Model):
    """Section Catalogue."""

    class MaterialCatalogManager(models.Manager):

        def get_by_natural_key(self, TITLE):
            return self.get(TITLE=TITLE)

    objects = MaterialCatalogManager()

    TITLE = models.CharField(max_length=250,
                             null=False, blank=False,
                             default=None, choices=None,
                             error_messages=None,
                             help_text=_("Nom du catalogue"),
                             verbose_name=None,
                             unique=True)

    DESCRIPTION = models.CharField(max_length=250,
                                   null=False, blank=False,
                                   default=None, choices=None,
                                   error_messages=None,
                                   help_text=_("Description du catalogue"),
                                   verbose_name=None)

    LOCATION = models.CharField(max_length=250,
                                null=False, blank=False,
                                default=None, choices=None,
                                error_messages=None,
                                help_text=_("Lieu d'application du catalogue"),
                                verbose_name=None)

    def natural_key(self):
        return (self.TITLE,)

    def __str__(self):
        return self.TITLE


HELP_TEXT = {
    'NAME': _("Nom du matériau"),
    'SECOND_NAME': _("Nom du matériau (utilisé pour l'acier afin de donner son correspondant suivant Eurocode)"),
    'DESCRIPTION': _("Description détaillée du matériau"),
    'TYPE': _("Type de matériau (1-Acier, 2-Béton, 3-Aluminium, 4-Bois, 5-Autres)"),
    'T': _("Température du matériau [°C]"),
    'THERM': _("Type de matériau /pour l'acier : indicateur du traitement thermique-1 /pour le bois : massif-0, lamellé-collé-1, lamellé-collé KertoS spécifique pour la norme Kerto-2, lamellé-collé KertoQ spécifique pour la norme Kerto-3, lamellé-collé KertoS-4, lamellé-collé KertoQ-5"),
    'DEFMAT': _("Indicateur du matériau par défaut pour un type donné"),
    'NU': _("coefficient de Poisson"),
    'LX': _("Coefficient de dilatation thermique"),
    'RO': _("Densité"),
    'CS': _("Coefficient de réduction au cisaillement pour l'acier / Vitesse de combustion en mm/min (données spécifiques pour le dimensionnement bois selon CB71)"),
    'DAMPCOEF': _("Coefficient d'amortissement"),
    'E': _("Module d'Young; Module d'élasticité longitudinale pour le bois"),
    'GMEAN': _("Module moyen de cisaillement G"),
    'KIRCHOFF': _("Module de Kirchoff, G"),
    'E_5': _("Module d'élasticité axiale 5% (données spécifiques pour le dimensionnement bois selon Eurocode5)"),
    'E_TRANS': _("Module d'élasticité transversale 5% (données spécifiques pour le dimensionnement bois selon Eurocode5)"),
    'RE': _("Résistance de calcul"),
    'RT': _("Résistance limite à la traction"),
    'RE_AXCOMP': _("Résistance à la compression axiale ( pour le bois)"),
    'RE_BENDING': _("Résistance à la flexion (pour le bois)"),
    'RE_AXTENS': _("Résistance à la traction axiale (pour le bois)"),
    'RE_TRTENS': _("Résistance à la traction transversale (pour le bois)"),
    'RE_TRCOMPR': _("Résistance à la compression transversale (pour le bois)"),
    'RE_SHEAR': _("Résistance au cisaillement (pour le bois)"),
    # # Eurocode 5
    # 'EC_OK':
    #     _("Indicateur de la prise des données matériau dans le calcul réglementaire suivant Eurocode5 (0/1)"),
    # 'EC_DEFORMAT':
    #     _("Module de cisaillement (données spécifiques pour le dimensionnement bois selon Eurocode5)"),
    # # CB71
    # 'BC71_CTG':
    #     _("Catégorie (données spécifiques pour le dimensionnement bois selon CB71)"),
    # 'BC71_REFR':
    #     _("Retrait - en % (données spécifiques pour le dimensionnement bois selon CB71)"),
    # 'BC71_NATURE':
    #     _("Nature du bois : non résineux-0, résineux-1 (données spécifiques pour le dimensionnement bois selon CB71)"),
    # 'BC71_HUMIDITY':
    #     _("Humidité initiale - de montage (données spécifiques pour le dimensionnement bois selon CB71)"),
    # 'BC_OK':
    #     _("Indicateur de la prise des données matériau dans le calcul réglementaire suivant CB71 (0/1)"),
    # # PN
    # 'PN_E_TRANS':
    #     _("Module d'élasticité transversale (données spécifiques pour le dimensionnement bois selon PN)"),
    # 'PN_E_ADITIONAL':
    #     _("Paramètre additionnel (données spécifiques pour le dimensionnement bois selon PN)"),
    # 'PN_OK':
    #     _("Indicateur de la prise des données matériau dans le calcul réglementaire suivant PN (0/1)"),
    # 'PN_DEFORMAT':
    #     _("Module de cisaillement (données spécifiques pour le dimensionnement bois selon PN)"),
    # 'RESERVE':
    #     _("0= unite N/m; 1= unite kip/in"),
    # 'RE_CODE':
    #     _("Le premier chiffre signifie la résistance (caractéristique ou de calcul) et le deuxième - une éprouvette cubique ou cylindrique"),

}


ERROR_MESSAGES = {
    'DEFAULT': {'null': None, 'blank': None, 'invalid': None, 'invalid_choice': None, 'unique': None,
                'unique_for_date': None},
}


class Material(models.Model):
    """
    Material.

    """

    class Meta:
        unique_together = ("CATALOG", "NAME", "T")

    CATALOG = models.ForeignKey(MaterialCatalog)
    CATEGORY = models.ForeignKey(MaterialCategory)

    THERMAL_TREATMENT = 1
    NATURAL = 0
    GLUED = 1
    GLUED_KERTOS_SPEFICIC = 2
    GLUED_KERTOQ_SPEFICIC = 3
    GLUED_KERTOS = 4
    GLUED_KERTOQ = 5

    THERM_CHOICES = (
        (THERMAL_TREATMENT, _("indicateur du traitement thermique")),
        (NATURAL, _("massif")),
        (GLUED, _("lamellé-collé")),
        (GLUED_KERTOS_SPEFICIC, _("lamellé-collé KertoS spécifique pour la norme Kerto")),
        (GLUED_KERTOQ_SPEFICIC, _("lamellé-collé KertoQ spécifique pour la norme Kerto")),
        (GLUED_KERTOS, _("lamellé-collé KertoS")),
        (GLUED_KERTOQ, _("lamellé-collé KertoQ")),
    )

    NAME = models.CharField(max_length=50,
                            null=False, blank=False,
                            default=None, choices=None,
                            error_messages=None,
                            help_text=HELP_TEXT['NAME'],
                            verbose_name=None)

    SECOND_NAME = models.CharField(max_length=50,
                                   null=False, blank=True,
                                   default='', choices=None,
                                   error_messages=None,
                                   help_text=HELP_TEXT['SECOND_NAME'],
                                   verbose_name=None)

    DESCRIPTION = models.CharField(max_length=50,
                                   null=False, blank=True,
                                   default='', choices=None,
                                   error_messages=None,
                                   help_text=HELP_TEXT['DESCRIPTION'],
                                   verbose_name=None)

    T = models.IntegerField(null=False, blank=False,
                            default=20, choices=None,
                            error_messages=None,
                            help_text=HELP_TEXT['T'],
                            verbose_name=None,
                            validators=[MinValueValidator(-274), MaxValueValidator(9999)])

    THERM = models.IntegerField(null=False, blank=False,
                                default=None, choices=None,
                                error_messages=None,
                                help_text=HELP_TEXT['THERM'],
                                verbose_name=None,
                                validators=[MinValueValidator(0), MaxValueValidator(5)])

    DEFMAT = models.BooleanField(null=False, blank=False,
                                 default=None,
                                 error_messages=None,
                                 help_text=HELP_TEXT['DEFMAT'],
                                 verbose_name=None)

    NU = models.FloatField(null=False, blank=False,
                           default=None, choices=None,
                           error_messages=None,
                           help_text=HELP_TEXT['NU'],
                           verbose_name=None,
                           validators=[MinValueValidator(-1), MaxValueValidator(0.5)])

    LX = models.FloatField(null=True, blank=True,
                           default=None, choices=None,
                           error_messages=None,
                           help_text=HELP_TEXT['LX'],
                           verbose_name=None)

    RO = models.FloatField(null=False, blank=False,
                           default=None, choices=None,
                           error_messages=None,
                           help_text=HELP_TEXT['RO'],
                           verbose_name=None,
                           validators=[MinValueValidator(0)])

    CS = models.FloatField(null=True, blank=True,
                           default=None, choices=None,
                           error_messages=None,
                           help_text=HELP_TEXT['CS'],
                           verbose_name=None,
                           validators=[MinValueValidator(0)])

    DAMPCOEF = models.FloatField(null=True, blank=True,
                                 default=None, choices=None,
                                 error_messages=None,
                                 help_text=HELP_TEXT['DAMPCOEF'],
                                 verbose_name=None,
                                 validators=[MinValueValidator(0)])

    # ----- Elasticity -----
    # ----------------------
    E = models.FloatField(null=False, blank=False,
                          default=None, choices=None,
                          error_messages=None,
                          help_text=HELP_TEXT['E'],
                          verbose_name=None,
                          validators=[MinValueValidator(0)])

    GMEAN = models.FloatField(null=True, blank=True,
                              default=None, choices=None,
                              error_messages=None,
                              help_text=HELP_TEXT['GMEAN'],
                              verbose_name=None,
                              validators=[MinValueValidator(0)])

    KIRCHOFF = models.FloatField(null=True, blank=True,
                                 default=None, choices=None,
                                 error_messages=None,
                                 help_text=HELP_TEXT['KIRCHOFF'],
                                 verbose_name=None,
                                 validators=[MinValueValidator(0)])

    E_5 = models.FloatField(null=True, blank=True,
                            default=None, choices=None,
                            error_messages=None,
                            help_text=HELP_TEXT['E_5'],
                            verbose_name=None,
                            validators=[MinValueValidator(0)])

    E_TRANS = models.FloatField(null=True, blank=True,
                                default=None, choices=None,
                                error_messages=None,
                                help_text=HELP_TEXT['E_TRANS'],
                                verbose_name=None,
                                validators=[MinValueValidator(0)])

    # ----- Resistance -----
    # ----------------------
    RE = models.FloatField(null=True, blank=True,
                           default=None, choices=None,
                           error_messages=None,
                           help_text=HELP_TEXT['RE'],
                           verbose_name=None,
                           validators=[MinValueValidator(0)])

    RT = models.FloatField(null=True, blank=True,
                           default=None, choices=None,
                           error_messages=None,
                           help_text=HELP_TEXT['RT'],
                           verbose_name=None,
                           validators=[MinValueValidator(0)])

    RE_AXCOMP = models.FloatField(null=True, blank=True,
                                  default=None, choices=None,
                                  error_messages=None,
                                  help_text=HELP_TEXT['RE_AXCOMP'],
                                  verbose_name=None,
                                  validators=[MinValueValidator(0)])

    RE_AXCOMP = models.FloatField(null=True, blank=True,
                                  default=None, choices=None,
                                  error_messages=None,
                                  help_text=HELP_TEXT['RE_AXCOMP'],
                                  verbose_name=None,
                                  validators=[MinValueValidator(0)])

    RE_BENDING = models.FloatField(null=True, blank=True,
                                   default=None, choices=None,
                                   error_messages=None,
                                   help_text=HELP_TEXT['RE_BENDING'],
                                   verbose_name=None,
                                   validators=[MinValueValidator(0)])

    RE_AXTENS = models.FloatField(null=True, blank=True,
                                  default=None, choices=None,
                                  error_messages=None,
                                  help_text=HELP_TEXT['RE_AXTENS'],
                                  verbose_name=None,
                                  validators=[MinValueValidator(0)])

    RE_TRTENS = models.FloatField(null=True, blank=True,
                                  default=None, choices=None,
                                  error_messages=None,
                                  help_text=HELP_TEXT['RE_TRTENS'],
                                  verbose_name=None,
                                  validators=[MinValueValidator(0)])

    RE_TRCOMPR = models.FloatField(null=True, blank=True,
                                   default=None, choices=None,
                                   error_messages=None,
                                   help_text=HELP_TEXT['RE_TRCOMPR'],
                                   verbose_name=None,
                                   validators=[MinValueValidator(0)])

    RE_SHEAR = models.FloatField(null=True, blank=True,
                                 default=None, choices=None,
                                 error_messages=None,
                                 help_text=HELP_TEXT['RE_SHEAR'],
                                 verbose_name=None,
                                 validators=[MinValueValidator(0)])

    def __str__(self):
        return self.NAME
