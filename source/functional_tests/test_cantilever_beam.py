#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.test import TestCase
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
# from selenium import webdriver
from selenium.webdriver import Firefox
# from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select

import sys
from unittest import skip


class NewVisitorTest(StaticLiveServerTestCase):

    fixtures = ['solvers.json', 'sections.json',
                'categories.json', 'calculations.json']

    @classmethod
    def setUpClass(cls):
        for arg in sys.argv:
            if 'liveserver' in arg:
                cls.live_server_url = 'http://' + arg.split('=')[1]
        cls.browser = Firefox()
        cls.browser.implicitly_wait(3)
        super().setUpClass()

    @classmethod
    def tearDownClass(cls):
        cls.browser.refresh()
        cls.browser.quit()

    def test_can_launch_cantilever_beam_calc(self):
        # Robert a entendu parler d'un calculateur d'étude mécanique en ligne
        # pour une poutre console
        self.browser.get(self.live_server_url +
                         '/calculation/cantilever_beam/input/')

        # Il remarque que le titre de la page d'accueil parle de calculateur
        # de poutre console
        self.assertIn('Cantilever beam', self.browser.title)
        h2_text = self.browser.find_element_by_tag_name('h2').text
        self.assertIn('CANTILEVER BEAM', h2_text)

        # Il remarque que la page comprend plusieurs sections
        h3_elem = self.browser.find_elements_by_tag_name('h3')
        h3_text = [elem.text for elem in h3_elem]
        self.assertIn('Main informations', h3_text)
        self.assertIn('Common inputs', h3_text)
        self.assertIn('Specific inputs', h3_text)

        # Il est invité à remplir les informations générales de l'étude
        # Il choisi le nom de l'etude
        solver = self.browser.find_element_by_id('id_name')
        solver.send_keys('ETUDE_001')

        # Il choisi le type de solver pour l'étude
        solver = Select(self.browser.find_element_by_id('id_solver'))
        solver.select_by_visible_text('Python')

        # Il choisi le type de section pour l'étude
        section = Select(self.browser.find_element_by_id('id_section'))
        section.select_by_visible_text('Bastaing (H165xL75)')

        # Il choisi le type de materiau pour l'étude
        material = Select(self.browser.find_element_by_id('id_material'))
        material.select_by_visible_text('S235')

        # Il valide les infos générales par le bouton OK
        submitButton = self.browser.find_element_by_id('id_submit')
        submitButton.submit()

        # Il arrive sur la page des résultats
        resulturl = self.browser.current_url
        self.assertRegex(resulturl, '.+/calculation/result/[0-9]+/')

        # Il arrive sur la page des résultats
        # self.fail("Finir le test fonctionnel")
