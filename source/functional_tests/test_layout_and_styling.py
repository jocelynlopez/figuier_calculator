#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.test import TestCase
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
# from selenium import webdriver
from selenium.webdriver import Firefox
# from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select

import sys
from unittest import skip


class NewVisitorTest(StaticLiveServerTestCase):

    fixtures = ['solvers.json', 'sections.json',
                'categories.json', 'calculations.json']

    @classmethod
    def setUpClass(cls):
        for arg in sys.argv:
            if 'liveserver' in arg:
                cls.live_server_url = 'http://' + arg.split('=')[1]
        cls.browser = Firefox()
        cls.browser.implicitly_wait(3)
        super().setUpClass()

    @classmethod
    def tearDownClass(cls):
        cls.browser.refresh()
        cls.browser.quit()

    @skip('Trouver un meilleur moyen de tester que le style est bien charger')
    def test_layout_and_styling(self):
        # Robert va sur la page d'accueil du cantilever_beam input
        self.browser.get(self.live_server_url +
                         '/calculation/cantilever_beam/input/')
        self.browser.set_window_size(1000, 768)
        self.browser.set_window_position(0, 0)

        # He notices the input box is nicely centered
        inputnamebox = self.browser.find_element_by_id('id_submit')
        self.assertAlmostEqual(
            inputnamebox.location['x'] + inputnamebox.size['width'] / 2.0,
            1000 / 2,
            delta=100
        )
