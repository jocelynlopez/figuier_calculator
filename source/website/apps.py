#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.apps import AppConfig

# Classe pour visualiser le nom dans l'interface d'administration


class WebsiteConfig(AppConfig):
    name = 'website'
    verbose_name = "Site web"
