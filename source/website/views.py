#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.shortcuts import redirect
from django.template import RequestContext
from study.models import StudyDefinition

def home(request):
    """Render home page."""
    return redirect('study_ipage', study_name="DC_Ferme")
