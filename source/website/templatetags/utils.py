#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
from django.utils.safestring import mark_safe
from django import template


register = template.Library()
class_re = re.compile(r'(?<=class=["\'])(.*)(?=["\'])')
for_re = re.compile(r'(?<=for=["\'])(.*)(?=["\'])')


@register.filter
def prettyname(value, prefixToCut=None):
    string = str(value)
    if prefixToCut is not None:
        string = string.replace(prefixToCut, '')
    if '_' in string and string.count('_') == 1:
        strings = string.split('_')
        return mark_safe(strings[0] + '<sub>' + strings[1] + '</sub>')
    elif '^' in string and string.count('^') == 1:
        strings = string.split('^')
        return mark_safe(strings[0] + '<sup>' + strings[1] + '</sup>')
    else:
        return mark_safe(string)
