#!/usr/bin/env python
# -*- coding: utf-8 -*-


def menu(request):
    AUTHOR = "Calman v0.0.1",
    DESCRIPTION = "Site de calcul",

    MENU = (
        {"name": "Accueil",
         "url": "/study/DC_Ferme/input/",
         "subitems": None},

        {"name": "Etude",
         "url": "",
         "subitems": (
             {"name": "Descente de charge", "url": "/study/DC_Ferme/input/"},
         ), },

        {"name": "Calcul",
         "url": "",
         "subitems": (
             {"name": "Charge sur le toit", "url": "/calculation/charge_toit/input/"},
             {"name": "Ferme", "url": "/calculation/ferme/input/"},
         ), },

    )

    return {"author": AUTHOR, "MENU": MENU, "description": DESCRIPTION}
