Django==1.9.5
jsonfield==1.0.3
jsonschema==2.5.1
numpy==1.11.0
path.py==8.1.2
gunicorn==19.4.5
pyyaml==3.11
sqlparse >0.2
# Python Interpreter 3.5.0
