#!/usr/bin/env python
# -*- coding: utf-8 -*-

import subprocess

import os
from django.conf import settings
from django.template.loader import render_to_string


class BaseCalculation(object):

    mesher_type = ''
    SUB_MESHER_DIR = 'mesher'

    def __init__(self, calculation, solver):
        self.solver = solver
        self.context = dict(calculation.ivars)
        self.name = calculation.name
        self.calculation_name = calculation.definition.name
        self.jobid = str(calculation.pk)
        self.workdir = os.path.join(settings.RESULTS_ROOT, self.jobid, self.SUB_MESHER_DIR)

        # Directories creation
        if not os.path.exists(self.workdir):
            os.makedirs(self.workdir)

        self.set_mesh_file()

    def set_mesh_file(self):
        self.mesh_script_file = None
        self.mesh_file = None
        self.context.update({'mesh_file': self.mesh_file})
        raise NotImplementedError('Need to be overrided by subclass')

    def get_template(self):
        if self.mesher_type == 'salome':
            extension = '.py'

        return os.path.join(os.path.basename(settings.MESHERS_ROOT), self.mesher_type,
                            self.calculation_name, self.calculation_name + extension)
