#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os

from django.conf import settings


class SalomeLocalServer(object):
    """A Salome object running on a local machine."""
    MODES = ('batch', 'terminal', 'gui')
    SHUTDOWN_OPTIONS = (1, 0)

    def __init__(self, version='7.8.0', port=2810, shutdown=1, mode='terminal'):
        self.default_version = version
        self.set_options(version, port, shutdown, mode)
        self.exe = settings.SALOME_EXECUTABLES

    def set_options(self, version, port, shutdown, mode):
        """Set all options from arguments."""
        self.options = []
        # mode option
        if mode in self.MODES:
            self.options.append('--%s' % mode)
        else:
            print('mode %s undefined' % mode)
        # shutdown options
        if shutdown in self.SHUTDOWN_OPTIONS:
            self.options.append('--shutdown-servers=%s' % shutdown)
        else:
            print('shutdown options is 0 or 1')
        # Port option
        self.options.append('--port=%s' % port)

    def add_log_options(self, log_file_path):
        """Ajout d'un log."""
        self.options.append('-f %s' % log_file_path)

    def run_command(self, exe, *args):
        """Linux command for starting salome."""

        cmd = 'python2 %s %s' % (exe, ' '.join(args))
        print(cmd)
        os.system(cmd)
        # os.system('touch /home/jocelyn/Documents/DEV/calculator-manager/results/16/touch_ok')
        # from subprocess import check_output
        # return check_output([exe], timeout=300)

    def get_running_version(self, version):
        """Get the running version of salome (between argument version if any or self.default_version)."""
        if version and version in self.exe.keys():
            return version
        elif version and version not in self.exe.keys():
            print("Salome Version %s unavailable.\nVersions availables are :\n" %
                  version + '\n'.join(self.exe.keys()))
            return None
        elif not version and self.default_version in self.exe.keys():
            return self.default_version
        elif not version and self.default_version not in self.exe.keys():
            print("Salome Version %s unavailable.\nVersions availables are :\n" %
                  self.default_version + '\n'.join(self.exe.keys()))
        else:
            print("Salome Version is undefined.\nChoice versions between :\n" + '\n'.join(self.exe.keys()))

    def run_script(self, pythonFile, version=None):
        """Lancement du script dans Salome."""
        running_version = self.get_running_version(version)
        if running_version is not None:
            return self.run_command(self.exe[running_version], pythonFile, *self.options)
