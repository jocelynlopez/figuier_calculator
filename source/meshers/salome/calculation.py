#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
from os.path import join, basename

from django.conf import settings

from solvers.utils import render_to_file
from meshers.utils import BaseCalculation


class SalomeBaseCalculation(BaseCalculation):

    mesher_type = 'salome'

    def __init__(self, calculation, solver, **kwargs):
        super(SalomeBaseCalculation, self).__init__(calculation, solver)
        if not os.path.exists(self.workdir):
            os.makedirs(self.workdir)

    def set_mesh_file(self):
        self.mesh_script_file = join(self.workdir, self.name + '.py')
        self.mesh_file = join(self.workdir, self.name + '.mmed')
        self.context.update({'mesh_file': self.mesh_file})

    def preprocess(self):
        """Create script files for salome."""
        render_to_file(self.get_template(), self.mesh_script_file, self.context)

    def run(self, version=None):
        """Run Code_Aster calculation."""
        # Build all files
        self.preprocess()

        # Generation of mesh
        os.chdir(settings.MESHERS_ROOT)
        return self.solver.run_script(self.mesh_script_file, version=None)
