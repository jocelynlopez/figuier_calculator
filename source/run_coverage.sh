
../virtualenv/bin/coverage run ./manage.py test $1 $2 $3 $4
../virtualenv/bin/coverage report --fail-under=98
../virtualenv/bin/coverage html
firefox ../coverage/index.html
