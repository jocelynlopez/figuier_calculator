#!/usr/bin/env python
# coding=utf-8

import logging
from collections import OrderedDict

from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404, redirect, render

from calculation.forms import CalculationForm
from calculation.models import CalculationDefinition
from calculation.views import get_calculation_or_run, make_calculation_groups
from study.forms import StudyForm
from study.models import Study, StudyDefinition

logger = logging.getLogger(__name__)

"""
Les variables de chaque 'calculation' sont affichées, en dehors des variables dépendantes de calculs précédents.
"""


def get_min_ivars(study):

    l_ivars = []
    for calculation in study.calculations:
        definition = calculation["definition"]
        name = calculation["name"]

        # Get ivars for CalculationDefinition
        ivars = CalculationDefinition.objects.get(name=definition).ivars

        # Populate ivars list
        for var in ivars.keys():
            if var not in calculation["idvars"].keys():
                if name in l_ivars.keys():
                    l_ivars[name].append(var)
                else:
                    l_ivars[name] = [var]

    return l_ivars


def extract_ivars(cleaned_data):
    """Get input variables from cleaned data of the form."""

    # Delete keys which is not an input variable
    # cleaned_data.pop('name')

    # Get all input variables and clean their name
    ivars = {}
    for key, value in cleaned_data.items():
        if 'ivar_' in key:
            ivars[key.replace('ivar_', '')] = value

    return ivars


def _study_context(study_def):
    context = {
        "title": study_def.name.replace("_", " ").capitalize(),

        "activeMenuItem": "Study",
        "tags": study_def.tags.all(),
        "study": study_def,
        "paths": (
            ("Study", reverse('study_home')),
        ),
    }
    return context


def build_calculations(study_def, request):
    calculations = OrderedDict()
    for i, calculation in enumerate(study_def.calculations.all()):
        exclude_fields = list(calculation.idvars.keys())
        form = CalculationForm(request.POST or None,
                               ivars=calculation.definition.ivars,
                               prefix=str(i + 1),
                               exclude_fields=exclude_fields)
        form.fields['name'].initial = 'default_name'

        solvers = calculation.definition.solvers.all()
        if len(solvers) == 1:
            form.fields['solver'].initial = solvers[0]
        form.fields['solver'].queryset = solvers

        meshers = calculation.definition.meshers.all()
        if len(meshers) > 0:
            thereIsMesher = True
            form.fields['mesher'].queryset = meshers
        else:
            thereIsMesher = False
        if len(meshers) == 1:
            form.fields['mesher'].initial = meshers[0]
        form.fields['mesher'].queryset = meshers

        calc_groups = make_calculation_groups(calculation.definition, form)[0]

        calculations[calculation.name] = {"calculation": calculation, "form": form,
                                          "groups": calc_groups, "thereIsMesher": thereIsMesher}
    return calculations


def _input_context(study_def, study_form, request):
    """Function returning global context for the calculation."""

    calculations = build_calculations(study_def, request)
    groups, ParametersExist = make_calculation_groups(study_def, study_form)

    context = {
        "groups": groups,
        "ParametersExist": ParametersExist,
        "calculations": calculations,
        "study_form": study_form
    }

    return context


def input_page(request, study_name=None):
    """Render input page."""
    study_def = get_object_or_404(StudyDefinition, name=study_name)
    study_form = StudyForm(request.POST or None, ivars=study_def.ivars)

    context = _study_context(study_def)
    context.update(_input_context(study_def, study_form, request))

    if study_form.is_valid():
        logger.info("Traitement de l'étude '%s'." % study_name)
        study = study_form.save(commit=False)
        study.definition = study_def
        study.ivars = {'study': extract_ivars(study_form.cleaned_data)}
        logger.debug("  - ivars = %s" % study.ivars)
        study.save()

        # Check calculations forms in execution order
        study.ovars = {}
        calculations = context["calculations"]
        all_calculations_is_valid = True
        for name in calculations:
            logger.info("   - Traitement du calcul '%s' dans l'étude '%s'." %
                        (name, study_name))
            calculation = calculations[name]["calculation"]
            calc_form = calculations[name]["form"]

            # a = calc_form.is_valid()
            # raise NotImplementedError
            if calc_form.is_valid():
                all_calculations_is_valid = all_calculations_is_valid and calc_form.is_valid()

                logger.info("       - son formulaire est valide.")
                calc = calc_form.save(commit=False)
                calc.definition = calculation.definition
                calc.ivars = extract_ivars(calc_form.cleaned_data)

                # --------------------------------------------------
                # set here dependent ivars from previous calculation
                dependent_ivars = {}
                for ivar_name, idvar in calculation.idvars.items():
                    if idvar["calculation"] in study.ovars.keys():
                        if idvar["name"] in study.ovars[idvar["calculation"]]:
                            dependent_ivars[ivar_name] = study.ovars[
                                idvar["calculation"]][idvar["name"]]
                    if idvar["calculation"] in study.ivars.keys():
                        if idvar["name"] in study.ivars[idvar["calculation"]]:
                            dependent_ivars[ivar_name] = study.ivars[
                                idvar["calculation"]][idvar["name"]]
                calc.ivars.update(dependent_ivars)
                calc = get_calculation_or_run(calc)

                # Save calculations in study
                study.calculations.add(calc)
                study.ivars[name] = calc.ivars
                study.ovars[name] = calc.ovars
            else:
                all_calculations_is_valid = False
                logger.info(
                    "Le formulaire du calcul '%s' n'est pas complet." % name)
                logger.debug("Les erreurs dans le formulaire son : %s" %
                             calc_form.errors)

        if all_calculations_is_valid:
            study.save()
            return redirect('latex2pdf', pk=study.pk, resu_type="Study")
    else:
        logger.info(
            "Le formulaire de l'étude '%s' n'est pas complet." % study_name)
        logger.debug("Les erreurs dans le formulaire son : %s" %
                     study_form.errors)

    return render(request, 'study_input.html', context)


def result_page(request, pk):
    """Render input page."""
    study = get_object_or_404(Study, pk=pk)

    calculations = {}
    for calculation in study.calculations.all():
        name = calculation.definition.name
        calculations[name] = {"calculation": calculation}
        calculations[name]["form"] = CalculationForm(ovars=calculation.definition.ovars,
                                                     ovars_result=calculation.ovars)

    return render(request, 'study_result.html', {"calculations": calculations, "resu": study})
