#!/usr/bin/env python
# coding=utf-8

from django import forms

from study.models import Study
from calculation.fields import build_input_field, build_output_field


class StudyForm(forms.ModelForm):

    class Meta:
        model = Study

        fields = ('cpu_time',)

    def __init__(self, *args, **kwargs):

        if 'ivars' in kwargs.keys():
            ivars = kwargs.pop('ivars')
        elif 'ovars' in kwargs.keys():
            ovars = kwargs.pop('ovars')
            ovars_result = kwargs.pop('ovars_result')
        super(StudyForm, self).__init__(*args, **kwargs)

        if 'ivars' in vars():
            for name, field_info in ivars.items():
                self.fields['ivar_%s' % name] = build_input_field(name, field_info)

        elif 'ovars' in vars():
            for calc_name, fields_info in ovars.items():
                for name, field_info in fields_info.items():
                    result = ovars_result[name]
                    self.fields['ovar_%s_%s' % (calc_name, name)] = build_output_field(
                        name, field_info, result)
