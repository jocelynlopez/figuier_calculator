#!/usr/bin/env python
# -*- coding: utf-8 -*-

import jsonfield

from django.db import models
from django.utils import timezone

from calculation.models import Calculation, CalculationCategory, Tags
from calculation.models import ListField, CalculationDefinition


class StudyCalculationDefinition(models.Model):
    """Calculation model in a Study."""

    class Meta:
        ordering = ['execution_order']

    name = models.CharField(max_length=50)
    definition = models.ForeignKey(CalculationDefinition)
    description = models.TextField()
    level = models.IntegerField()
    execution_order = models.IntegerField()
    idvars = jsonfield.JSONField(default=None)


class StudyDefinition(models.Model):
    """Definition of a calculation."""
    name = models.CharField(max_length=50, unique=True)
    category = models.ForeignKey(CalculationCategory)
    description = models.TextField()
    hypothesis = ListField()
    images = jsonfield.JSONField(default=None)
    reports = jsonfield.JSONField(default=None)

    tags = models.ManyToManyField(Tags)
    ivars = jsonfield.JSONField(default=None)
    ovars = jsonfield.JSONField(default=None)

    calculations = models.ManyToManyField(StudyCalculationDefinition)

    def __str__(self):
        return self.name


class Study(models.Model):
    """Definition of a calculation."""
    # name = models.CharField(max_length=50)
    definition = models.ForeignKey(StudyDefinition)

    calculations = models.ManyToManyField(Calculation)

    created_date = models.DateTimeField(default=timezone.now)
    updated_date = models.DateTimeField(auto_now=True)
    cpu_time = models.DurationField(null=True, blank=True)

    ivars = jsonfield.JSONField()
    ovars = jsonfield.JSONField(default=None)

    def __str__(self):
        return self.name
