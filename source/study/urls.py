#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.conf.urls import url

from study import views

urlpatterns = [
    url(r'^$', views.input_page, name='study_home'),
    url(r'^(?P<study_name>.*)/input/$', views.input_page, name='study_ipage'),
    url(r'^result/(?P<pk>[0-9]+)/$', views.result_page, name='study_rpage'),
]
