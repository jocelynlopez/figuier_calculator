#!/usr/bin/env python
# -*- coding: utf-8 -*-

from subprocess import Popen, PIPE
import tempfile
from os.path import join, basename, dirname
import logging
import os

from django.shortcuts import get_object_or_404
from django.http import HttpResponse
from django.template.loader import get_template
from django.conf import settings

from calculation.models import Calculation
from study.models import Study


def get_report_template_name(calculation, report_type):
    return join(basename(settings.REPORTS_ROOT), report_type,
                calculation.definition.name,
                calculation.definition.reports[report_type][0])


def latex2pdf(request, pk, resu_type="Calculation", attachment=False):
    """Render input page."""
    if resu_type == "Calculation":
        calculation = get_object_or_404(Calculation, pk=pk)
        input_context = calculation.ivars
        output_context = calculation.ovars
        template_name = get_report_template_name(calculation, 'latex')
    elif resu_type == "Study":
        study = get_object_or_404(Study, pk=pk)
        input_context = study.ivars
        output_context = study.ovars
        template_name = get_report_template_name(study, 'latex')

    report_context = {'i': input_context, 'o': output_context}
    logger = logging.getLogger(__name__)
    logger.info("Génération du rapport avec le contexte : %s" % report_context)
    wk_file = join(settings.REPORTS_ROOT, 'latex',
                   study.definition.name,
                   study.definition.reports['latex'][0])
    template = get_template(template_name)
    rendered_tpl = template.render(report_context).encode('utf-8')

    pdf = compil_latex(wk_file, rendered_tpl)

    # with tempfile.TemporaryDirectory() as tempdir:
    #     # Create subprocess, supress output with PIPE and
    #     # run latex twice to generate the TOC properly.
    #     # Finally read the generated pdf.
    #     logger.info("tempdir : %s" % tempdir)
    #     Popen(['cp', '%s/*' % wkdir, '%s/.' % tempdir])
    #     for i in range(2):
    #         process = Popen(['pdflatex', '-output-directory', tempdir],
    #                         stdin=PIPE, stdout=PIPE)
    #         process.communicate(rendered_tpl)
    #     with open(join(tempdir, 'texput.pdf'), 'rb') as f:
    #         pdf = f.read()
    response = HttpResponse(content_type='application/pdf')
    if attachment:
        response['Content-Disposition'] = 'attachment; filename=texput.pdf'
    response.write(pdf)
    return response


def compil_latex(wk_file, rendered_tpl):
    logger = logging.getLogger(__name__)
    logger.info('cp %s/* %s/.' % (dirname(wk_file), "reports_tmp"))
    logger.info('cd /tmp/reports_tmp')
    os.system('mkdir -p /tmp/reports_tmp')
    os.system('cp %s/* /tmp/reports_tmp/.' % (dirname(wk_file)))
    os.system('cd /tmp/reports_tmp && ')

    texfile = '/tmp/reports_tmp/' + os.path.splitext(os.path.basename(wk_file))[0] + '.tex'
    with open(texfile, 'wb') as f:
        f.write(rendered_tpl)
    os.system('cd /tmp/reports_tmp && pdflatex %s' % texfile)
    # os.system('cd /tmp/reports_tmp && xelatex %s' % texfile)
    with open('/tmp/reports_tmp/' + os.path.splitext(os.path.basename(wk_file))[0] + '.pdf', 'rb') as f:
        pdf = f.read()
    os.system('rm -rf /tmp/reports_tmp')
    return pdf
