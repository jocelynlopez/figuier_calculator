#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.conf.urls import url
from reports import views

urlpatterns = [
    url(r'^(?P<resu_type>.*)/(?P<pk>[0-9]+)/latex2pdf/$', views.latex2pdf, name='latex2pdf'),
]
