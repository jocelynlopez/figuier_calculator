#!/usr/bin/env python
# coding=utf-8


from django import forms
from django.utils.translation import ugettext as _

from calculation.models import Calculation
from calculation.fields import build_input_field, build_output_field

# Constants:
CONST = {
    "name": {
        "empty_error": _("Veuillez fournir un nom pour le calcul"),
        "placeholder": "",
        "css_class": "form-control",
        "pattern": "^[_A-z0-9]{1,}$",
    },
    "solver": {
        "empty_error": _("Veuillez choisir un solveur"),
        "css_class": "form-control c-select",
    },
    "mesher": {
        "empty_error": _("Veuillez choisir un mailleur"),
        "css_class": "form-control c-select",
    },
    "section": {
        "empty_error": _("Veuillez choisir une section"),
        "css_class": "form-control c-select",
    },
    "material": {
        "empty_error": _("Veuillez choisir un matériau"),
        "css_class": "form-control c-select",
    },
}


class CalculationForm(forms.ModelForm):

    class Meta:
        model = Calculation
        fields = ('name', 'solver', 'mesher')
        widgets = {
            # 'name': forms.fields.TextInput(attrs={
            #     'class': 'form-control',
            # }),
            'solver': forms.fields.Select(attrs={
                'class': 'form-control',
            }),
            'mesher': forms.fields.Select(attrs={
                'class': 'form-control',
            }),
        }

    def __init__(self, *args, **kwargs):

        # extraction des arguments supplémentaires fournis
        if 'ivars' in kwargs.keys():
            ivars = kwargs.pop('ivars')
        elif 'ovars' in kwargs.keys():
            ovars = kwargs.pop('ovars')
            ovars_result = kwargs.pop('ovars_result')
        if 'exclude_fields' in kwargs.keys():
            exclude_fields = kwargs.pop('exclude_fields')
        super(CalculationForm, self).__init__(*args, **kwargs)

        # Parametres de base
        if 'exclude_fields' in vars():
            if not 'name' in exclude_fields:
                self.fields['name'].widget.attrs["class"] = 'form-control'
        else:
            self.fields['name'].widget.attrs["class"] = 'form-control'

        if 'exclude_fields' in vars():
            if not 'solver' in exclude_fields:
                self.fields['solver'].widget.attrs["class"] = 'form-control'
        else:
            self.fields['solver'].widget.attrs["class"] = 'form-control'

        if 'exclude_fields' in vars():
            if not 'mesher' in exclude_fields:
                self.fields['mesher'].widget.attrs["class"] = 'form-control'
        else:
            self.fields['mesher'].widget.attrs["class"] = 'form-control'

        if 'ivars' in vars():
            for name, field_info in ivars.items():
                if 'exclude_fields' in vars():
                    if not name in exclude_fields:
                        self.fields['ivar_%s' % name] = build_input_field(name, field_info)
                else:
                    self.fields['ivar_%s' % name] = build_input_field(name, field_info)
        elif 'ovars' in vars():
            for name, field_info in ovars.items():
                if 'exclude_fields' in vars():
                    if not name in exclude_fields:
                        result = ovars_result[name]
                        self.fields['ovar_%s' % name] = build_output_field(name, field_info, result)
                else:
                    result = ovars_result[name]
                    self.fields['ovar_%s' % name] = build_output_field(name, field_info, result)
