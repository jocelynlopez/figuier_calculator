#!/usr/bin/env python
# -*- coding: utf-8 -*-

import jsonfield
import ast

from django.db import models
from django.utils.translation import ugettext as _
from django.utils import timezone


class ListField(models.TextField):
    """List Field for django of basic object."""
    __metaclass__ = models.SubfieldBase
    description = "Stores a python list of basic object"

    def from_db_value(self, value, expression, connection, context):
        if value is None:
            return value
        return ast.literal_eval(value)

    def to_python(self, value):
        if isinstance(value, list):
            return value

        if value is None:
            return value

        return ast.literal_eval(value)

    def get_prep_value(self, value):
        if value is None:
            return value

        return str(value)


class Solver(models.Model):
    """Solver for a calculation."""

    # Block needed for named fixtures (not only pk in fixture)
    # --------------------------------------------------------
    class SolverManager(models.Manager):

        def get_by_natural_key(self, name):
            return self.get(name=name)

    objects = SolverManager()

    def natural_key(self):
        return (self.name,)
    # --------------------------------------------------------

    name = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.name


class Mesher(models.Model):
    """Mesher for a calculation with an EF solver."""

    # Block needed for named fixtures (not only pk in fixture)
    # --------------------------------------------------------
    class MesherManager(models.Manager):

        def get_by_natural_key(self, name):
            return self.get(name=name)

    objects = MesherManager()

    def natural_key(self):
        return (self.name,)
    # --------------------------------------------------------

    name = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.name


class Tags(models.Model):
    """Tags for a calculation."""

    # Block needed for named fixtures (not only pk in fixture)
    # --------------------------------------------------------
    class TagsManager(models.Manager):

        def get_by_natural_key(self, name):
            return self.get(name=name)

    objects = TagsManager()

    def natural_key(self):
        return (self.name,)
    # --------------------------------------------------------

    name = models.CharField(max_length=50,
                            unique=True)

    def __str__(self):
        return self.name


class CalculationCategory(models.Model):
    """Category for a calculation."""

    # Block needed for named fixtures (not only pk in fixture)
    # --------------------------------------------------------
    class CalculationCategoryManager(models.Manager):

        def get_by_natural_key(self, title):
            return self.get(title=title)

    objects = CalculationCategoryManager()

    def natural_key(self):
        return (self.title,)
    # --------------------------------------------------------

    title = models.CharField(_('title'), max_length=250, unique=True)
    parent = models.ForeignKey('self', blank=True, null=True, related_name='child',
                               on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = "Calculation's categories"
        ordering = ['title']

    def __str__(self):
        p_list = self._recurse_for_parents(self)
        p_list.append(self.title)
        return self.get_separator().join(p_list)

    def _recurse_for_parents(self, cat_obj):
        p_list = []
        if cat_obj.parent_id:
            p = cat_obj.parent
            p_list.append(p.title)
            more = self._recurse_for_parents(p)
            p_list.extend(more)
        if cat_obj == self and p_list:
            p_list.reverse()
        return p_list

    def get_separator(self):
        return ' :: '


class CalculationDefinition(models.Model):
    """Definition of a calculation."""

    # Block needed for named fixtures (not only pk in fixture)
    # --------------------------------------------------------
    class CalculationDefinitionManager(models.Manager):

        def get_by_natural_key(self, name):
            return self.get(name=name)

    objects = CalculationDefinitionManager()

    def natural_key(self):
        return (self.name,)
    # --------------------------------------------------------

    name = models.CharField(max_length=50, unique=True)
    category = models.ForeignKey(CalculationCategory)
    solvers = models.ManyToManyField(Solver)
    meshers = models.ManyToManyField(Mesher)
    description = models.TextField()
    hypothesis = ListField()
    images = jsonfield.JSONField(default=None)
    reports = jsonfield.JSONField(default=None)

    tags = models.ManyToManyField(Tags)

    ivars = jsonfield.JSONField(default=None)
    ovars = jsonfield.JSONField(default=None)

    def __str__(self):
        return self.name


class Calculation(models.Model):
    """A calculation with it's input and results."""
    name = models.CharField(max_length=50)
    definition = models.ForeignKey(CalculationDefinition)
    solver = models.ForeignKey(Solver)
    mesher = models.ForeignKey(Mesher, null=True, blank=True)

    created_date = models.DateTimeField(default=timezone.now)
    updated_date = models.DateTimeField(auto_now=True)
    cpu_time = models.DurationField(null=True, blank=True)

    ivars = jsonfield.JSONField()
    ovars = jsonfield.JSONField(default=None)
