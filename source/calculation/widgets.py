#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.utils.safestring import mark_safe
from django import forms


class ChoiceWidget(forms.Select):

    def __init__(self, attrs=None, choices=(), calculation=None):
        self.calculation = calculation
        super(ChoiceWidget, self).__init__(attrs, choices)

    def render(self, name, value, attrs=None, choices=()):
        # Auto select if one choice available
        if value is None and len(self.choices) == 2:
            value = self.choices[1][0]
        output = []
        output.append('<div class="input-group-addon">{name}</div>'.format(**self.calculation))
        output.append(super(ChoiceWidget, self).render(name, value, attrs, choices))
        return mark_safe('\n'.join(output))


class ChoiceValueWidget(forms.Select):

    def __init__(self, attrs=None, choices=(), calculation=None):
        self.calculation = calculation
        super(ChoiceValueWidget, self).__init__(attrs, choices)

    def render(self, name, value, attrs=None, choices=()):
        final_attrs = self.build_attrs(attrs, name=name)
        value_id = final_attrs['id'].replace('id', 'value')
        # Auto select if one choice available
        if value is None and len(self.choices) == 2:
            value = self.choices[1][0]
        output = []
        output.append('<div class="input-group-addon">{name}</div>'.format(**self.calculation))
        output.append(super(ChoiceValueWidget, self).render(name, value, attrs, choices))
        output.append(
            '<div class="input-group-addon"> <span id="{value_id}"></span>{units}</div>'.format(
                value_id=value_id, units=self.calculation['units']))
        return mark_safe('\n'.join(output))


class BooleanWidget(forms.CheckboxInput):

    def __init__(self, calculation=None, *args, **kwargs):
        self.calculation = calculation
        forms.CheckboxInput.__init__(self, *args, **kwargs)

    def render(self, *args, **kwargs):
        output = []
        output.append('<div class="input-group-addon">{name}</div>'.format(**self.calculation))
        output.append(forms.CheckboxInput.render(self, *args, **kwargs))
        return mark_safe('\n'.join(output))


class ScalarFloatWidget(forms.TextInput):

    def __init__(self, attrs=None, calculation=None):
        self.calculation = calculation
        super(ScalarFloatWidget, self).__init__(attrs)

    def render(self, name, value, attrs=None):
        output = []
        output.append('<div class="input-group-addon">{name}</div>'.format(**self.calculation))
        output.append(super(ScalarFloatWidget, self).render(name, value, attrs))
        output.append(
            '<div class="input-group-addon"> <span id="value_{name}"></span>{units}</div>'.format(**self.calculation))
        return mark_safe('\n'.join(output))


class ScalarIntegerWidget(forms.TextInput):

    def __init__(self, attrs=None, calculation=None):
        self.calculation = calculation
        super(ScalarIntegerWidget, self).__init__(attrs)

    def render(self, name, value, attrs=None):
        output = []
        output.append('<div class="input-group-addon">{name}</div>'.format(**self.calculation))
        output.append(super(ScalarIntegerWidget, self).render(name, value, attrs))
        output.append(
            '<div class="input-group-addon"> <span id="value_{name}"></span>{units}</div>'.format(**self.calculation))
        return mark_safe('\n'.join(output))
