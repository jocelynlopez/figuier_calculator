#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.test import TestCase
from unittest import skip

from calculation.forms import CONST, CalculationForm
from calculation.models import Calculation


def get_correct_data():
    return {
        'name': ["my calc"],
        'solver': ["1"],
    }


def data_with_blank(var):
    wrong_data = get_correct_data()
    wrong_data[var] = ""
    return wrong_data


class CalculationResultsFormTest(TestCase):

    fixtures = ['solvers.json', 'sections.json',
                'categories.json', 'calculations.json']

    def test_form_vars_input_has_placeholder_and_css_classes(self):
        form = CalculationForm()
        data = get_correct_data()
        l_vars = data.keys()
        for var in l_vars:
            form = CalculationForm(data)
            self.assertIn('class="{}"'.format(CONST[var]["css_class"]),
                          form.as_p())
            if "placeholder" in CONST[var].keys():
                self.assertIn('placeholder="{}"'.
                              format(CONST[var]["placeholder"]), form.as_p())

    def test_form_validation_for_blank_var(self):
        l_vars = get_correct_data().keys()
        for var in l_vars:
            form = CalculationForm(data_with_blank(var))
            self.assertFalse(form.is_valid())
            self.assertEqual(form.errors[var], [CONST[var]["empty_error"]])

    @skip("data is not valid: raise error on save. Need to impleted all var of Calculation model")
    def test_form_save_handles_saving(self):
        data = get_correct_data()
        form = CalculationForm(data=data)
        import pdb
        pdb.set_trace()

        calc = form.save()      # Should not raise
        self.assertEqual(calc, Calculation.objects.first())

        l_vars = data.keys()
        for var in l_vars:
            self.assertEqual(getattr(calc, var), data[var])
