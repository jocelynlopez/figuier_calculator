#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.test import TestCase

from calculation.models import CalculationDefinition, Solver, CalculationCategory


class SolverTest(TestCase):

    @classmethod
    def setUpClass(cls):
        # Attributes values expected
        cls.name_expected = "Python"

        # Model creation
        cls.solver = Solver(name=cls.name_expected)

        # Save in database
        cls.solver.save()
        super().setUpClass()

    def test_name_attribute(self):
        solver = Solver.objects.all()[0]
        self.assertEqual(solver.name, self.name_expected)

    def test_string_representation(self):
        entry = Solver(name="Python")
        self.assertEqual(str(entry), entry.name)


class CategoryTest(TestCase):

    fixtures = ['categories.json']

    @classmethod
    def setUpClass(cls):
        # Attributes values expected
        cls.base_expected = "section"
        cls.master_expected = "OTUA"
        cls.middle_expected = "IPE"
        cls.title_expected = "IPE80"
        cls.str_expected = "OTUA :: IPE :: IPE80"

        super().setUpClass()

    def test_base_attribute(self):
        cat = CalculationCategory.objects.get(title="IPE80")
        self.assertEqual(cat.base, self.base_expected)

    def test_title_attribute(self):
        cat = CalculationCategory.objects.get(title="IPE80")
        self.assertEqual(cat.title, self.title_expected)

    def test_parent_attribute(self):
        cat = CalculationCategory.objects.get(title="IPE80")
        self.assertEqual(cat.parent.title, self.middle_expected)

    def test_string_representation(self):
        cat = CalculationCategory.objects.get(title="IPE80")
        self.assertEqual(str(cat), self.str_expected)


class CalculationTest(TestCase):

    fixtures = ['solvers.json', 'sections.json',
                'categories.json', 'calculations.json']

    @classmethod
    def setUpClass(cls):
        cls.name_expected = "cantilever_beam"
        cls.category_expected = "Beam"
        cls.solvers_expected = ["Python", "Excel"]
        cls.ivars_expected = {
            "force": {
                "type": "Torseur",
                "units": ["N", "N.m"],
                "desc": "Torseur d'effort appliquer à l'extrémité de la poutre"
            }
        }
        cls.ovars_expected = {
            "disp_max": {
                "type": "Vecteur",
                "units": ["mm"],
                "desc": "Déplacements maximaux en bout de poutre"
            }
        }
        super().setUpClass()

    def test_name_attribute(self):
        calc = CalculationDefinition.objects.all()[0]
        self.assertEqual(calc.name, self.name_expected)

    def test_string_representation(self):
        entry = CalculationDefinition(name="cantilever_beam")
        self.assertEqual(str(entry), entry.name)

    def test_category_attribute(self):
        calc = CalculationDefinition.objects.get(name=self.name_expected)
        self.assertEqual(calc.category.title, self.category_expected)

    def test_solvers_attribute(self):
        calc = CalculationDefinition.objects.get(name=self.name_expected)
        l_solvers = [solver.name for solver in calc.solvers.all()]
        self.assertListEqual(l_solvers, self.solvers_expected)

    def test_ivars_attribute(self):
        calc = CalculationDefinition.objects.all()[0]
        self.assertDictEqual(calc.ivars, self.ivars_expected)

    def test_ovars_attribute(self):
        calc = CalculationDefinition.objects.all()[0]
        self.assertDictEqual(calc.ovars, self.ovars_expected)
