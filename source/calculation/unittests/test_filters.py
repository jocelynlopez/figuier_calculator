#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.test import TestCase
from unittest import skip

from calculation.templatetags.utils import islist


class islistTest(TestCase):

    def test_true(self):
        self.assertTrue(islist([]))

    def test_false(self):
        self.assertFalse(islist({}))
