#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.test import TestCase
from django.core.urlresolvers import resolve, reverse
from django.utils.html import escape
from calculation.models import CalculationDefinition, Calculation
from calculation.views import input_page, g_context
from calculation.forms import CalculationForm

from calculation.forms import CONST


class GetContextTest(TestCase):

    fixtures = ['solvers.json', 'sections.json',
                'categories.json', 'calculations.json']

    def setUp(self):
        self.calc = CalculationDefinition.objects.get(name="cantilever_beam")

    def test_return_context(self):
        expected_ctx = {
            "title": "Cantilever beam",
            "calc": self.calc,
            "paths": (
                ("Calculation", reverse('calculations')),
            )
        }
        result_ctx = g_context(self.calc)
        self.assertDictEqual(expected_ctx, result_ctx)
        # self.assertDictContainsSubset(expected_ctx, result_ctx)


class InputPageTest(TestCase):

    fixtures = ['solvers.json', 'sections.json',
                'categories.json', 'calculations.json']

    # ----------------------------------------
    def post_invalid_input(self):
        return self.client.post('/calculation/cantilever_beam/input/',
                                {'name': "my calc"})
    # ----------------------------------------

    def setUp(self):
        """Basic get request"""
        self.response = self.client.get('/calculation/cantilever_beam/input/')

    # ----------------------------------------

    def test_url_resolves_to_input_page_view(self):
        found = resolve('/calculation/my_calculation/input/')
        self.assertEqual(found.func, input_page)

    def test_status_code(self):
        self.assertEqual(self.response.status_code, 200)

    def test_uses_template(self):
        self.assertTemplateUsed(self.response, 'input.html')

    def test_uses_request_method(self):
        self.assertEqual(self.response.request["REQUEST_METHOD"], "GET")

    def test_input_page_uses_form(self):
        self.assertIsInstance(self.response.context['form'],
                              CalculationForm)

    def test_wrong_calculation_name(self):
        resp = self.client.get('/calculation/incorrect_666/input/')
        self.assertEqual(resp.status_code, 404)

    # Test Context :
    # ==============

    # Test Content :
    # ==============
    def test_content_basic_items(self):
        self.assertIn("Main informations", self.response.content.decode())
        self.assertIn("Common inputs", self.response.content.decode())
        self.assertIn("Specific inputs", self.response.content.decode())

        self.assertIn("Solver", self.response.content.decode())
        # self.assertIn("Sections", self.response.content.decode())

    # Test POST REQUEST :
    # ===================
    def test_correct_post_request(self):
        resp = self.client.post('/calculation/cantilever_beam/input/',
                                {'name': "my calc",
                                 'solver': "1"})
        self.assertEqual(resp.status_code, 302)
        self.assertEqual(resp['Location'], '/calculation/result/1/')

    def test_empty_post_request(self):
        resp = self.client.post('/calculation/cantilever_beam/input/')
        self.assertEqual(resp.status_code, 200)
        self.assertContains(resp, escape(CONST["name"]["empty_error"]))
        self.assertContains(resp, escape(CONST["solver"]["empty_error"]))

    # def test_wrong_post_request(self):
    #     response = self.post_invalid_input()
    #     self.assertEqual(response.status_code, 200)
    #     self.assertContains(response, escape(EMPTY_NAME_ERROR))

    def test_for_invalid_input_passes_form_to_template(self):
        response = self.post_invalid_input()
        self.assertIsInstance(response.context['form'], CalculationForm)

    def test_for_invalid_input_nothing_saved_to_db(self):
        self.post_invalid_input()
        self.assertEqual(Calculation.objects.count(), 0)
