#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.conf.urls import url

from calculation import views

urlpatterns = [
    url(r'^$', views.input_page, name='calc_home'),
    url(r'^(?P<calc_name>.*)/input/$', views.input_page, name='calc_ipage'),
    url(r'^result/(?P<pk>[0-9]+)/$', views.result_page, name='calc_rpage'),
]
