#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.shortcuts import redirect, render, get_object_or_404
from django.template import RequestContext
from django.core.urlresolvers import reverse
from django.conf import settings

from calculation.models import CalculationDefinition, Calculation
from calculation.forms import CalculationForm
from solvers import run

import logging
logger = logging.getLogger(__name__)


def _calc_context(calculation_def):
    context = {
        "title": calculation_def.name.replace("_", " ").capitalize(),

        "activeMenuItem": "Calcul",
        "tags": calculation_def.tags.all(),
        "calc": calculation_def,
        "paths": (
            ("Calculation", reverse('calc_home')),
        ),
    }
    return context


def make_calculation_groups(calculation_def, form):
    groups = {'ungrouped': []}
    ParametersExist = False
    for field in form:
        if "ivar_" in field.name:
            ivar_name = field.name.replace('ivar_', '')
            ivar = calculation_def.ivars[ivar_name]
            if 'group' in ivar.keys():
                group = ivar['group']
                if not group in groups.keys():
                    groups[group] = [field]
                else:
                    groups[group].append(field)
            else:
                groups['ungrouped'].append(field)
            ParametersExist = True
    return groups, ParametersExist


def _input_context(calculation_def, form):
    """Function returning global context for the calculation."""

    # Recuperation des booleens
    thereIsNumber = False
    thereIsTorsor = False
    for value in calculation_def.ivars.values():
        if 'integer' == value['type'] or 'float' == value['type']:
            thereIsNumber = True
        if 'torsor' == value['type']:
            thereIsTorsor = True

    form.fields['name'].initial = 'default_name'

    solvers = calculation_def.solvers.all()
    if len(solvers) == 1:
        form.fields['solver'].initial = solvers[0]
    form.fields['solver'].queryset = solvers

    meshers = calculation_def.meshers.all()
    if len(meshers) > 0:
        thereIsMesher = True
        form.fields['mesher'].queryset = meshers
    else:
        thereIsMesher = False
    if len(meshers) == 1:
        form.fields['mesher'].initial = meshers[0]
    form.fields['mesher'].queryset = meshers

    groups, ParametersExist = make_calculation_groups(calculation_def, form)

    context = {
        "thereIsNumber": thereIsNumber,
        "thereIsTorsor": thereIsTorsor,
        "thereIsMesher": thereIsMesher,
        "groups": groups,
        "ParametersExist": ParametersExist,
    }

    context.update({'form': form})

    return context

def _render_image(FV_voile="19327 N", FH_voile="142 N", FV_jambe="982 N", FH_jambe="123 N"):
    from PIL import Image, ImageDraw, ImageFont
    path = settings.STATIC_ROOT + "/ferme.png"
    im = Image.open(path)
    draw = ImageDraw.Draw(im)
    draw.text((10,10), "Bonjour")
    del draw
    # write to stdout
    im.save(sys.stdout, "PNG")

def _result_context(calculation, form):
    """Function returning global context for the calculation."""

    calculation_def = calculation.definition

    # Recuperation des booleens
    thereIsNumber = False
    thereIsTorsor = False
    for value in calculation_def.ovars.values():
        if 'integer' == value['type'] or 'float' == value['type']:
            thereIsNumber = True
        if 'torsor' == value['type']:
            thereIsTorsor = True

    if calculation.mesher:
        thereIsMesher = True
    else:
        thereIsMesher = False

    context = {
        "thereIsNumber": thereIsNumber,
        "thereIsTorsor": thereIsTorsor,
        "thereIsMesher": thereIsMesher,
    }

    onumbers = {}
    otorsors = {}
    for field, value in calculation.definition.ovars.items():
        if 'float' in value['type'] or 'integer' in value['type']:
            onumbers[field] = calculation.ovars[field]
        elif 'torsor' in value['type']:
            otorsors[field] = calculation.ovars[field]

    context.update({'resu': calculation, 'onumbers': onumbers,
                    'otorsors': otorsors, 'form': form,
                    # "pdflatex": reverse('latex2pdf', kwargs={'pk': calculation.pk, 'resu_type': 'Calculation'})
                    })

    return context


def extract_ivars(cleaned_data):
    """Get input variables from cleaned data of the form."""

    # Delete keys which is not an input variable
    cleaned_data.pop('name')
    cleaned_data.pop('solver')
    cleaned_data.pop('mesher')

    # Get all input variables and clean their name
    ivars = {}
    for key, value in cleaned_data.items():
        if 'ivar_' in key:
            ivars[key.replace('ivar_', '')] = value

    return ivars


def get_calculation_or_run(calculation):
    logger.debug("inputs variables for calculation are : %s" % calculation.ivars)
    matches = Calculation.objects.filter(ivars=calculation.ivars)
    if matches:
        logger.info("Aucun calcul nécessaire, 'Calculation' avec les mêmes paramètres déjà existant !")
        calculation = matches[0]
    else:
        logger.info("Lancement de l'exécution du calcul : %s" % calculation)
        calculation.save()    # Sauvegarde préliminaire afin d'avoir un ID avant l'execution
        calculation.ovars, calculation.cpu_time = run.run(calculation)
        calculation.save()
    logger.debug("outputs variables for calculation are : %s" % calculation.ovars)
    return calculation


def input_page(request, calc_name=None):
    """Render input page."""
    calculation_def = get_object_or_404(CalculationDefinition, name=calc_name)

    form = CalculationForm(request.POST or None, ivars=calculation_def.ivars)
    if form.is_valid():
        fcalc = form.save(commit=False)
        fcalc.definition = calculation_def
        fcalc.ivars = extract_ivars(form.cleaned_data)
        fcalc = get_calculation_or_run(fcalc)
        return redirect('calc_rpage', pk=fcalc.pk)

    context = _calc_context(calculation_def)
    context.update(_input_context(calculation_def, form))

    return render(request, 'calculation_input_page.html', context)


def result_page(request, pk):
    """Render input page."""
    calculation = get_object_or_404(Calculation, pk=pk)

    form = CalculationForm(ovars=calculation.definition.ovars, ovars_result=calculation.ovars)

    context = _calc_context(calculation.definition)
    context.update(_result_context(calculation, form))

    return render(request, 'calculation_result_page.html', context)
