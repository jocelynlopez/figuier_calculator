#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django import forms
import django

from calculation import widgets as calc_widgets
from website.templatetags.utils import prettyname

VALIDATORS = {
    "min": django.core.validators.MinValueValidator,
    "max": django.core.validators.MaxValueValidator,
    "regex": django.core.validators.RegexValidator,

}


def getDjangoValidators(validators):
    D_validators = []
    for validator, value in validators.items():
        D_validators.append(VALIDATORS[validator](value))
    return D_validators


class BaseCalculationField(object):

    def __init__(self, *args, **kwargs):

        # Mandatory parameters
        self.name = kwargs.pop('name')
        self.type = kwargs.pop('type')
        self.description = kwargs.pop('description')

        # Optionnal parameters
        if 'group' in kwargs.keys():
            self.group = kwargs.pop('group')
        else:
            self.group = None

        if 'units' in kwargs.keys():
            self.units = kwargs.pop('units')
        else:
            self.units = None

        if 'validators' in kwargs.keys():
            kwargs['validators'] = getDjangoValidators(kwargs.pop('validators'))

        if 'choices' in kwargs.keys():
            kwargs['choices'] = [["", "---------"]] + kwargs['choices']

        if 'attrs' in kwargs.keys():
            self.attrs = kwargs.pop('attrs')
        else:
            self.attrs = None

        # Save needed parameters
        self.calculation = {"name": prettyname(self.name), "type": self.type,
                            "description": self.description, "units": prettyname(self.units)}

        return args, kwargs


class BooleanField(BaseCalculationField, forms.BooleanField):

    def __init__(self, *args, **kwargs):
        args, kwargs = BaseCalculationField.__init__(self, *args, **kwargs)
        kwargs['widget'] = calc_widgets.BooleanWidget(attrs=self.attrs,
                                                      calculation=self.calculation)
        forms.BooleanField.__init__(self, *args, **kwargs)


class ChoiceField(BaseCalculationField, forms.ChoiceField):

    def __init__(self, *args, **kwargs):
        args, kwargs = BaseCalculationField.__init__(self, *args, **kwargs)
        kwargs['widget'] = calc_widgets.ChoiceWidget(attrs=self.attrs,
                                                     calculation=self.calculation)
        forms.ChoiceField.__init__(self, *args, **kwargs)


class ChoiceWithValueField(BaseCalculationField, forms.ChoiceField):

    def __init__(self, *args, **kwargs):
        args, kwargs = BaseCalculationField.__init__(self, *args, **kwargs)
        kwargs['widget'] = calc_widgets.ChoiceValueWidget(attrs=self.attrs,
                                                          calculation=self.calculation)
        forms.ChoiceField.__init__(self, *args, **kwargs)


class ScalarFloatField(BaseCalculationField, forms.FloatField):

    def __init__(self, *args, **kwargs):
        args, kwargs = BaseCalculationField.__init__(self, *args, **kwargs)
        kwargs['widget'] = calc_widgets.ScalarFloatWidget(attrs=self.attrs,
                                                          calculation=self.calculation)
        forms.FloatField.__init__(self, *args, **kwargs)


class ScalarIntegerField(BaseCalculationField, forms.IntegerField):

    def __init__(self, *args, **kwargs):
        args, kwargs = BaseCalculationField.__init__(self, *args, **kwargs)
        kwargs['widget'] = calc_widgets.ScalarFloatWidget(attrs=self.attrs,
                                                          calculation=self.calculation)
        forms.IntegerField.__init__(self, *args, **kwargs)


def build_input_field(name, field_info):

    if field_info['type'] == 'float':
        field = ScalarFloatField(name=name,
                                 type=field_info['type'],
                                 description=field_info['description'],
                                 units=field_info['units'][0],
                                 validators=field_info['validators'],
                                 attrs={'class': 'form-control'})
    elif field_info['type'] == 'integer':
        field = ScalarIntegerField(name=name,
                                   type=field_info['type'],
                                   description=field_info['description'],
                                   units=field_info['units'][0],
                                   validators=field_info['validators'],
                                   attrs={'class': 'form-control'})
    elif field_info['type'] == 'boolean':
        field = BooleanField(name=name,
                             type=field_info['type'],
                             description=field_info['description'],
                             required=False,
                             attrs={'class': 'form-control'})
    elif field_info['type'] == 'choice':
        field = ChoiceField(name=name,
                            type=field_info['type'],
                            description=field_info['description'],
                            choices=field_info['choices'],
                            attrs={'class': 'form-control'})
    elif field_info['type'] == 'choicevalue':
        field = ChoiceWithValueField(name=name,
                                     type=field_info['type'],
                                     description=field_info['description'],
                                     units=field_info['units'][0],
                                     choices=field_info['choices'],
                                     attrs={'class': 'form-control',
                                            'onchange': 'SetValueForChoiceValueField(this)'})
    return field


def build_output_field(name, field_info, result):

    if field_info['type'] == 'float':
        field = ScalarFloatField(name=name,
                                 initial=result,
                                 type=field_info['type'],
                                 description=field_info['description'],
                                 units=field_info['units'][0],
                                 attrs={'class': 'form-control',
                                        'disabled': 'disabled'})
    elif field_info['type'] == 'integer':
        field = ScalarIntegerField(name=name,
                                   initial=result,
                                   type=field_info['type'],
                                   description=field_info['description'],
                                   units=field_info['units'][0],
                                   attrs={'class': 'form-control',
                                          'disabled': 'disabled'})
    elif field_info['type'] == 'boolean':
        field = ScalarIntegerField(name=name,
                                   initial=result,
                                   type=field_info['type'],
                                   description=field_info['description'],
                                   attrs={'class': 'form-control',
                                          'disabled': 'disabled'})
    return field
