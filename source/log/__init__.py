import os
import json
import logging.config

DEFAULT_PATH_CONF = os.path.join(os.path.dirname(__file__), "conf.json")


def setup_logging(path=DEFAULT_PATH_CONF, level=logging.INFO, env_key='LOG_CFG'):
    """Setup logging configuration."""
    value = os.getenv(env_key, None)
    if value:
        path = value
    if os.path.exists(path):
        with open(path, 'rt') as f:
            config = json.load(f)
        logging.config.dictConfig(config)
    else:
        logging.basicConfig(level=level)

setup_logging()
