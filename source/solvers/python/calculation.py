#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import logging
import shutil
from os.path import join, basename, dirname


from django.conf import settings

from solvers import utils


class PythonCalculation(utils.BaseCalculation):
    solver_type = 'python'

    def preprocess(self):
        logger = logging.getLogger(__name__)
        logger.debug("Complétion du script Python avec le contexte : %s" % self.context)

        python_script = join(self.workdir, self.name + '.py')
        utils.render_to_file(self.get_template(), python_script, self.context)

        # Copie du module calman
        orig_calman_module = join(settings.SOLVERS_ROOT, "python", "calman.py")
        dest_calman_module = join(self.workdir, "calman.py")
        shutil.copy(orig_calman_module, dest_calman_module)

        return python_script

    def run(self, version=None):
        python_script = self.preprocess()
        self.solver.run(python_script, version)
        return self.get_ovars()
