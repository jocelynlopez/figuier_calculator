#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.conf import settings

from solvers import utils


# def import_python_solver(module_solver_name):
#     """Import module named 'module_solver_name'."""
#     from importlib import import_module

#     # On sauvegarde le sys path puis on l'ecrase par le chemin des solvers python
#     # cela permet d'eviter l'importation de mauvais module
#     backup_sys_path = sys.path
#     solver_path = os.path.join(settings.PYTHON_SOLVERS, module_solver_name)

#     sys.path = [solver_path] + backup_sys_path

#     # Importation du python solver
#     solver = import_module(module_solver_name)

#     # On restaure le sys.path original
#     sys.path = backup_sys_path

#     return solver


# def run_python_solver(calculation):
#     """Run main function in the calculation module."""
#     module = import_python_solver(calculation.definition.name)
#     return module.main(**calculation.ivars)


class PythonLocalServer(object):

    def __init__(self, python_exe=None):
        """Build a Python solver in local."""
        if python_exe == None:
            self.python_exe = settings.PYTHON_EXECUTABLES
        else:
            self.python_exe = python_exe

    def run(self, python_script, version=None):
        """Run a python script."""
        # import json
        if version is not None:
            cmd = [self.python_exe[version], python_script]
        else:
            cmd = [self.python_exe[self.version], python_script]

        response = utils.run_local_command(cmd)
        # json_string = response.stdout
        # return json.loads(json_string)
        return response
