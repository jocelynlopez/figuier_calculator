#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Module de création de fichier .export pour Code_aster."""

import os


class AsterBaseFile(object):

    # Default values available inside ASTK or at $CODE_ASTER_ROOT/lib/astk/unites_logiques.tcl
    DEFAULT_VALUES = {
        'base': {'is_file': False, 'unit': 0, 'data': True, 'result': True, 'compressed': True},
        'bhdf': {'is_file': False, 'unit': 0, 'data': True, 'result': True, 'compressed': True},
        'ensi': {'is_file': True, 'unit': 0, 'data': True, 'result': False, 'compressed': False},
        'distr': {'is_file': True, 'unit': 0, 'data': True, 'result': False, 'compressed': False},
        'hostfile': {'is_file': True, 'unit': 0, 'data': True, 'result': False, 'compressed': False},
        'nom': {'is_file': True, 'unit': 0, 'data': False, 'result': True, 'compressed': False},
        'para': {'is_file': True, 'unit': 0, 'data': False, 'result': True, 'compressed': False},
        'btc': {'is_file': True, 'unit': 0, 'data': False, 'result': True, 'compressed': False},
        'flash': {'is_file': False, 'unit': 0, 'data': False, 'result': True, 'compressed': False},
        'repe': {'is_file': False, 'unit': 0, 'data': True, 'result': True, 'compressed': False},
        'comm': {'is_file': True, 'unit': 1, 'data': True, 'result': False, 'compressed': False},
        'mail': {'is_file': True, 'unit': 20, 'data': True, 'result': False, 'compressed': False},
        'erre': {'is_file': True, 'unit': 9, 'data': False, 'result': True, 'compressed': False},
        'mess': {'is_file': True, 'unit': 6, 'data': False, 'result': True, 'compressed': False},
        'resu': {'is_file': True, 'unit': 8, 'data': False, 'result': True, 'compressed': False},
        'cast': {'is_file': True, 'unit': 37, 'data': False, 'result': True, 'compressed': True},
        'mast': {'is_file': True, 'unit': 1, 'data': False, 'result': True, 'compressed': False},
        'mgib': {'is_file': True, 'unit': 19, 'data': True, 'result': False, 'compressed': False},
        'mmed': {'is_file': True, 'unit': 20, 'data': True, 'result': False, 'compressed': False},
        'msh': {'is_file': True, 'unit': 19, 'data': True, 'result': False, 'compressed': False},
        'msup': {'is_file': True, 'unit': 19, 'data': True, 'result': False, 'compressed': False},
        'datg': {'is_file': True, 'unit': 16, 'data': True, 'result': False, 'compressed': False},
        'pos': {'is_file': True, 'unit': 37, 'data': False, 'result': True, 'compressed': False},
        'dat': {'is_file': True, 'unit': 29, 'data': False, 'result': True, 'compressed': False},
        'ps': {'is_file': True, 'unit': 24, 'data': False, 'result': True, 'compressed': False},
        'agraf': {'is_file': True, 'unit': 25, 'data': False, 'result': True, 'compressed': False},
        'digr': {'is_file': True, 'unit': 26, 'data': False, 'result': True, 'compressed': False},
        'rmed': {'is_file': True, 'unit': 80, 'data': False, 'result': True, 'compressed': False},
        'unv': {'is_file': True, 'unit': 30, 'data': False, 'result': True, 'compressed': True},
        'libr': {'is_file': True, 'unit': 38, 'data': False, 'result': False, 'compressed': False},
        'erre': {'is_file': True, 'unit': 9, 'data': False, 'result': True, 'compressed': False},
    }

    def __init__(self, file_name, extension=None, unit=None, data=None, result=None, compressed=None):
        self.file_name = file_name

        if extension is not None:
            self.extension = extension
        else:
            self.extension = os.path.splitext(file_name)[1][1:]

        # Set default values and override if necessary
        self.set_default_values(**self.DEFAULT_VALUES[self.extension])
        if unit is not None:
            self.unit = unit
        if data is not None:
            self.data = data
        if result is not None:
            self.result = result
        if compressed is not None:
            self.compressed = compressed

        # save line for export file
        self.line = self.get_string()

    def set_default_values(self, is_file, unit, data, result, compressed):
        self.is_file = is_file
        self.unit = unit
        self.data = data
        self.result = result
        self.compressed = compressed

    def get_string(self):
        # Convert boolean value
        if self.is_file:
            F = 'F'
        else:
            F = 'R'

        if self.result:
            R = 'R'
        else:
            R = ''

        if self.data:
            D = 'D'
        else:
            D = ''

        if self.compressed:
            C = 'C'
        else:
            C = ''

        file_path = self.file_name

        # Build dict for formated string
        values = {
            "F": F,
            "extension": self.extension,
            "file_path": file_path,
            "D": D,
            "R": R,
            "C": C,
            "unit": self.unit,
        }
        return "{F} {extension} {file_path} {D}{R}{C} {unit}".format(**values)

    def __repr__(self):
        return self.get_string()


class AsterExportFile(object):

    # Parameters list available at D1.02.05 documentation of Code_Aster
    # mandatory parameters : actions, version
    DEFAULT_P = {
        "actions": "make_etude",  # Choix entre : astout, make_cmde, make_ele, make_env, make_etude, make_exec
        "classe": None,         # Nom du groupe de classe batch
        "consbtc": None,        # oui / non. Oui : pour construire (sans soumetttre) le fichier btc.
        "corefilesize": None,   # Taille des fichiers « core » (valeur ou « unlimited »)
        "cpresok": None,        # RESNOOK / RESOK : pour un astout : recopie des fichiers « out » pour les tests NOOK ou OK
        "debug": None,          # nodebug / debug : version « debug » d'aster
        "depart": None,         # Pour différer le lancement d'un calcul (syntaxe de la commande unix « at »).
        "detr_rep_trav": None,  # yes / no : faut-il détruire le répertoire de travail de l'exécution
        "display": None,        # Pour afficher le DISPLAY.
        "distrib": None,        # oui : le calcul est « distribué » (étude paramétrique)
        "exectool": None,       # Ligne de commande qui « encapsulera » l'exécution aster.
        "facmtps": None,        # facteur temps CPU des tests / à ce qui est écrit dans le fichier .para du test
        "follow_output": None,  # yes / no : pour demander (ou non) le suivi interactif de l'exécution.
        "mclient": None,        # Nom de la machine « client » (exemple :clautXXX.der.edf.fr)
        "mem_aster": None,      # Mémoire aster (MB pas en pourcentage de memjob). Exemple : 30.25
        "memjob": None,         # Mémoire total du job (MB pas en Ko). Exemple : 40000
        "mode": None,           # interactif / batch
        "mpi_nbcpu": None,      # Nombre de CPU de calcul pour une exécution en parallèle MPI
        "mpi_nbnoeud": None,    # Nombre de « noeuds » de calcul pour une exécution en parallèle MPI
        "nbmaxnook": None,      # Nombre maximum d'erreurs pour un astout. Défaut : 5
        "ncpus": None,          # Nombre de CPU de calcul pour une exécution en paralléle OpenMP
        "noeud": None,          # Nom du noeud de calcul
        "nomjob": None,         # Nom du « job »
        "origine": None,        # Nom de l'application ayant généré le fichier .export (ASTK 1.8.3 )
        "profastk": None,       # Nom du fichier .astk associé au fichier .export
        "rep_trav": None,       # Nom du répertoire de travail. Exemple : /local00/home/user/trav
        "serveur": None,        # Nom de la machine serveur de calcul.
        "soumbtc": None,        # oui / non : soumission (ou non) du fichier .btc
        "tpsjob": None,         # Temps maximal du job (mn)
        "uclient": None,        # Nom d'utilisateur (côté client)
        "username": None,       # Nom d'utilisateur
        "version": None,        # Version d'aster : NEW10, STA10_mpi, ...
        "xterm": None,          # Commande xterm
        "jobid": None,          # Job Id
        "aster_root": None,     # Chemin d'installation de Code_Aster
        "protocol_exec": 'asrun.plugins.server.SSHServer',
        "protocol_copyto": 'asrun.plugins.server.SCPServer',
        "protocol_copyfrom": 'asrun.plugins.server.SCPServer',
    }

    # Command line options list available at D1.02.05 documentation of Code_Aster
    # mandatory options : memjeveux, tpmax
    DEFAULT_A = {
        "memjeveux": 50,       # maximum size of the memory taken by the execution (in MB not in Mw)
        "memory": None,         # maximum size of the memory taken by the execution (in MB)
        "tpmax": 900,           # limit of the time of the execution (in seconds)
        "max_base": None,       # limit of the size of the results database
        "dbgjeveux": None,      # turn on some additional checkings in the memory management
        "num_job": None,        # job ID of the current execution
        "mode": None,           # execution mode (interactive or batch)
        "interact": None,       # it allows to enter commands after the execution of the command file.
        "rep_outils": None,     # directory of Code_Aster tools (ex. $ASTER_ROOT/outils)
        "rep_mat": None,        # directory of materials properties
        "rep_dex": None,        # directory of external datas (geometrical datas or properties...)
        "rep_glob": None,       # directory of the results database
        "rep_vola": None,       # directory of the temporary database
        "suivi_batch": None,    # force to flush the output after each line
        "totalview": None,      # required to run Code_Aster through the Totalview debugger
        "syntax": None,         # only check the syntax of the command file is done
    }

    def __init__(self, files, **kwargs):
        self.files = files

        # Set default parameters
        self.parameters = {}
        for key, value in self.DEFAULT_P.items():
            if value is not None:
                self.parameters[key] = value

        # Set default options
        self.options = {}
        for key, value in self.DEFAULT_A.items():
            if value is not None:
                self.options[key] = value

        # Set input values
        for key, value in kwargs.items():
            if key in self.DEFAULT_P.keys():
                self.parameters[key] = value
            elif key in self.DEFAULT_A.keys():
                self.options[key] = value
            else:
                raise ValueError("%s is not a valid options or parameters" % key)

        # Build content
        self.build()

    # def convert_memory_unit(self, memjeveux, mem_aster, memjob, ARCHITECTURE=64):
    #     self.AP['memjeveux'] = memjeveux * 8. / ARCHITECTURE
    #     self.AP['mem_aster'] = mem_aster / self.AP['memjob']
    #     self.AP['memjob'] = memjob * 1024

    def build(self):
        # self.convert_memory_unit(memjeveux, mem_aster, memjob, ARCHITECTURE=64)
        self.lines = []

        self.lines.append("# -----------")
        self.lines.append("# Parameters")
        self.lines.append("# -----------")
        for key, value in self.parameters.items():
            self.lines.append("P %s %s" % (key, value))

        self.lines.append("# -----------")
        self.lines.append("# Options")
        self.lines.append("# -----------")
        for key, value in self.options.items():
            self.lines.append("A %s %s" % (key, value))

        self.lines.append("# ------")
        self.lines.append("# Files")
        self.lines.append("# ------")
        for file in self.files:
            self.lines.append(file.line)

        return self.lines

    def writef(self, filepath):
        """Write Code_Aster export file at filepath."""
        with open(filepath, mode='w', encoding='utf-8') as f:
            f.write(self.writes())

    def writes(self):
        """Get content of the Code_Aster export file."""
        return '\n'.join(self.lines)

    def __repr__(self):
        return self.writes()
