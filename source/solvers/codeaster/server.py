#!/usr/bin/env python
# -*- coding: utf-8 -*-

from os.path import join

from django.conf import settings

from solvers.utils import run_local_command


class AsterLocalServer(object):

    def __init__(self, aster_root=None, **kwargs):
        """Build Code_Aster server."""
        self.host = 'localhost'

        # Don't set aster_root in default because solvers.settings is not import yet
        if aster_root is None:
            self.aster_root = settings.CODEASTER_LOCAL_ROOT
        else:
            self.aster_root = aster_root

        self._asrun_exe = join(self.aster_root, 'bin', 'as_run')

        self.cpu = None
        self.mem = None

    def get_infos(self):
        cmd = [self._asrun_exe, '--get_infos', self.host]
        infos = run_local_command(cmd, timeout=30)
        for line in infos.stdout:
            if 'cpu' in line:
                self.cpu = line.remove('cpu=')
            if 'mem' in line:
                self.mem = line.remove('mem=')

    def run(self, export_file, jobid=None):
        """build command for running calculation."""
        # Set JOBID if needed
        if jobid is not None:
            jobid_opt = '--num_job=%s' % jobid
            cmd = [self._asrun_exe, '--nodebug_stderr', jobid_opt, '--run', export_file]
        else:
            cmd = [self._asrun_exe, '--nodebug_stderr', '--run', export_file]
        return run_local_command(cmd)


class AsterRemoteServer(AsterLocalServer):

    def __init__(self, user=None, host=None, *args, **kwargs):
        super(AsterRemoteServer, self).__init__(*args, *kwargs)

        if (user and not host) or (not user and host):
            raise TypeError("user and host should be used together")
        else:
            self.user = user
            self.host = host

    def run(self, export_file, jobid=None):
        """build command for running calculation."""
        # Set JOBID if needed
        if self.jobid is not None:
            jobid_opt = '--num_job=%s' % self.jobid
            cmd = [self._asrun_exe, '--nodebug_stderr', '--proxy', jobid_opt, '--serv', export_file]
        else:
            cmd = [self._asrun_exe, '--nodebug_stderr', '--proxy', '--serv', export_file]
        return run_local_command(cmd)

    def get_resu(self, export_file):
        """build command for running calculation."""
        cmd = [self._asrun_exe, '--nodebug_stderr', '--proxy', '--get_results', export_file]
        return run_local_command(cmd)
