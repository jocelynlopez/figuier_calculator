#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import logging
from os.path import join, basename

from django.conf import settings

from solvers import utils
from solvers.codeaster.export import AsterBaseFile, AsterExportFile
from solvers.codeaster.server import AsterLocalServer, AsterRemoteServer


class AsterBaseCalculation(utils.BaseCalculation):
    solver_type = 'codeaster'

    def __init__(self, calculation, solver, mesh_file, **kwargs):
        super(AsterBaseCalculation, self).__init__(calculation, solver)

        self.mesh_file = mesh_file

        self.base_dir = join(self.workdir, 'base')
        self.export_file = join(self.workdir, self.name + '.export')

    def ifilepath(self, extension):
        """Get path for an input file of extension."""
        return join(self.workdir, self.name + '.' + extension)

    def ofilepath(self, extension):
        """Get path for an output file of extension."""
        return join(self.workdir, self.SUB_RESULT_DIR, self.name + '.' + extension)

    def build_export_file(self, version=None):
        """Build export file."""
        params = {
            "mode": "interactif",
            "aster_root": self.solver.aster_root,
            "nomjob": self.name,
            "jobid": self.jobid,
            "version": version
        }

        files = [
            # Input files
            AsterBaseFile(self.ifilepath('comm')),
            AsterBaseFile(self.mesh_file),
            # Output files
            AsterBaseFile(self.ofilepath('mess')),
            AsterBaseFile(self.ofilepath('erre')),
            AsterBaseFile(self.base_dir, extension='base', data=False),
            AsterBaseFile(self.ovars_file, extension='dat', unit=10),
        ]

        # Write export file
        export_file = AsterExportFile(files, **params)
        export_file.writef(self.export_file)

        return export_file

    def preprocess(self, version=None):
        """Create all necessary files for code_aster."""
        logger = logging.getLogger(__name__)
        logger.debug("Complétion du script Code_aster avec le contexte : %s" % self.context)

        # Command file creation from template comm file
        utils.render_to_file(self.get_template(), self.ifilepath('comm'), self.context)

        # Export file creation
        self.build_export_file(version=version)

    def run(self, version=None):
        """Run Code_Aster calculation."""
        # Build all files
        self.preprocess(version)

        # Execute Code_Aster calculation
        self.solver.run(self.export_file, jobid=self.jobid)

        # if server is remote we need to get results from remote server
        if isinstance(self.solver, AsterRemoteServer):
            self.solver.run(self.export_file)

        return self.get_ovars()
