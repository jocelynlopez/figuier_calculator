#!/usr/bin/env python
# -*- coding: utf-8 -*-

import subprocess

import os
from django.conf import settings
from django.template.loader import render_to_string


class BaseCalculation(object):

    solver_type = ''    # Need to be defined in subclass
    SUB_RESULT_DIR = 'result'
    SUB_SOLVER_DIR = 'solver'

    def __init__(self, calculation, solver):
        self.solver = solver
        self.context = dict(calculation.ivars)
        self.name = calculation.name
        self.calculation_name = calculation.definition.name
        self.jobid = str(calculation.pk)
        self.workdir = os.path.join(settings.RESULTS_ROOT, self.jobid, self.SUB_SOLVER_DIR)
        self.ovars_file = os.path.join(settings.RESULTS_ROOT, self.jobid, self.SUB_RESULT_DIR, 'ovars.json')

        # Directories creation
        if not os.path.exists(self.workdir):
            os.makedirs(self.workdir)
        if not os.path.exists(os.path.dirname(self.ovars_file)):
            os.makedirs(os.path.dirname(self.ovars_file))

        self.context.update({'ovars_file': self.ovars_file})

    def get_ovars(self):
        """Get results dict from ovars file."""
        import json
        with open(self.ovars_file, 'r') as ovars_file:
            return json.load(ovars_file)

    def get_template(self):
        if self.solver_type == 'python':
            extension = '.py'
        elif self.solver_type == 'codeaster':
            extension = '.comm'

        return os.path.join(os.path.basename(settings.SOLVERS_ROOT), self.solver_type,
                            self.calculation_name, self.calculation_name + extension)


def render_to_file(template, filename, context):
    open(filename, "w").write(render_to_string(template, context))


def run_local_command(cmd, timeout=600):
    """Run calculation on server."""
    return subprocess.run(cmd,
                          stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                          universal_newlines=True, timeout=timeout, check=True)
