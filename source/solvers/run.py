#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging

from solvers.codeaster.server import AsterLocalServer
from solvers.codeaster.calculation import AsterBaseCalculation
from meshers.salome.server import SalomeLocalServer
from meshers.salome.calculation import SalomeBaseCalculation
from solvers.python.server import PythonLocalServer
from solvers.python.calculation import PythonCalculation


def run(calculation):
    """Run calculation with the appropriate solver."""
    logger = logging.getLogger(__name__)

    from datetime import datetime
    start_time = datetime.now()

    # get mesher info
    if calculation.mesher:
        mesher = calculation.mesher.name
        mesher_type = mesher.split(' ')[0]
        mesher_version = mesher.split(' ')[1]
        # Mesh generation
        if mesher_type == 'Salome':
            salome_server = SalomeLocalServer()
            salome_calculation = SalomeBaseCalculation(calculation, salome_server)
            logger.info("Lancement de la génération du maillage avec Salome (id=%s)" % calculation.pk)
            salome_calculation.run(version=mesher_version)
            mesh = salome_calculation.mesh_file

    # get solver info
    solver = calculation.solver.name
    solver_type = solver.split(' ')[0]
    solver_version = solver.split(' ')[1]
    # Run calculation
    if solver_type == 'Python':
        python_server = PythonLocalServer()
        logger.debug("Création du calcul Python")
        python_calculation = PythonCalculation(calculation, python_server)
        logger.info("Lancement de la résolution du calcul Python (id=%s)" % calculation.pk)
        result = python_calculation.run(version=solver_version)

    elif solver_type == 'Code_aster':
        codeaster_server = AsterLocalServer()
        logger.debug("Création du calcul Code_aster avec le maillage : '%s'" % mesh)
        codeaster_calculation = AsterBaseCalculation(calculation, codeaster_server, mesh_file=mesh)
        logger.info("Lancement de la résolution du calcul Code_aster (id=%s)" % calculation.pk)
        result = codeaster_calculation.run(version=solver_version)

    cpu_time = datetime.now() - start_time

    return result, cpu_time
