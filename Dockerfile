FROM jocelynlopez/base-ca-salome-python:latest
MAINTAINER Jocelyn LOPEZ <jocelyn.lopez.pro@gmail.com>

# Copy python developments
# ------------------------
COPY ["database", "/DEV/figuier_calculator/database"]
COPY ["deployment", "/DEV/figuier_calculator/deployment"]
COPY ["logs", "/DEV/figuier_calculator/logs"]
COPY ["source", "/DEV/figuier_calculator/source"]
COPY ["static", "/DEV/figuier_calculator/static"]
COPY ["requirements.txt", "/DEV/figuier_calculator/requirements.txt"]
EXPOSE 8000

# Make virtualenv
# ---------------
#RUN virtualenv -p python3 /DEV/figuier_calculator/virtualenv && \
#    /DEV/figuier_calculator/virtualenv/bin/pip install -r /DEV/figuier_calculator/requirements.txt

# Clean database
# --------------
RUN ansible-playbook /DEV/figuier_calculator/deployment/dev.yaml

# Active the django server
# ------------------------
RUN chmod +x /DEV/figuier_calculator/source/manage.py
WORKDIR /DEV/figuier_calculator/source/
#ENTRYPOINT ["/DEV/figuier_calculator/virtualenv/bin/python", "manage.py", "runserver", "0.0.0.0:8000"]
