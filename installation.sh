#!/bin/bash

# System
sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get install git -y

# Prerequisites for calculator
sudo apt-get install python3 python3-pip texlive-latex-base texlive-lang-french texlive-pictures texlive-latex-extra -y
sudo apt-get install ansible virtualenv -y

# Installation de codeaster
sudo mkdir -p /DEV/figuier_calculator/softwares
sudo chown -R figuier:figuier /DEV/figuier_calculator/softwares
./deployment/install_prerequisites/solvers/codeaster.sh prerequisites
cd /DEV/figuier_calculator/aster-full-src-12.6.0/
python setup.py --aster_root=/DEV/figuier_calculator/softwares/codeaster -q --noprompt
cd /DEV/figuier_calculator
#./deployment/install_prerequisites/solvers/codeaster.sh install 12.7 /DEV/figuier_calculator/softwares/codeaster
./deployment/install_prerequisites/solvers/codeaster.sh macro CALC_CHAMP_COMB 12.6 /DEV/figuier_calculator/softwares/codeaster
./deployment/install_prerequisites/solvers/codeaster.sh macro IMPR_JSON 12.6 /DEV/figuier_calculator/softwares/codeaster

# Installation de Python
./deployment/install_prerequisites/solvers/python.sh prerequisites
./deployment/install_prerequisites/solvers/python.sh install 3.5.2 /DEV/figuier_calculator/softwares/python

# Installation de l'environnement virtuel
virtualenv -p python3 /DEV/figuier_calculator/virtualenv
/DEV/figuier_calculator/virtualenv/bin/pip install -r /DEV/figuier_calculator/requirements.txt

# Installation de Salom�
cd /DEV/figuier_calculator
./Salome-V7_8_0-public.run -l French -f -q -t /DEV/figuier_calculator/softwares/salome

# Initialisation du calculateur
ansible-playbook /DEV/figuier_calculator/deployment/dev.yaml
chmod +x /DEV/figuier_calculator/run_server.sh
(crontab -u $USER -l; echo "@reboot /DEV/figuier_calculator/run_server.sh" ) | crontab -u $USER -
chmod +x /DEV/figuier_calculator/run_client.sh
(crontab -u $USER -l; echo "@reboot /DEV/figuier_calculator/run_client.sh" ) | crontab -u $USER -