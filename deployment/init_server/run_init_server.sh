#!/bin/bash


echo "Avez-vous modifié le mot de passe root initial ?"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) break;;
        No ) exit;;
    esac
done

echo "Avez-vous installé python 2 sur les serveurs ? : 'sudo apt-get update && sudo apt-get upgrade && sudo apt-get install python-minimal -y'"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) break;;
        No ) exit;;
    esac
done

echo "Copie des clés SSH"
ssh-copy-id -i ~/.ssh/id_rsa.pub jocelyn@mechanical-oc.fr
echo "Execution de l'initialisation"
ansible-playbook init_server.yml -i init_hosts -k
