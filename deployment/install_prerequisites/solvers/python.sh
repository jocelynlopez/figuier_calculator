#!/usr/bin/env bash
# -*- coding: utf-8 -*-

function install_prerequisites()
{
    # system package
    sudo apt-get install wget libssl-dev openssl -y
}

function install_python()
{
    # Parameters
    PYTHON_VERSION=$1
    PYTHON_ROOT=$2

    # Download python interpreter
    cd /tmp
    wget http://www.python.org/ftp/python/$PYTHON_VERSION/Python-$PYTHON_VERSION.tgz
    tar -zxvf Python-$PYTHON_VERSION.tgz
    cd Python-$PYTHON_VERSION

    # Install python interpreter
    make clean
    mkdir -p $PYTHON_ROOT/$PYTHON_VERSION
    ./configure --prefix=$PYTHON_ROOT/$PYTHON_VERSION --with-ensurepip=install
    make
    make install

    # Raccourci python si python v3
    cd $PYTHON_ROOT/$PYTHON_VERSION/bin
    if [ -e "$PYTHON_ROOT/$PYTHON_VERSION/bin/python3" ]; then
      ln -s python3 python
    fi
}


USAGE=$(cat <<-END
Usage: `basename $0` [COMMAND] [OPTION]
Script for downloading and installing a specific version of Python.

Commands :
    prerequisites          install prerequisites of Python.
    install                install Python

Parameters :
    - prerequisites:
        syntax : `basename $0` prerequisites
    - install: 
        syntax : `basename $0` install version python_root
        - version            Python version to install.
        - python_root        Python install directory.

Options :
    -v                     Version of this script.
    -h                     Show this message.
END
)


# Parse command line options.
while getopts hvo: OPT; do
    case "$OPT" in
        h)
            echo "$USAGE"
            exit 0
            ;;
        v)
            echo "`basename $0` version 0.1"
            exit 0
            ;;
        \?)
            # getopts issues an error message
            echo "$USAGE" >&2
            exit 1
            ;;
    esac
done

# Remove the switches we parsed above.
shift `expr $OPTIND - 1`

# We want at least one non-option argument. 
# Remove this block if you don't need it.
if [ $# -eq 0 ]; then
    echo "$USAGE" >&2
    exit 1
fi

# Access additional arguments as usual through 
# variables $@, $*, $1, $2, etc. or using this loop:
if [ "$1" = "prerequisites" ]; then

    install_prerequisites

elif [ "$1" = "install" ]; then

    if [[ "$2" != "" && "$3" != "" ]]; then
        install_python $2 $3
        exit 0
    else
        echo "version and python_root parameter are mandatory"
        echo "$USAGE" >&2
        exit 1
    fi


else
    echo "command '$1' unknown"
    echo "$USAGE" >&2
    exit 1
fi
