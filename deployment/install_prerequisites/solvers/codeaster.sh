#!/usr/bin/env bash
# -*- coding: utf-8 -*-

function post_infos()
{
INFO=$(cat <<-END

For client/server use. the command : user@host needed to work !
So if port is different than default, you need to configure ~/.ssh/config

example :
Host my_cluster.com
    HostName my_cluster.com
    Port 28719
    User john
END
)

echo "$INFO"
}

function install_prerequisites()
{
    # system package
    sudo apt-get install wget -y

    # packages prerequisites
    sudo apt-get install gcc g++ gfortran python python-dev python-numpy python-qt4 tk bison -y
    sudo apt-get install flex liblapack-dev zlib1g-dev -y

    # softwares prerequisites
    sudo apt-get install gnuplot cmake ddd vim-gnome-py2 geany nedit grace python-matplotlib -y
}

function install_code_aster()
{
    # Parameters
    CODEASTER_VERSION=$1
    CODEASTER_ROOT=${2}

    echo "Installation de la version $CODEASTER_VERSION de Code_aster dans '$CODEASTER_ROOT'..."
    sleep 2

    # Download Code_aster
    cd /tmp
    wget http://web-code-aster.org/FICHIERS/aster-full-src-$CODEASTER_VERSION.0-1.noarch.tar.gz
    tar -zxvf aster-full-src-$CODEASTER_VERSION.0-1.noarch.tar.gz
    cd /tmp/aster-full-src-$CODEASTER_VERSION.0

    # Install Code_aster
    python2 setup.py install --prefix=$CODEASTER_ROOT --noprompt --quiet
}

function install_dev_macro()
{
    # Parameters
    MACRO=${1}
    CODEASTER_VERSION=${2}
    CODEASTER_ROOT=${3}

    MACRO_ARCHIVE="https://bitbucket.org/scienticod/$MACRO/get/HEAD.zip"

    echo "Installation de la macro $MACRO dans Code_aster v$CODEASTER_VERSION..."
    sleep 2

    cd /tmp

    # Check if HEAD.zip file already exist and delete if true
    if [ -e "/tmp/HEAD.zip" ]; then
      rm -f "/tmp/HEAD.zip"
    fi

    # Download macro archive
    wget -q $MACRO_ARCHIVE

    # If an error occur when downloading stop script
    if [ $? != 0 ]; then
      echo "An error occur when downloading macro from : '$MACRO_ARCHIVE'"
      exit 1
    fi

    # Check if destination folder exist and delete if true
    if [ -d "/tmp/codeaster-$MACRO-master" ]; then
      rm -rf "/tmp/codeaster-$MACRO-master"
    fi

    # Decompress macro archive
    unzip -q "HEAD.zip" -d "codeaster-$MACRO-master"

    # If an error occur when unzip macro archive stop script
    if [ $? != 0 ]; then
      echo "An error occur unzipping macro archive"
      exit 1
    fi

    # Install macro in Code_aster
    cd /tmp/codeaster-$MACRO-master/*/.

    CAT_FILE="${MACRO,,}.capy"
    CAT_OFILE="$CODEASTER_ROOT/$CODEASTER_VERSION/lib/aster/Cata/cata.py"

    OPS_FILE="${MACRO,,}_ops.py"
    OPS_OFILE="$CODEASTER_ROOT/$CODEASTER_VERSION/lib/aster/Macro/$OPS_FILE"

    MSG_FILE="${MACRO,,}.py"
    MSG_OFILE="$CODEASTER_ROOT/$CODEASTER_VERSION/lib/aster/Messages/$MSG_FILE"

    # Check if ops file already exist and install it if true
    if [ -e $OPS_FILE ]; then
        if [ -e $OPS_OFILE ]; then
            echo "Becareful $OPS_FILE already exist in Code_aster install directory !"
        else
            cp $OPS_FILE $OPS_OFILE
        fi
    fi

    # Check if message file already exist and delete if true
    if [ -e $MSG_FILE ]; then
        if [ -e $MSG_OFILE ]; then
            echo "Becareful $MSG_FILE already exist in Code_aster install directory !"
        else
            cp $MSG_FILE $MSG_OFILE
        fi
    fi

    # Check if ops.py file already exist and delete if true
    if [ -e $CAT_FILE ]; then
        cat $CAT_FILE >> $CAT_OFILE
    fi

    echo "Macro $MACRO successful installed."
}


USAGE=$(cat <<-END
Usage: `basename $0` [COMMAND] [OPTION]
Script for downloading and installing a specific version of Code_aster.

Commands :
    prerequisites          install prerequisites of Code_aster.
    install                install Code_aster

Parameters :
    - prerequisites:
        syntax : `basename $0` prerequisites
    - install: 
        syntax : `basename $0` install version as_root
        - version            Code_aster version to install.
        - as_root            Code_aster install directory.
    - macro:
        syntax : `basename $0` macro name version as_root
        - name               Macro name
        - version            Code_aster version to install.
        - as_root            Code_aster install directory.

Options :
    -v                     Version of this script.
    -h                     Show this message.
END
)


# Parse command line options.
while getopts hvo: OPT; do
    case "$OPT" in
        h)
            echo "$USAGE"
            exit 0
            ;;
        v)
            echo "`basename $0` version 0.1"
            exit 0
            ;;
        \?)
            # getopts issues an error message
            echo "$USAGE" >&2
            exit 1
            ;;
    esac
done

# Remove the switches we parsed above.
shift `expr $OPTIND - 1`

# We want at least one non-option argument. 
# Remove this block if you don't need it.
if [ $# -eq 0 ]; then
    echo "$USAGE" >&2
    exit 1
fi

# Access additional arguments as usual through 
# variables $@, $*, $1, $2, etc. or using this loop:
if [ "$1" = "prerequisites" ]; then

    install_prerequisites

elif [ "$1" = "install" ]; then

    if [[ "$2" != "" && "$3" != "" ]]; then
        install_code_aster $2 $3
        post_infos
        exit 0
    else
        echo "version and aster_root parameter are mandatory"
        echo "$USAGE" >&2
        exit 1
    fi

elif [ "$1" = "macro" ]; then

    if [[ "$2" != "" && "$3" != "" && "$4" != "" ]]; then
        install_dev_macro $2 $3 $4
        exit 0
    else
        echo "name, version and aster_root parameter are mandatory"
        echo "$USAGE" >&2
        exit 1
    fi

else
    echo "command '$1' unknown"
    echo "$USAGE" >&2
    exit 1
fi