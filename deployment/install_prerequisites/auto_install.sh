#!/usr/bin/env bash
# -*- coding: utf-8 -*-

# ---------------------------------------------
# Configuration de l'installation
# ---------------------------------------------
ASTER_ROOT="/home/$USER/codeaster"
ASTER_VERSIONS=( "12.6" "13.2" )
ASTER_MACROS=( "IMPR_JSON" "CALC_CHAMP_COMB" )

PYTHON_ROOT="/home/$USER/python"
PYTHON_VERSIONS=( "2.7.12" "3.5.2" )
# ---------------------------------------------


# Install Code_aster solver
./solvers/codeaster.sh prerequisites
for as_version in "${ASTER_VERSIONS[@]}"
do
    echo "Install Code_aster v$as_version"
    ./solvers/codeaster.sh install $as_version $ASTER_ROOT
done

# Install macro for Code_aster
for as_version in "${ASTER_VERSIONS[@]}"
do
    for as_macro in "${ASTER_MACROS[@]}"
    do
        echo "Install macro $as_macro pour Code_aster v$as_version"
        ./solvers/codeaster.sh macro $as_macro $as_version $ASTER_ROOT
    done
done

# Install python solver
./solvers/python.sh prerequisites
for py_version in "${PYTHON_VERSIONS[@]}"
do
    echo "Install Python v$py_version"
    ./solvers/python.sh install $py_version $PYTHON_ROOT
done


