---
- hosts: prod1
  remote_user: jocelyn
  become: yes
  become_method: sudo

  vars:
    project_root: '/home/{{ ansible_ssh_user }}/sites/{{ ansible_host }}'
    git_repo: 'git@bitbucket.org:negetem/calman-src.git'
    git_branch: 'master'
    git_repo_database: 'git@bitbucket.org:negetem/calman-database.git'
    git_branch_database: 'master'
    git_repo_deployment: 'git@bitbucket.org:negetem/calman-deployment.git'
    git_branch_deployment: 'master'
    admin: 'jocelyn'
    email: 'jocelyn.lopez60@gmail.com'

  tasks:

    ###################
    # Nginx
    ###################

    - name: Remove default site config
      file: path='/etc/nginx/{{ item }}/default'
            state=absent
      with_items:
        - sites-enabled
        - sites-available

    - name: Write nginx.conf
      action: template src=templates/nginx.conf.j2 dest=/etc/nginx/{{ item }}/{{ ansible_host }}.conf
      with_items:
        - sites-enabled
        - sites-available
      notify:
        - restart nginx

    - name: Start nginx service
      service: name=nginx state=started enabled=true

    ###################
    # Deploying Calman
    ###################

    - name: Creates directories
      file: path={{ project_root }}/{{ item }} state=directory owner={{ ansible_ssh_user }}
      with_items:
        - logs
        - database
        - source
        - static
        - virtualenv

    - name: Git clone/pull calman-src from bitbucket
      git: repo={{ git_repo }}
           version={{ git_branch }}
           dest={{ project_root }}/source
           key_file='/home/{{ ansible_ssh_user }}/.ssh/id_rsa'
           accept_hostkey=yes
           force=yes
      become_user: jocelyn

    - name: Git clone/pull calman-database from bitbucket
      git: repo={{ git_repo_database }}
           version={{ git_branch_database }}
           dest={{ project_root }}/database
           key_file='/home/{{ ansible_ssh_user }}/.ssh/id_rsa'
           accept_hostkey=yes
           force=yes
      become_user: jocelyn

    - name: Git clone/pull calman-deployment from bitbucket
      git: repo={{ git_repo_deployment }}
           version={{ git_branch_deployment }}
           dest={{ project_root }}/deployment
           key_file='/home/{{ ansible_ssh_user }}/.ssh/id_rsa'
           accept_hostkey=yes
           force=yes
      become_user: jocelyn

    - name: Create/Update virtualenv with requirements
      pip: requirements={{ project_root }}/source/requirements.txt
           virtualenv={{ project_root }}/virtualenv
      remote_user: "{{ ansible_ssh_user }}"

    - name: Setup Gunicorn config
      template: src=templates/gunicorn.conf.py.j2
                dest={{ project_root }}/logs/gunicorn.conf.py
      remote_user: "{{ ansible_ssh_user }}"
      notify: restart supervisor

    - name: Setup supervisor config (to manage gunicorn)
      template: src=templates/supervisor.conf.j2
                dest=/etc/supervisor/conf.d/{{ ansible_host }}.conf
      notify: restart supervisor

    - name: Setup Nginx config for our app
      template: src=templates/nginx.conf.j2
                dest=/etc/nginx/sites-enabled/{{ ansible_host }}.conf
      notify: restart nginx

    ######################################
    # Run django manage operation
    ######################################

    - name: Activate permission to manage.py
      file: path={{ project_root }}/source/manage.py state=touch mode="u+rwx,g+rx,o+rx"

    - name: Remove database
      file: path='{{ project_root }}/database/calman.sqlite'
            state=absent

    - name: Remove results
      file: path='{{ project_root }}/results'
            state=absent

    - name: Launch collectstatic
      django_manage: >
          command='collectstatic --noinput'
          app_path={{ project_root }}/source
          virtualenv={{ project_root }}/virtualenv

    - name: Launch migrations
      django_manage: >
          command={{ item }}
          app_path={{ project_root }}/source
          virtualenv={{ project_root }}/virtualenv
      with_items:
        - flush --noinput
        - makemigrations --noinput
        - migrate --noinput
        - migrate --run-syncdb --noinput

    - name: Load independent basic fixtures
      django_manage: >
          command={{ item }}
          app_path={{ project_root }}/source
          virtualenv={{ project_root }}/virtualenv
      with_items:
        - loaddata categories.yaml
        - loaddata solvers.yaml
        - loaddata meshers.yaml
        - loaddata tags.yaml

    - name: Load dependent basic fixtures
      django_manage: >
          command={{ item }}
          app_path={{ project_root }}/source
          virtualenv={{ project_root }}/virtualenv
      with_items:
        - loaddata calculations.json
        - loaddata study_calculation_definition.json
        - loaddata study_definition.json

    # - name: Load independent section fixtures
    #   django_manage: >
    #       command={{ item }}
    #       app_path={{ project_root }}/source
    #       virtualenv={{ project_root }}/virtualenv
    #   with_items:
    #     - loaddata sections/Category.yaml
    #     - loaddata sections/ShapeType.yaml

    # - name: Load dependent section fixtures
    #   django_manage: >
    #       command={{ item }}
    #       app_path={{ project_root }}/source
    #       virtualenv={{ project_root }}/virtualenv
    #   with_items:
    #     - loaddata sections/Catalog.yaml
    #     - loaddata sections/Shape.yaml
    #     - loaddata sections/WOODPRO.yaml
    #     - loaddata sections/RCATPRO.yaml

    # - name: Load independent materials fixtures
    #   django_manage: >
    #       command={{ item }}
    #       app_path={{ project_root }}/source
    #       virtualenv={{ project_root }}/virtualenv
    #   with_items:
    #     - loaddata materials/Catalog.yaml
    #     - loaddata materials/Category.yaml

    # - name: Load dependent materials fixtures
    #   django_manage: >
    #       command={{ item }}
    #       app_path={{ project_root }}/source
    #       virtualenv={{ project_root }}/virtualenv
    #   with_items:
    #     - loaddata materials/Rmat001.yaml

    - name: Set permission 666 to database
      file: path='{{ project_root }}/database/calman.sqlite' state=touch mode="u+rw,g+rw,o+rw"

    - name: Set permission 666 to info.log
      file: path='{{ project_root }}/logs/info.log' state=touch mode="u+rw,g+rw,o+rw"

    - name: Set permission 666 to debug.log
      file: path='{{ project_root }}/logs/debug.log' state=touch mode="u+rw,g+rw,o+rw"

    - name: Set permission 666 to errors.log
      file: path='{{ project_root }}/logs/errors.log' state=touch mode="u+rw,g+rw,o+rw"

    # - name: Create superuser
    #   django_manage: command='createsuperuser --noinput --username={{ admin }} --email={{ email }}'
    #   become_user: root

    - name: restart supervisor
      service: name=supervisor state=restarted enabled=yes

    - name: restart nginx
      service: name=nginx state=restarted enabled=yes

    - pause: minutes=2

  ###################
  # Handlers
  ###################
  handlers:
    - name: reload iptables
      action: shell /sbin/iptables-restore < /etc/iptables.up
    - name: update tzdata
      command: /usr/sbin/dpkg-reconfigure --frontend noninteractive tzdata

