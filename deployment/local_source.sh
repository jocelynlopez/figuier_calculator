git clone git@bitbucket.org:negetem/calman-deployment.git deployment
git clone git@bitbucket.org:negetem/calman.git source
virtualenv --python=python3.5 virtualenv
source virtualenv/bin/activate
pip install -r source/requirements.txt
echo 'alias calman="source virtualenv/bin/activate & cd source"' >> ~/.bashrc
