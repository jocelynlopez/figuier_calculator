#!/bin/bash

cd /DEV/figuier_calculator/source
source /DEV/figuier_calculator/virtualenv/bin/activate
python /DEV/figuier_calculator/source/manage.py runserver 0.0.0.0:8000