#!/usr/bin/env python
# -*- coding: utf-8 -*-

from math import *

import calman


@calman.dump("{{ovars_file}}")
def main():

    # Parametres
    force_surfacique_couverture = {{couverture}} * 9.81
    n_pannes = {{n_pannes}}
    S_pannes = {{S_pannes}} * 1e-6
    S_pannes_sabliere = {{S_pannes_sabliere}} * 1e-6
    S_arbaletrier = {{S_arbaletrier}} * 1e-6
    S_entrait = {{S_entrait}} * 1e-6
    S_liteau = {{S_liteau}} * 1e-6
    e_liteau = {{e_liteau}} * 1e-2
    S_chevrons = {{S_chevrons}} * 1e-6
    e_chevrons = {{e_chevrons}} * 1e-2
    ep_laine = {{ep_laine}} * 1e-3

    classe = {{classe}}
    p_toit = {{p_toit}}
    L_ferme = {{L_ferme}}
    L_maison = {{L_maison}}
    n_fermes = {{n_fermes}}
    m_divers = {{m_divers}}

    # -----------------------
    # Calcul du poids prore :
    # -----------------------
    l_toiture = (L_ferme / cos(radians(p_toit)))
    surface = l_toiture * L_maison
    force_volumique_bois = classe * 9.81
    force_surfacique_BA13 = 12 * 9.81
    force_volumique_laine_de_roche = 55 * 9.81

    PP_couverture = force_surfacique_couverture * surface
    PP_pannes = force_volumique_bois * S_pannes * L_maison * n_pannes
    PP_pannes_sabliere = force_volumique_bois * S_pannes_sabliere * L_maison * 2
    PP_arbaletrier = force_volumique_bois * S_arbaletrier * l_toiture * n_fermes
    PP_entrait = force_volumique_bois * S_entrait * L_ferme * n_fermes
    PP_liteau = force_volumique_bois * S_liteau * L_maison * (l_toiture / e_liteau)
    PP_chevrons = force_volumique_bois * S_chevrons * l_toiture * (L_maison / e_chevrons)
    PP_l_roche = force_volumique_laine_de_roche * ep_laine * surface
    PP_BA13 = force_surfacique_BA13 * surface
    PP_divers = m_divers * 9.81

    PP_toit = PP_couverture + PP_pannes + PP_pannes_sabliere + PP_arbaletrier
    PP_toit += PP_entrait + PP_liteau + PP_chevrons + PP_l_roche + PP_BA13 + PP_divers

    m_PP = PP_toit / 9.81 / surface

    # ------------------------------
    # Calcul de la charge de neige :
    # ------------------------------
    # Valeur caracteristique de la charge de neige au sol :
    Sk = 0.45   # kN/m^2
    # Coefficient de forme de toiture :
    mu_1 = 0.8 * (60 - p_toit) / 30.
    # Coefficient d'exposition :
    Ce = 1.25    # (site protege : penalisant)
    # Coefficient thermique :
    Ct = 1      # (pas de vitrage : penalisant)
    # Charge de neige
    S = Sk * mu_1 * Ce * Ct     # kN/m^2
    m_NEIGE = S * 1000 / 9.81

    # ------------------------------
    # Calcul de la charge du vent :
    # ------------------------------
    # Base de la vitesse de reference (region 2) :
    V_b0 = 24       # m/s
    # Reduction fct. Saison :
    C_season = 1
    # Reduction dir. Vent :
    C_dir = 1       # penalisant
    # Coefficient de probabilite :
    C_prob = 1      # penalisant
    # Vitesse de reference :
    V_b = V_b0 * C_season * C_dir
    # Pression dynamique de reference :
    q_b = 0.5 * 1.25 * V_b ** 2     # N/m^2
    # Facteurs de terrain :
    #k_r_IIIa = 0.19*(0.2/0.05)**0.07     # Terrain IIIa
    k_r_IV = 0.19*(1/0.05)**0.07         # Terrain IV
    # Hauteur du batiment
    z = 15.0
    # Coefficient de rugosite :
    C_r_z = k_r_IV * log(z/0.2)	# Penalisant terrain IIIa (I et II exclus)
    # Coefficient d'orographie :
    C_0_z = 1
    # Vitesse moyenne du vent :
    V_m_z = V_b * C_r_z * C_0_z
    # Coefficient de turbulence :
    k_I = 1				# Valeur recommandee et penalisante (Annexe nationale non penalisante)
    # Ecart type de la turbulence :
    sigma_v = V_b * k_I * k_r_IV	# Penalisant terrain IV
    # Intensite de la turbulence :
    l_v_z = sigma_v / V_m_z
    # Pression dynamique de pointe :
    q_p_z = (1 + 7 * l_v_z) * 0.5 * 1.25 * V_m_z**2      # N/m^2
    # Coefficient structural :
    C_s_C_d = 1
    # Coefficient de pression exterieur :
    C_pe = 1.0
    # Pression aerodynamique exterieur :
    W_e = q_p_z * C_pe

    m_VENT = C_s_C_d * W_e / 9.81

    return {'m_PP': m_PP, "m_NEIGE": m_NEIGE, "m_VENT": m_VENT}

if __name__ == '__main__':
    main()
