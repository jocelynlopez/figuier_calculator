#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json


def dump(path):
    def decorated(func):
        def wrapper(*args, **kwargs):
            response = func(*args, **kwargs)
            if isinstance(response, dict):
                with open(path, 'w') as ovars_file:
                    ovars_file.write(json.dumps(response))
            return response
        return wrapper
    return decorated
