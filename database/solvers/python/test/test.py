#!/usr/bin/env python
# -*- coding: utf-8 -*-

import calman


@calman.dump("{{ovars_file}}")
def main():

    Fz = {{Fz}}
    L = {{L}}
    E = {{E}}
    I_Gz = {{I_Gz}}

    fleche = Fz * (L * 1000)**3 / (48 * E * I_Gz)

    return {'f': fleche}

if __name__ == '__main__':
    main()
