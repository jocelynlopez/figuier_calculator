# Librairies standard
# -------------------
from math import *

# Modules salome
# --------------
import salome
import GEOM
import SMESH
from salome.geom import geomBuilder
from salome.smesh import smeshBuilder

# Initialisation
# --------------
salome.salome_init()
theStudy = salome.myStudy
smesh = smeshBuilder.New(theStudy)
gp = geomBuilder.New(theStudy)


# ---------------
# Corps du script
# ---------------

# Variables :
# -----------
L_ferme = {{L_ferme}}
H_poincon = {{H_poincon}}
l_blochet = {{l_blochet}}
p_toit = {{p_toit}}
a_jambe = {{a_jambe}}
H_murd = {{H_murd}}
H_murg = {{H_murg}}

# Variables dependantes :
# ---------------------
L_ferme_droit = (L_ferme - (H_murd - H_murg) / tan(radians(p_toit)))/2
L_ferme_gauche = L_ferme_droit + (H_murd - H_murg) / tan(radians(p_toit))

a_DA = -tan(radians(p_toit))
b_DA = L_ferme_droit * tan(radians(p_toit))
b_DAbis = L_ferme_gauche * tan(radians(p_toit))
a_CD = -1 / tan(radians(a_jambe))
b_CD = (L_ferme_droit - l_blochet) / tan(radians(a_jambe))
b_CDbis = (L_ferme_gauche - l_blochet) / tan(radians(a_jambe))
x_sol = (b_CD - b_DA) / (a_DA - a_CD)
z_sol = a_DA * x_sol + b_DA

x_solbis = (b_CDbis - b_DAbis) / (a_DA - a_CD)
z_solbis = a_DA * x_solbis + b_DAbis

# Generation de la geometrie :
# ----------------------------
OZ = gp.MakeVectorDXDYDZ(0, 0, 1)

A = gp.MakeVertex(L_ferme_droit, 0, 0)
B = gp.MakeVertex(L_ferme_droit - l_blochet + H_murd * tan(radians(a_jambe)), 0, -H_murd)
C = gp.MakeVertex(L_ferme_droit - l_blochet, 0, 0)
D = gp.MakeVertex(x_sol, 0, z_sol)
E = gp.MakeVertex(H_poincon / tan(radians(p_toit)), 0, L_ferme_droit * tan(radians(p_toit)) - H_poincon)
F = gp.MakeVertex(0, 0, L_ferme_droit * tan(radians(p_toit)))
G = gp.MakeVertex(0, 0, L_ferme_droit * tan(radians(p_toit)) - H_poincon)

#Abis = gp.MakeMirrorByAxis(A, OZ)
#Bbis = gp.MakeMirrorByAxis(B, OZ)
#Cbis = gp.MakeMirrorByAxis(C, OZ)
#Dbis = gp.MakeMirrorByAxis(D, OZ)
Ebis = gp.MakeMirrorByAxis(E, OZ)

Abis = gp.MakeVertex(-L_ferme_gauche, 0, H_murg - H_murd)
Bbis = gp.MakeVertex(-(L_ferme_gauche - l_blochet + H_murg * tan(radians(a_jambe))), 0, -H_murd)
Cbis = gp.MakeVertex(-L_ferme_gauche + l_blochet, 0, H_murg - H_murd)
Dbis = gp.MakeVertex(-x_solbis, 0, z_solbis-(H_murd-H_murg))

gp.addToStudy(A, 'A')
gp.addToStudy(B, 'B')
gp.addToStudy(C, 'C')
gp.addToStudy(D, 'D')

gp.addToStudy(Abis, 'Abis')
gp.addToStudy(Bbis, 'Bbis')
gp.addToStudy(Cbis, 'Cbis')
gp.addToStudy(Dbis, 'Dbis')

L_B_C = gp.MakeLineTwoPnt(B, C)
L_C_D = gp.MakeLineTwoPnt(C, D)
L_C_A = gp.MakeLineTwoPnt(C, A)
L_A_D = gp.MakeLineTwoPnt(A, D)
L_D_E = gp.MakeLineTwoPnt(D, E)
L_E_F = gp.MakeLineTwoPnt(E, F)
L_G_E = gp.MakeLineTwoPnt(G, E)
L_F_G = gp.MakeLineTwoPnt(F, G)
L_Bbis_Cbis = gp.MakeLineTwoPnt(Bbis, Cbis)
L_Cbis_Dbis = gp.MakeLineTwoPnt(Cbis, Dbis)
L_Cbis_Abis = gp.MakeLineTwoPnt(Cbis, Abis)
L_Abis_Dbis = gp.MakeLineTwoPnt(Abis, Dbis)
L_Dbis_Ebis = gp.MakeLineTwoPnt(Dbis, Ebis)
L_Ebis_F = gp.MakeLineTwoPnt(Ebis, F)
L_G_Ebis = gp.MakeLineTwoPnt(G, Ebis)

l_lines = [L_B_C, L_C_D, L_C_A, L_A_D, L_D_E, L_E_F, L_G_E,
           L_F_G, L_Bbis_Cbis, L_Cbis_Dbis, L_Cbis_Abis,
           L_Abis_Dbis, L_Dbis_Ebis, L_Ebis_F, L_G_Ebis]
GEO_FERME = gp.MakeCompound(l_lines)
gp.addToStudy(GEO_FERME, 'GEO_FERME')


ARBALETR = gp.CreateGroup(GEO_FERME, gp.ShapeType["EDGE"])
l_ID_ARBALETR = []
l_ID_ARBALETR += gp.GetSameIDs(GEO_FERME, L_A_D)
l_ID_ARBALETR += gp.GetSameIDs(GEO_FERME, L_D_E)
l_ID_ARBALETR += gp.GetSameIDs(GEO_FERME, L_E_F)
l_ID_ARBALETR += gp.GetSameIDs(GEO_FERME, L_Ebis_F)
l_ID_ARBALETR += gp.GetSameIDs(GEO_FERME, L_Dbis_Ebis)
l_ID_ARBALETR += gp.GetSameIDs(GEO_FERME, L_Abis_Dbis)
gp.UnionIDs(ARBALETR, l_ID_ARBALETR)
gp.addToStudyInFather(GEO_FERME, ARBALETR, 'ARBALETR')

A_AUVENT = gp.CreateGroup(GEO_FERME, gp.ShapeType["EDGE"])
l_ID_A_AUVENT = []
l_ID_A_AUVENT += gp.GetSameIDs(GEO_FERME, L_Ebis_F)
l_ID_A_AUVENT += gp.GetSameIDs(GEO_FERME, L_Dbis_Ebis)
l_ID_A_AUVENT += gp.GetSameIDs(GEO_FERME, L_Abis_Dbis)
gp.UnionIDs(A_AUVENT, l_ID_A_AUVENT)
gp.addToStudyInFather(GEO_FERME, A_AUVENT, 'A_AUVENT')

A_SSVENT = gp.CreateGroup(GEO_FERME, gp.ShapeType["EDGE"])
l_ID_A_SSVENT = []
l_ID_A_SSVENT += gp.GetSameIDs(GEO_FERME, L_A_D)
l_ID_A_SSVENT += gp.GetSameIDs(GEO_FERME, L_D_E)
l_ID_A_SSVENT += gp.GetSameIDs(GEO_FERME, L_E_F)
gp.UnionIDs(A_SSVENT, l_ID_A_SSVENT)
gp.addToStudyInFather(GEO_FERME, A_SSVENT, 'A_SSVENT')

BLOCHET = gp.CreateGroup(GEO_FERME, gp.ShapeType["EDGE"])
l_ID_BLOCHET = []
l_ID_BLOCHET += gp.GetSameIDs(GEO_FERME, L_C_A)
l_ID_BLOCHET += gp.GetSameIDs(GEO_FERME, L_Cbis_Abis)
gp.UnionIDs(BLOCHET, l_ID_BLOCHET)
gp.addToStudyInFather(GEO_FERME, BLOCHET, 'BLOCHET')

POINCON = gp.CreateGroup(GEO_FERME, gp.ShapeType["EDGE"])
l_ID_POINCON = []
l_ID_POINCON += gp.GetSameIDs(GEO_FERME, L_F_G)
gp.UnionIDs(POINCON, l_ID_POINCON)
gp.addToStudyInFather(GEO_FERME, POINCON, 'POINCON')

JAMBE = gp.CreateGroup(GEO_FERME, gp.ShapeType["EDGE"])
l_ID_JAMBE = []
l_ID_JAMBE += gp.GetSameIDs(GEO_FERME, L_B_C)
l_ID_JAMBE += gp.GetSameIDs(GEO_FERME, L_C_D)
l_ID_JAMBE += gp.GetSameIDs(GEO_FERME, L_Bbis_Cbis)
l_ID_JAMBE += gp.GetSameIDs(GEO_FERME, L_Cbis_Dbis)
gp.UnionIDs(JAMBE, l_ID_JAMBE)
gp.addToStudyInFather(GEO_FERME, JAMBE, 'JAMBE')

ENTRAIT = gp.CreateGroup(GEO_FERME, gp.ShapeType["EDGE"])
l_ID_ENTRAIT = []
l_ID_ENTRAIT += gp.GetSameIDs(GEO_FERME, L_G_E)
l_ID_ENTRAIT += gp.GetSameIDs(GEO_FERME, L_G_Ebis)
gp.UnionIDs(ENTRAIT, l_ID_ENTRAIT)
gp.addToStudyInFather(GEO_FERME, ENTRAIT, 'ENTRAIT')

N_EFFORT = gp.CreateGroup(GEO_FERME, gp.ShapeType["VERTEX"])
l_ID_N_EFFORT = []
l_ID_N_EFFORT += gp.GetSameIDs(GEO_FERME, A)
l_ID_N_EFFORT += gp.GetSameIDs(GEO_FERME, D)
l_ID_N_EFFORT += gp.GetSameIDs(GEO_FERME, E)
l_ID_N_EFFORT += gp.GetSameIDs(GEO_FERME, F)
l_ID_N_EFFORT += gp.GetSameIDs(GEO_FERME, Ebis)
l_ID_N_EFFORT += gp.GetSameIDs(GEO_FERME, Dbis)
l_ID_N_EFFORT += gp.GetSameIDs(GEO_FERME, Abis)
gp.UnionIDs(N_EFFORT, l_ID_N_EFFORT)
gp.addToStudyInFather(GEO_FERME, N_EFFORT, 'N_EFFORT')

N_BLOQ = gp.CreateGroup(GEO_FERME, gp.ShapeType["VERTEX"])
l_ID_N_BLOQ = []
l_ID_N_BLOQ += gp.GetSameIDs(GEO_FERME, A)
l_ID_N_BLOQ += gp.GetSameIDs(GEO_FERME, B)
l_ID_N_BLOQ += gp.GetSameIDs(GEO_FERME, Bbis)
l_ID_N_BLOQ += gp.GetSameIDs(GEO_FERME, Abis)
gp.UnionIDs(N_BLOQ, l_ID_N_BLOQ)
gp.addToStudyInFather(GEO_FERME, N_BLOQ, 'N_BLOQ')

N_BLOQ_J = gp.CreateGroup(GEO_FERME, gp.ShapeType["VERTEX"])
l_ID_N_B = []
l_ID_N_B += gp.GetSameIDs(GEO_FERME, B)
l_ID_N_B += gp.GetSameIDs(GEO_FERME, Bbis)
gp.UnionIDs(N_BLOQ_J, l_ID_N_B)
gp.addToStudyInFather(GEO_FERME, N_BLOQ_J, 'N_BLOQ_J')

N_BLOQ_A = gp.CreateGroup(GEO_FERME, gp.ShapeType["VERTEX"])
l_ID_N_A = []
l_ID_N_A += gp.GetSameIDs(GEO_FERME, A)
l_ID_N_A += gp.GetSameIDs(GEO_FERME, Abis)
gp.UnionIDs(N_BLOQ_A, l_ID_N_A)
gp.addToStudyInFather(GEO_FERME, N_BLOQ_A, 'N_BLOQ_A')


# Generation du maillage :
# ------------------------
MESH_FERME = smesh.Mesh(GEO_FERME)
ALGO_1D = MESH_FERME.Segment()
ALGO_1D.NumberOfSegments(15)

isDone = MESH_FERME.Compute()
smesh.SetName(MESH_FERME.GetMesh(), 'FERME')

MESH_FERME.GroupOnGeom(ARBALETR, 'ARBALETR', SMESH.EDGE)
MESH_FERME.GroupOnGeom(A_AUVENT, 'A_AUVENT', SMESH.EDGE)
MESH_FERME.GroupOnGeom(A_SSVENT, 'A_SSVENT', SMESH.EDGE)
MESH_FERME.GroupOnGeom(BLOCHET, 'BLOCHET', SMESH.EDGE)
MESH_FERME.GroupOnGeom(POINCON, 'POINCON', SMESH.EDGE)
MESH_FERME.GroupOnGeom(JAMBE, 'JAMBE', SMESH.EDGE)
MESH_FERME.GroupOnGeom(ENTRAIT, 'ENTRAIT', SMESH.EDGE)

MESH_FERME.GroupOnGeom(N_EFFORT, 'N_EFFORT', SMESH.NODE)
MESH_FERME.GroupOnGeom(N_BLOQ, 'N_BLOQ', SMESH.NODE)
MESH_FERME.GroupOnGeom(N_BLOQ_J, 'N_BLOQ_J', SMESH.NODE)
MESH_FERME.GroupOnGeom(N_BLOQ_A, 'N_BLOQ_A', SMESH.NODE)


MESH_FERME.ExportMED(r'{{mesh_file}}',
                     False, SMESH.MED_V2_2, True, None, False)
if salome.sg.hasDesktop():
    salome.sg.updateObjBrowser(1)
