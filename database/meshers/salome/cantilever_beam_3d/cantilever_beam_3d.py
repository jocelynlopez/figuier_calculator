#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import salome

salome.salome_init()
theStudy = salome.myStudy

import salome_notebook
notebook = salome_notebook.NoteBook(theStudy)
sys.path.insert(
    0, r'C:/Documents and Settings/jlopez/Bureau/FISSURATION/MODELISATION')

###
# GEOM component
###

import GEOM
from salome.geom import geomBuilder
from math import *
import SALOMEDS
import SMESH
import SALOMEDS
from salome.smesh import smeshBuilder

smesh = smeshBuilder.New(theStudy)
gp = geomBuilder.New(theStudy)

# ***************************************************************************
# ******************************UTILS****************************************
# ***************************************************************************


def almostEqual(valueToTest, testingValue, tol=1E-007):
    if valueToTest < 0:
        test_inf = valueToTest >= testingValue * (1 + tol)
        test_sup = testingValue * (1 - tol) >= valueToTest
    else:
        test_inf = testingValue * (1 - tol) <= valueToTest
        test_sup = valueToTest <= testingValue * (1 + tol)
    return test_inf and test_sup


def GetInPlaceList(theShapeWhere, theShapeWhatList):
    """
    Use a GetInPlace with a list of subshape.
    """
    groups = []
    for theShapeWhat in theShapeWhatList:
        if theShapeWhat.GetType() == 37:
            groups.append(gp.GetInPlace(theShapeWhere, theShapeWhat))
        else:
            grp = gp.CreateGroup(theShapeWhere,
                                 theShapeWhat.GetShapeType()._v)
            subShape = gp.GetInPlace(theShapeWhere, theShapeWhat)
            subShapeID = gp.GetSubShapeID(theShapeWhere, subShape)
            gp.UnionIDs(grp, [subShapeID])
            groups.append(grp)
    return gp.UnionListOfGroups(groups)


def GetIDFromShapeList(MainShape, SubShapeList):
    SubShapeListID = []
    for subshape in SubShapeList:
        SubShapeListID.append(gp.GetSubShapeID(MainShape, subshape))
    return SubShapeListID


def GetGroupByName(Shape, GroupName):
    """Renvoi le groupe GroupName de Shape"""
    for grp in gp.GetGroups(Shape):
        if grp.GetName() == GroupName:
            return grp


def GeometricProgressionWithMAX(startLength, ratio, maxRatio, totalLength):
    """
    Creation d'une hypothese de taille progressive avec une
    limite de taille.
    """

    # Conversion en float
    startLength = float(startLength)
    ratio = float(ratio)
    maxRatio = float(maxRatio)
    totalLength = float(totalLength)

    hyp = smesh.CreateHypothesis('NumberOfSegments')
    hyp.SetDistrType(2)
    hyp.SetConversionMode(1)

    nelem = 0
    local_length = 0
    global_length = 0
    while True:
        if global_length > totalLength:
            hyp.SetNumberOfSegments(nelem)
            hyp.SetTableFunction([0, 1,
                                  1, maxRatio])
            break

        if local_length > startLength / maxRatio:
            ConstPos = global_length / totalLength
            nelem_restant = int((totalLength - global_length) /
                                (startLength / maxRatio)) + 1
            hyp.SetNumberOfSegments(nelem + nelem_restant)
            hyp.SetTableFunction([0, 1,
                                  ConstPos, maxRatio,
                                  1, maxRatio])
            break
        local_length = startLength * ratio**nelem
        global_length += local_length
        nelem += 1
    return hyp


def GetOrthogonalVec(V1, V2):
    """Retourne le vecteur V3 tel que V3 = V1 x V2"""
    V1_X0, V1_Y0, V1_Z0 = gp.VectorCoordinates(V1)
    V2_X0, V2_Y0, V2_Z0 = gp.VectorCoordinates(V2)

    V3_X0 = V1_Y0 * V2_Z0 - V1_Z0 * V2_Y0
    V3_Y0 = V1_Z0 * V2_X0 - V1_X0 * V2_Z0
    V3_Z0 = V1_X0 * V2_Y0 - V1_Y0 * V2_X0

    return gp.MakeVectorDXDYDZ(V3_X0, V3_Y0, V3_Z0)


def CreateGroupFromCDG(ExtractShape, GroupInShape, ShapeType, CS=None,
                       X=None, Y=None, Z=None):
    GRP = gp.CreateGroup(GroupInShape, ShapeType)

    O = gp.MakeVertex(0, 0, 0)
    OX = gp.MakeVectorDXDYDZ(1, 0, 0)
    OY = gp.MakeVectorDXDYDZ(0, 1, 0)
    CS_GLOBAL = gp.MakeMarkerPntTwoVec(O, OX, OY)

    for subshape in gp.ExtractShapes(ExtractShape, ShapeType, True):
        P = gp.MakeCDG(subshape)
        gp.Position(P, CS, CS_GLOBAL)
        Px, Py, Pz = gp.PointCoordinates(P)

        if X is not None:
            if not almostEqual(Px, X):
                continue
        if Y is not None:
            if not almostEqual(Py, Y):
                continue
        if Z is not None:
            if not almostEqual(Pz, Z):
                continue
        subshape_ID = gp.GetSubShapeID(GroupInShape, subshape)
        gp.AddObject(GRP, subshape_ID)
    return GRP


def repere(gp, name="O"):
    """Creation du repere"""

    O = gp.MakeVertex(0, 0, 0)
    OX = gp.MakeVectorDXDYDZ(1, 0, 0)
    OY = gp.MakeVectorDXDYDZ(0, 1, 0)
    OZ = gp.MakeVectorDXDYDZ(0, 0, 1)

    gp.addToStudy(O, name)
    gp.addToStudy(OX, name + "X")
    gp.addToStudy(OY, name + "Y")
    gp.addToStudy(OZ, name + "Z")

    O_Folder = gp.NewFolder('Repere %s' % (name))
    gp.PutToFolder(O, O_Folder)
    gp.PutToFolder(OX, O_Folder)
    gp.PutToFolder(OY, O_Folder)
    gp.PutToFolder(OZ, O_Folder)

    return O, OX, OY, OZ


def AddToStudyCurrentObjects(obj_list=locals(), SHAPETYPELIST=None,
                             FOLDER='', inDIR=None, FOLDERSHAPE=True,
                             PREFIX='', SUFFIX=''):
    """
    Ajout automatique des elements courant dans l'arborescence.

    obj_list : Liste des objets a ajouter (par defaut tout les objets)
    SHAPETYPELIST : liste des type de forme concerne (ex: ['VERTEX'])
    PREFIX : PREFIX dans le nom
    SUFFIX : suffixe dans le nom
    """

    # Creation de la liste de valeur des types de formes possible
    if SHAPETYPELIST is not None:
        SHAPETYPELISTVALUE = map(gp.ShapeType.__getitem__, SHAPETYPELIST)

    # Creation de la liste des types de formes existant
    shapeTypeList = []
    for name, obj in obj_list.items():
        if 'GetShapeType' not in dir(obj):
            continue
        if str(obj.GetShapeType()) not in shapeTypeList:
            shapeTypeList.append(str(obj.GetShapeType()))

    # Creation des dossiers
    SFOLDERS = []
    if FOLDER:
        if inDIR:
            M_FOLDER = gp.NewFolder(FOLDER, inDIR)
        else:
            M_FOLDER = gp.NewFolder(FOLDER)
        if FOLDERSHAPE:
            for shapeType in shapeTypeList:
                SFOLDERS.append(gp.NewFolder(shapeType, M_FOLDER))
    else:
        if FOLDERSHAPE:
            for shapeType in shapeTypeList:
                if inDIR:
                    SFOLDERS.append(gp.NewFolder(shapeType, inDIR))
                else:
                    SFOLDERS.append(gp.NewFolder(shapeType))

    for name, obj in obj_list.items():
        if 'GetShapeType' not in dir(obj):
            continue

        if SHAPETYPELIST is None:
            gp.addToStudy(obj, PREFIX + name + SUFFIX)
            if FOLDERSHAPE:
                idx = shapeTypeList.index(str(obj.GetShapeType()))
                gp.PutToFolder(obj, SFOLDERS[idx])
        else:
            if obj.GetShapeType()._v in SHAPETYPELISTVALUE:
                gp.addToStudy(obj, PREFIX + name + SUFFIX)
                idx = SHAPETYPELISTVALUE.index(obj.GetShapeType()._v)
                gp.PutToFolder(obj, FOLDERLIST[idx])

# ***************************************************************************
# ***************************************************************************
# ***************************************************************************


# ***************************************************************************
# **********************************SECTIONS*********************************
# ***************************************************************************

class CantileverBeam3D(object):

    def __init__(self, REPERE_PROFIL, GEO_PARAM, MESH_PARAM, welding=True):

        self.O = gp.MakeVertex(0, 0, 0)
        self.OX = gp.MakeVectorDXDYDZ(1, 0, 0)
        self.OY = gp.MakeVectorDXDYDZ(0, 1, 0)
        self.OZ = gp.MakeVectorDXDYDZ(0, 0, 1)

        self.REPERE_PROFIL = REPERE_PROFIL

        self.GEO = {'SOUDURE': {},
                    'PROFIL': {},
                    'PLATE': {},
                    'CL': {}}

        self.MESH = {'SOUDURE': {},
                     'PROFIL': {},
                     'PLATE': {},
                     'CL': {},
                     'POST': {}}

        self.GEO_PARAM = GEO_PARAM
        self.MESH_PARAM = MESH_PARAM
        self.welding = welding

        self.AutoParameters()
        if self.welding:
            self.AutoWeldingParameters()

        self.DIR = gp.NewFolder(self.GetName())

    def GetName(self):
        raise NotImplementedError(
            'This method need to be defined in the sub-class')

    def GetMaxDIM(self):
        PROFIL = self.GEO['PROFIL']['MAIN']
        [Xmin, Xmax, Ymin, Ymax, Zmin, Zmax] = gp.BoundingBox(PROFIL, True)
        DimX = Xmax - Xmin
        DimY = Ymax - Ymin
        DimZ = Zmax - Zmin

        self.DIM_H = max(DimX, DimY)
        self.DIM_V = DimZ
        self.DIM = max(DimX, DimY, DimZ)

    def AutoParameters(self):
        pass

    def AutoWeldingParameters(self):
        # Constant
        # NB_ = 0.4

        # Declaration des variables raccourci pour simplifier les expressions
        a = self.GEO_PARAM['SOUDURE']['a']

        if 'nb_elem' not in self.MESH_PARAM['SOUDURE'].keys():
            self.MESH_PARAM['SOUDURE']['nb_elem'] = 3
        nbElem = self.MESH_PARAM['SOUDURE']['nb_elem']

        # ---------------------------------------
        # -------------  GEOMETRY  --------------
        # ---------------------------------------
        # Calculs
        p = a * 2**0.5
        # square_length = RATIO_SQUARE_WELDING * p / 2

        # Stockage
        self.GEO_PARAM['SOUDURE']['p'] = p
        # self.GEO_PARAM['SOUDURE']['r_partition'] = RATIO_SQUARE_WELDING

        # ---------------------------------------
        # ---------------  MESH  ----------------
        # ---------------------------------------
        # Calculs
        if 'pfiss' in self.MESH_PARAM['SOUDURE'].keys():
            min_height = self.MESH_PARAM['SOUDURE']['pfiss'] / nbElem
        else:
            min_height = (p / 2) / nbElem
        # nb_elem_on_square = max(3, int(square_length / min_height) - 1)

        # Stockage
        # self.MESH_PARAM['SOUDURE']['nb_elem_on_square'] = nb_elem_on_square
        self.MESH_PARAM['PROFIL']['min_height'] = min_height

    def MakeProfilSection(self, addToStudy=False):
        """Fonction renvoyant le tuple suivant :
        CONTOUR_EXT, CONTOUR_INT, START_POINT, PLATE_VEC

        avec :
        CONTOUR_EXT: Contour exterieur du profil
        CONTOUR_INT: Contour interieur du profil (si existant)
        START_POINT: Un point appartenant a CONTOUR_EXT
        PLATE_VEC:   Le vecteur partant de START_POINT et perpendiculaire a
                     CONTOUR_EXT et colineaire a la plaque
        """
        raise NotImplementedError(
            'This method need to be defined in the sub-class')

    def MakeProfil(self, addToStudy=False):
        """Creation du volume du tube"""
        ProfilSection = self.GEO['PROFIL']['SECTION']
        CONTOUR_EXT = self.GEO['PROFIL']['CONTOUR_EXT']
        H = self.GEO_PARAM['PROFIL']['H']
        PROFIL = gp.MakePrismVecH(ProfilSection, self.OZ, H)

        # Stockage
        self.GEO['PROFIL']['MAIN'] = PROFIL

        # Impression
        if addToStudy:
            AddToStudyCurrentObjects(locals(), FOLDER="PROFIL", inDIR=self.DIR)
            gp.addToStudy(PROFIL, 'PROFIL')
            gp.PutToFolder(PROFIL, self.DIR)

        return PROFIL

    def MakeWelding(self, Partition=True, addToStudy=False):
        """Creation de la soudure"""

        # Declaration des variables raccourci pour simplifier les expressions
        FACE_TYPE = gp.ShapeType["FACE"]
        ep = self.GEO_PARAM['PROFIL']['ep']
        a = self.GEO_PARAM['SOUDURE']['a']
        p = self.GEO_PARAM['SOUDURE']['p']
        CONTOUR_EXT = self.GEO['PROFIL']['CONTOUR_EXT']
        # r = self.GEO_PARAM['SOUDURE']['r_partition']
        START_POINT = self.GEO['SOUDURE']['START_POINT']
        PLATE_VEC = self.GEO['SOUDURE']['PLATE_VEC']
        pfiss = self.MESH_PARAM['SOUDURE']['pfiss']
        # Contenu
        (STx, STy, STz) = gp.PointCoordinates(START_POINT)
        (PVx, PVy, PVz) = gp.VectorCoordinates(PLATE_VEC)
        HDD2S = gp.MakeVertex(STx, STy, STz + p)
        HDD3S = gp.MakeVertex(STx + PVx * p, STy + PVy * p, STz)

        if not Partition:
            LSV = gp.MakeLineTwoPnt(START_POINT, HDD2S)
            LSD = gp.MakeLineTwoPnt(HDD2S, HDD3S)
            LSH = gp.MakeLineTwoPnt(HDD3S, START_POINT)
            L_EDGES = [LSV, LSH]
            CONTOUR = gp.MakeWire([LSV, LSD, LSH], 1e-007)
            SECTION = gp.MakeFaceWires([CONTOUR], 1)
        else:
            HDDMS = gp.MakeVertex(STx + PVx * p / 2,
                                  STy + PVy * p / 2,
                                  STz + p / 2)
            HDDMIS = gp.MakeVertex(STx + PVx * p / 4,
                                   STy + PVy * p / 4,
                                   STz + p / 4)
            HDD22S = gp.MakeVertex(STx, STy, STz + p - pfiss)
            HDD23S = gp.MakeVertex(STx + PVx * p / 2,
                                   STy + PVy * p / 2,
                                   STz)

            HDD2MS = gp.MakeVertex(STx + PVx * p / 4,
                                   STy + PVy * p / 4,
                                   STz + p * 3 / 4)
            HDDM3S = gp.MakeVertex(STx + PVx * p * 3 / 4,
                                   STy + PVy * p * 3 / 4,
                                   STz + p / 4)
            HDD22MS = gp.MakeVertex(STx + PVx * p / 6,
                                    STy + PVy * p / 6,
                                    STz + p / 2)
            HDD2M3S = gp.MakeVertex(STx + PVx * p / 2,
                                    STy + PVy * p / 2,
                                    STz + p / 6)

            LI01 = gp.MakeLineTwoPnt(HDD22S, START_POINT)
            LI02 = gp.MakeLineTwoPnt(START_POINT, HDDMIS)
            LI03 = gp.MakeLineTwoPnt(HDDMIS, HDD22MS)
            LI04 = gp.MakeLineTwoPnt(HDD22MS, HDD22S)
            LI05 = gp.MakeLineTwoPnt(HDD22MS, HDD2MS)
            LI06 = gp.MakeLineTwoPnt(HDD2MS, HDD2S)
            LI07 = gp.MakeLineTwoPnt(HDD2S, HDD22S)
            LI08 = gp.MakeLineTwoPnt(HDDMIS, HDDMS)
            LI09 = gp.MakeLineTwoPnt(HDDMS, HDD2MS)

            LII01 = gp.MakeLineTwoPnt(START_POINT, HDD23S)
            LII02 = gp.MakeLineTwoPnt(HDD23S, HDD2M3S)
            LII03 = gp.MakeLineTwoPnt(HDD2M3S, HDDMIS)
            LII04 = gp.MakeLineTwoPnt(HDD2M3S, HDDM3S)
            LII05 = gp.MakeLineTwoPnt(HDDM3S, HDDMS)
            LII06 = gp.MakeLineTwoPnt(HDD23S, HDD3S)
            LII07 = gp.MakeLineTwoPnt(HDD3S, HDDM3S)

            L_EDGES = [LII01, LII06, LII07, LII05, LI09, LII06, LI02, LI08, LI01, LI07, LI06]

            WIRE_TRI_TR1 = gp.MakeWire([LI01, LI02, LI03, LI04], 1e-007)
            WIRE_TRI_TR2 = gp.MakeWire([LI04, LI05, LI06, LI07], 1e-007)
            WIRE_TRI_TR3 = gp.MakeWire([LI03, LI08, LI09, LI05], 1e-007)
            WIRE_TRII_TR1 = gp.MakeWire([LII01, LII02, LII03, LI02], 1e-007)
            WIRE_TRII_TR2 = gp.MakeWire([LII03, LII04, LII05, LI08], 1e-007)
            WIRE_TRII_TR3 = gp.MakeWire([LII02, LII06, LII07, LII04], 1e-007)

            FACE_TRI_TR1 = gp.MakeFaceWires([WIRE_TRI_TR1], 1)
            FACE_TRI_TR2 = gp.MakeFaceWires([WIRE_TRI_TR2], 1)
            FACE_TRI_TR3 = gp.MakeFaceWires([WIRE_TRI_TR3], 1)
            FACE_TRII_TR1 = gp.MakeFaceWires([WIRE_TRII_TR1], 1)
            FACE_TRII_TR2 = gp.MakeFaceWires([WIRE_TRII_TR2], 1)
            FACE_TRII_TR3 = gp.MakeFaceWires([WIRE_TRII_TR3], 1)

            SECTION = gp.MakeFuseList([FACE_TRI_TR1, FACE_TRI_TR2, FACE_TRI_TR3,
                                       FACE_TRII_TR1, FACE_TRII_TR2, FACE_TRII_TR3],
                                      True, False)

        SOUDURE = gp.MakePipe(SECTION, CONTOUR_EXT)

        # Recuperation de groupe
        F_PLATE = CreateGroupFromCDG(SOUDURE, SOUDURE, FACE_TYPE, Z=0)
        # E_APOTH = GetInPlaceList(SOUDURE, L_EDGES)

        # Stockage
        self.GEO['SOUDURE']['MAIN'] = SOUDURE
        self.GEO['SOUDURE']['SECTION'] = SECTION
        # self.GEO['SOUDURE']['E_APOTH'] = E_APOTH
        self.GEO['SOUDURE']['F_PLATE'] = F_PLATE
        self.GEO['SOUDURE']['L_APOTH'] = gp.MakeWire([LI02, LI08], 1e-07)
        self.GEO['SOUDURE']['L_FISS'] = LI07
        self.GEO['SOUDURE']['PT_FOND_FISS'] = HDD22S

        # Impression
        if addToStudy:
            AddToStudyCurrentObjects(locals(), FOLDER="SOUDURE",
                                     inDIR=self.DIR)
            gp.addToStudy(SOUDURE, 'SOUDURE')
            gp.PutToFolder(SOUDURE, self.DIR)

        return SOUDURE

    def MakePlate(self, addToStudy=False):
        # Declaration des variables raccourci pour simplifier les expressions
        Lx = self.GEO_PARAM['PLATE']['Lx']
        Ly = self.GEO_PARAM['PLATE']['Ly']
        ep = self.GEO_PARAM['PLATE']['ep']

        # Contenu
        PLATE = gp.MakeBoxDXDYDZ(Lx, Ly, ep)
        gp.TranslateDXDYDZ(PLATE, -Lx / 2, -Ly / 2, -ep)

        # Stockage
        self.GEO['PLATE']['MAIN'] = PLATE

        # Impression
        if addToStudy:
            gp.addToStudy(PLATE, 'PLATE')
            gp.PutToFolder(PLATE, self.DIR)

        return PLATE

    def MoveSubShape(self):
        """Deplacement dans le repere du profil des objets."""

        if self.welding:
            SOUDURE = self.GEO['SOUDURE']['MAIN']
            gp.Position(SOUDURE, None, self.REPERE_PROFIL)
            self.GEO['SOUDURE']['MAIN'] = SOUDURE

            F_SOUDURE_SECTION = self.GEO['SOUDURE']['SECTION']
            gp.Position(F_SOUDURE_SECTION, None, self.REPERE_PROFIL)
            self.GEO['SOUDURE']['SECTION'] = F_SOUDURE_SECTION

        PROFIL = self.GEO['PROFIL']['MAIN']
        gp.Position(PROFIL, None, self.REPERE_PROFIL)
        self.GEO['PROFIL']['MAIN'] = PROFIL

        CONTOUR_EXT = self.GEO['PROFIL']['CONTOUR_EXT']
        gp.Position(CONTOUR_EXT, None, self.REPERE_PROFIL)
        self.GEO['PROFIL']['CONTOUR_EXT'] = CONTOUR_EXT

        F_PROFIL_SECTION = self.GEO['PROFIL']['SECTION']
        gp.Position(F_PROFIL_SECTION, None, self.REPERE_PROFIL)
        self.GEO['PROFIL']['SECTION'] = F_PROFIL_SECTION

    def GetProfilPartitionPlanes(self, addToStudy=False):
        """
        Cette methode renvoi la liste des plans de partition du profil

        Cette liste doit etre compose a minima des plans passant par les points de construction de la section du profil
        """
        if self.welding:
            # Declaration des variables raccourci pour simplifier les
            # expressions
            p = self.GEO_PARAM['SOUDURE']['p']

            # Variables dependantes
            self.GetMaxDIM()

            # Contenu
            PLANE_Z = gp.MakePlane(self.O, self.OZ, self.DIM * 2.1)
            PLANE_ZP = gp.MakeTranslationVectorDistance(PLANE_Z, self.OZ, p)

            # Impression
            if addToStudy:
                AddToStudyCurrentObjects(locals(), FOLDER="PARTITION_TOOLS",
                                         inDIR=self.DIR)

            return [PLANE_ZP]
        else:
            return []

    def MakePartition(self, addToStudy=False):
        """
        Partitionnement du volume du tube afin d'obtenir
        un maillage exploitable.
        """

        # Declaration des variables raccourci pour simplifier les expressions
        EDGE_TYPE = gp.ShapeType["EDGE"]
        FACE_TYPE = gp.ShapeType["FACE"]
        SOLID_TYPE = gp.ShapeType["SOLID"]
        ep = self.GEO_PARAM['PLATE']['ep']
        H = self.GEO_PARAM['PROFIL']['H']
        PROFIL = self.GEO['PROFIL']['MAIN']
        PLATE = self.GEO['PLATE']['MAIN']
        CONTOUR_EXT = self.GEO['PROFIL']['CONTOUR_EXT']
        if self.welding:
            a = self.GEO_PARAM['SOUDURE']['a']
            p = self.GEO_PARAM['SOUDURE']['p']
            SOUDURE = self.GEO['SOUDURE']['MAIN']
            F_SOUDURE_PLATE = self.GEO['SOUDURE']['F_PLATE']
            L_SOUDURE_FISS = self.GEO['SOUDURE']['L_FISS']
            PT_SOUDURE_FOND_FISS = self.GEO['SOUDURE']['PT_FOND_FISS']
            L_SOUDURE_APOTH = self.GEO['SOUDURE']['L_APOTH']

        # Contenu
        if self.welding:
            objectsToPartition = [PROFIL, SOUDURE]
        else:
            objectsToPartition = [PROFIL]

        PartitionPlanes = self.GetProfilPartitionPlanes()

        # Deplacement des objets dans le repere du profil
        self.MoveSubShape()
        for plane in PartitionPlanes:
            gp.Position(plane, None, self.REPERE_PROFIL)

        ASSEMBLAGE = gp.MakePartition(objectsToPartition,
                                      PartitionPlanes, [], [])

        F_PROFIL_SECTION = gp.GetInPlace(ASSEMBLAGE,
                                         self.GEO['PROFIL']['SECTION'])

        if self.welding:
            F_SOUDURE_PLATE = gp.GetInPlace(ASSEMBLAGE, F_SOUDURE_PLATE)
            WeldingExtruded = gp.MakePrismVecH2Ways(F_SOUDURE_PLATE, self.OZ, H)
            PlatePart = gp.MakePartition([PLATE], [WeldingExtruded],
                                         [], [], SOLID_TYPE, 0, [], 0)
        else:
            ProfilExtruded = gp.MakePrismVecH2Ways(F_PROFIL_SECTION, self.OZ,
                                                   H)
            PlatePart = gp.MakePartition([PLATE], [ProfilExtruded],
                                         [], [], SOLID_TYPE, 0, [], 0)

        # Creation du point de support du torseur a l'extrimite du profil
        PT_PROFIL_EXT = gp.MakeVertex(0, 0, H)
        gp.Position(PT_PROFIL_EXT, None, self.REPERE_PROFIL)

        ASSEMBLAGE = gp.MakeCompound([ASSEMBLAGE, PlatePart, PT_PROFIL_EXT])
        ASSEMBLAGE = gp.MakeGlueFaces(ASSEMBLAGE, 1e-007)
        ASSEMBLAGE = gp.MakeGlueEdges(ASSEMBLAGE, 1e-007)

        # Recuperation des volumes de l'assemblage
        PROFIL = gp.GetInPlace(ASSEMBLAGE, PROFIL)
        PLATE = gp.GetInPlace(ASSEMBLAGE, PlatePart)
        if self.welding:
            SOUDURE = gp.GetInPlace(ASSEMBLAGE, SOUDURE)

        # ----------------------
        # Creation des groupes
        # ----------------------
        if self.welding:
            # Groupe des faces communes entre profil/soudure
            F_SOUDURE_PROFIL = gp.CreateGroup(ASSEMBLAGE, FACE_TYPE)
            l_FACES = gp.GetSharedShapesMulti([PROFIL, SOUDURE], FACE_TYPE, False)
            gp.UnionIDs(F_SOUDURE_PROFIL, GetIDFromShapeList(ASSEMBLAGE, l_FACES))

            F_SOUDURE_SECTION = gp.GetInPlace(ASSEMBLAGE,
                                              self.GEO['SOUDURE']['SECTION'])

            # Hauteur du centre de gravite des edges verticales du profil
            GzVPROFIL = (H - p) / 2 + p
        else:
            GzVPROFIL = H / 2

        # Profil
        E_PROFIL_VERT = CreateGroupFromCDG(PROFIL, ASSEMBLAGE, EDGE_TYPE,
                                           Z=GzVPROFIL)
        F_PROFIL_SECTION = gp.GetInPlace(ASSEMBLAGE, F_PROFIL_SECTION)

        E_PROFIL_CT_EXT = gp.GetInPlace(ASSEMBLAGE,
                                        self.GEO['PROFIL']['CONTOUR_EXT'])
        V_PROFIL_EXT = gp.GetInPlace(ASSEMBLAGE, PT_PROFIL_EXT)

        # Plate
        MiddleHeightPLATE = -ep / 2.
        E_PLATE_VERT = CreateGroupFromCDG(PLATE, ASSEMBLAGE, EDGE_TYPE,
                                          Z=MiddleHeightPLATE)
        F_PLATE_PLANE = CreateGroupFromCDG(PLATE, ASSEMBLAGE, FACE_TYPE,
                                           Z=0)

        if self.welding:
            F_PLATE_CONTACT = CreateGroupFromCDG(SOUDURE, ASSEMBLAGE,
                                                 FACE_TYPE, Z=0)
        else:
            F_PLATE_CONTACT = CreateGroupFromCDG(PROFIL, ASSEMBLAGE,
                                                 FACE_TYPE, Z=0)

        # Extraction des objets pour les CL et post-traitement
        CL_EXT_PROFIL = CreateGroupFromCDG(PROFIL, ASSEMBLAGE, FACE_TYPE,
                                           Z=H)
        self.GEO['CL']['EXT_PROFIL'] = CL_EXT_PROFIL

        CL_BASE_PLATE = CreateGroupFromCDG(PLATE, ASSEMBLAGE, FACE_TYPE,
                                           Z=-ep)
        self.GEO['CL']['BASE_PLATE'] = CL_BASE_PLATE

        # Recuperation du profil ET soudure
        if self.welding:
            SoudureProfil = gp.UnionListOfGroups([PROFIL, SOUDURE])

        # Recuperation groupe de la soudure
        F_SOUDURE_FISS = gp.MakePipe(L_SOUDURE_FISS, CONTOUR_EXT)
        F_SOUDURE_FISS = gp.GetInPlace(ASSEMBLAGE, F_SOUDURE_FISS)

        F_SOUDURE_APOTH = gp.MakePipe(L_SOUDURE_APOTH, CONTOUR_EXT)
        F_SOUDURE_APOTH = gp.GetInPlace(ASSEMBLAGE, F_SOUDURE_APOTH)

        L_SOUDURE_FOND_FISS = gp.MakePipe(PT_SOUDURE_FOND_FISS, CONTOUR_EXT)
        L_SOUDURE_FOND_FISS = gp.GetInPlace(ASSEMBLAGE, L_SOUDURE_FOND_FISS)

        # Stockage
        self.ASSEMBLAGE = ASSEMBLAGE

        self.GEO['PROFIL']['MAIN'] = PROFIL
        self.GEO['PLATE']['MAIN'] = PLATE

        self.GEO['PROFIL']['E_VERT'] = E_PROFIL_VERT
        self.GEO['PROFIL']['SECTION'] = F_PROFIL_SECTION
        self.GEO['PROFIL']['E_CT_EXT'] = E_PROFIL_CT_EXT
        self.GEO['PROFIL']['V_EXT'] = V_PROFIL_EXT

        self.GEO['PLATE']['E_VERT'] = E_PLATE_VERT
        self.GEO['PLATE']['F_CONTACT'] = F_PLATE_CONTACT
        self.GEO['PLATE']['F_PLANE'] = F_PLATE_PLANE

        if self.welding:
            self.GEO['SOUDURE']['MAIN'] = SOUDURE
            self.GEO['PROFIL_SOUDURE'] = SoudureProfil
            self.GEO['SOUDURE']['F_PROFIL'] = F_SOUDURE_PROFIL
            self.GEO['SOUDURE']['F_PLATE'] = F_SOUDURE_PLATE
            self.GEO['SOUDURE']['F_APOTH'] = F_SOUDURE_APOTH
            self.GEO['SOUDURE']['F_FISS'] = F_SOUDURE_FISS
            self.GEO['SOUDURE']['L_FOND_FISS'] = L_SOUDURE_FOND_FISS
            self.GEO['SOUDURE']['SECTION'] = F_SOUDURE_SECTION

        # Impression
        gp.addToStudy(ASSEMBLAGE, 'ASSEMBLAGE')
        gp.addToStudyInFather(ASSEMBLAGE, PROFIL, 'PROFIL')
        gp.addToStudyInFather(ASSEMBLAGE, PLATE, 'PLATE')

        gp.addToStudyInFather(PROFIL, E_PROFIL_VERT, 'E_PROFIL_VERT')
        gp.addToStudyInFather(PROFIL, F_PROFIL_SECTION, 'SECTION')
        gp.addToStudyInFather(PROFIL, E_PROFIL_CT_EXT, 'E_CT_EXT')
        gp.addToStudyInFather(PROFIL, V_PROFIL_EXT, 'V_EXT')

        gp.addToStudyInFather(PLATE, E_PLATE_VERT, 'E_PLATE_VERT')
        gp.addToStudyInFather(PLATE, F_PLATE_CONTACT, 'F_PLATE_CONTACT')
        gp.addToStudyInFather(PLATE, F_PLATE_PLANE, 'F_PLATE_PLANE')

        gp.addToStudyInFather(PROFIL, CL_EXT_PROFIL, 'CL_EXT_PROFIL')
        gp.addToStudyInFather(PLATE, CL_BASE_PLATE, 'CL_BASE_PLATE')

        if self.welding:
            gp.addToStudyInFather(ASSEMBLAGE, SOUDURE, 'SOUDURE')
            gp.addToStudyInFather(ASSEMBLAGE, SoudureProfil,
                                  'PROFIL_AND_SOUDURE')
            gp.addToStudyInFather(SOUDURE, F_SOUDURE_PROFIL, 'F_PROFIL')
            gp.addToStudyInFather(SOUDURE, F_SOUDURE_PLATE, 'F_PLATE')
            gp.addToStudyInFather(SOUDURE, F_SOUDURE_APOTH, 'F_APOTH')
            gp.addToStudyInFather(SOUDURE, F_SOUDURE_FISS, 'F_FISS')
            gp.addToStudyInFather(SOUDURE, L_SOUDURE_FOND_FISS, 'L_FOND_FISS')
            gp.addToStudyInFather(SOUDURE, F_SOUDURE_SECTION, 'SECTION')
        gp.PutToFolder(ASSEMBLAGE, self.DIR)
        if addToStudy:
            AddToStudyCurrentObjects(
                locals(), FOLDER="PARTITION", inDIR=self.DIR)

        return ASSEMBLAGE

    def GetReversedVertEdgesIDOnProfil(self):
        """This method need to be surcharge by sub-class if needed."""
        return []

    def MakeGeometry(self, addToStudy=False):
        # Creation du profil
        self.MakeProfilSection(addToStudy)
        self.MakeProfil(addToStudy)

        # Creation de la soudure
        if self.welding:
            self.MakeWelding(Partition=True, addToStudy=addToStudy)

        # Creation de la plaque
        self.MakePlate(addToStudy)

        return self.MakePartition(addToStudy)

    def MakeMesh(self):
        # Creation des algorithmes et hypotheses de maillage
        self.BuildAlgorithms()

        # Creation de la section du profil
        self.MakeProfilSectionMesh()

        # Creation de la section de la soudure
        if self.welding:
            # Creation de la section de la soudure
            self.MakeWeldingSectionMesh()

            # Creation de la soudure et du profil
            self.MakeWeldingAndProfilMesh()
        else:
            self.MakeProfilMesh()

        # Creation de la plaque
        self.MakePlatePlaneMesh()
        self.MakePlateMesh()

        # Creation de l'assemblage
        self.MakeAssemblageMesh()

    def BuildAlgorithms(self):
        # Declaration des variables raccourci pour simplifier les expressions
        HYP = smesh.CreateHypothesis

        self.algo = {}
        self.algo['Prism3D'] = HYP('Prism_3D')
        self.algo['Quad2D'] = HYP('Quadrangle_2D')
        self.algo['Regular1D'] = HYP('Regular_1D')
        self.algo['QuadAxis2D'] = HYP('QuadFromMedialAxis_1D2D')
        smesh.SetName(self.algo['Prism3D'], 'Prism3D')
        smesh.SetName(self.algo['Quad2D'], 'Quad2D')
        smesh.SetName(self.algo['Regular1D'], 'Regular1D')
        smesh.SetName(self.algo['QuadAxis2D'], 'QuadAxis2D')

    def MakeWeldingSectionMesh(self):

        # Declaration des variables raccourci pour simplifier les expressions
        SECTION = self.GEO['SOUDURE']['SECTION']
        nbElem = self.MESH_PARAM['SOUDURE']['nb_elem']

        # Contenu
        SoudureSectionMesh = smesh.Mesh(SECTION)
        smesh.SetName(SoudureSectionMesh.GetMesh(), '_SOUDURE_SECTION')

        NbSegSoudure = SoudureSectionMesh.Segment()
        NbSegSoudure_H = NbSegSoudure.NumberOfSegments(nbElem)
        NbSegSoudure_H.SetDistrType(0)
        smesh.SetName(NbSegSoudure_H, 'NbSegSoudure')

        SoudureSectionMesh.AddHypothesis(self.algo['Quad2D'])
        SoudureSectionMesh.AddHypothesis(self.algo['Regular1D'])
        SoudureSectionMesh.Compute()

        # Stockage
        self.MESH['SOUDURE']['SECTION'] = SoudureSectionMesh

        return SoudureSectionMesh

    def MakeProfilSectionMesh(self):
        """Construit le maillage de la section du profil."""

        # Declaration des variables raccourci pour simplifier les expressions
        LAYER = self.MESH_PARAM['PROFIL']['layer']
        FINENESS = self.MESH_PARAM['PROFIL']['fineness']
        SECTION = self.GEO['PROFIL']['SECTION']

        # Contenu
        ProfilSectionMesh = smesh.Mesh(SECTION)
        smesh.SetName(ProfilSectionMesh.GetMesh(), '_PROFIL_SECTION')

        # Nombre de couche dans l'epaisseur :
        NbOfLayers2D = ProfilSectionMesh.Quadrangle(
            algo=smeshBuilder.QUAD_MA_PROJ)
        NbOfLayers2D_H = NbOfLayers2D.NumberOfLayers(LAYER)
        smesh.SetName(NbOfLayers2D_H, 'NbOfLayersProfil')

        # Taille automatique
        AutoLength1D = ProfilSectionMesh.Segment()
        AutoLength1D_H = AutoLength1D.AutomaticLength(FINENESS)
        smesh.SetName(AutoLength1D_H, 'AutoLengthProfil')

        # Mesh generation
        ProfilSectionMesh.AddHypothesis(self.algo['QuadAxis2D'])
        ProfilSectionMesh.AddHypothesis(self.algo['Regular1D'])
        ProfilSectionMesh.Compute()

        # Stockage
        self.MESH['PROFIL']['SECTION'] = ProfilSectionMesh

        return ProfilSectionMesh

    def MakeProfilMesh(self):

        # Declaration des variables raccourci pour simplifier les expressions
        PROFIL_VEDGES = self.GEO['PROFIL']['E_VERT']
        PROFIL_SECTION = self.GEO['PROFIL']['SECTION']
        PROFIL_SECT_MESH = self.MESH['PROFIL']['SECTION']
        CL_EXT_PROFIL = self.GEO['CL']['EXT_PROFIL']

        FINENESS = self.MESH_PARAM['PROFIL']['fineness']
        min_height = self.MESH_PARAM['PROFIL']['min_height']
        max_height = min(self.DIM_H / 4, self.DIM_V / 10)

        # Contenu
        ProfilMesh = smesh.Mesh(self.GEO['PROFIL']['MAIN'])
        smesh.SetName(ProfilMesh.GetMesh(), '_PROFIL')

        # Importation des sous-maillage existant
        PROFIL_Proj1D2D = ProfilMesh.Projection1D2D(geom=PROFIL_SECTION)
        PROFIL_Proj1D2D_H = PROFIL_Proj1D2D.SourceFace(PROFIL_SECTION,
                                                       PROFIL_SECT_MESH)
        smesh.SetName(PROFIL_Proj1D2D, 'PROFIL_Proj')
        smesh.SetName(PROFIL_Proj1D2D_H, 'PROFIL_Proj')
        smesh.SetName(PROFIL_Proj1D2D.GetSubMesh(), 'PROFIL_Proj')

        # Creation de sous-maillage
        ProfilHeight = ProfilMesh.Segment(geom=PROFIL_VEDGES)
        ProfilHeight_H = ProfilHeight.StartEndLength(min_height, max_height)
        reversedEdges_ID = self.GetReversedVertEdgesIDOnProfil()
        ProfilHeight_H.SetReversedEdges(reversedEdges_ID)
        smesh.SetName(ProfilHeight, 'ProfilHeight')
        smesh.SetName(ProfilHeight_H, 'ProfilHeight')
        smesh.SetName(ProfilHeight.GetSubMesh(), 'ProfilHeight')

        # Taille automatique
        AutoLength1D = ProfilMesh.Segment()
        AutoLength1D_H = AutoLength1D.AutomaticLength(FINENESS)
        smesh.SetName(AutoLength1D_H, 'AutoLengthPlate')

        ProfilMesh.AddHypothesis(self.algo['Prism3D'])
        ProfilMesh.AddHypothesis(self.algo['Quad2D'])
        ProfilMesh.AddHypothesis(self.algo['Regular1D'])
        ProfilMesh.Compute()

        # Creation des groupes de CL et post-traitement
        CL_EXT_PROFIL_F = ProfilMesh.GroupOnGeom(CL_EXT_PROFIL,
                                                 'F_EXTPRO', SMESH.FACE)
        CL_EXT_PROFIL_N = ProfilMesh.GroupOnGeom(CL_EXT_PROFIL,
                                                 'N_EXTPRO', SMESH.NODE)

        # Stockage
        self.MESH['PROFIL']['MAIN'] = ProfilMesh
        self.MESH['CL']['F_EXT_PROFIL'] = CL_EXT_PROFIL_F
        self.MESH['CL']['N_EXT_PROFIL'] = CL_EXT_PROFIL_N

        return ProfilMesh

    def MakeWeldingAndProfilMesh(self):

        # Declaration des variables raccourci pour simplifier les expressions
        PROFIL = self.GEO['PROFIL']['MAIN']
        SOUDURE = self.GEO['SOUDURE']['MAIN']
        PROFIL_AND_SOUDURE = self.GEO['PROFIL_SOUDURE']
        PROFIL_VEDGES = self.GEO['PROFIL']['E_VERT']
        PROFIL_SECTION = self.GEO['PROFIL']['SECTION']
        SOUDURE_SECTION = self.GEO['SOUDURE']['SECTION']
        PROFIL_SECT_MESH = self.MESH['PROFIL']['SECTION']
        SOUDURE_SECT_MESH = self.MESH['SOUDURE']['SECTION']
        CL_EXT_PROFIL = self.GEO['CL']['EXT_PROFIL']
        POST_SOUD_VERT = self.GEO['SOUDURE']['F_PROFIL']
        POST_SOUD_HORI = self.GEO['SOUDURE']['F_PLATE']
        POST_SOUD_APOTH = self.GEO['SOUDURE']['F_APOTH']
        POST_SOUD_FISS = self.GEO['SOUDURE']['F_FISS']
        POST_SOUD_FOND_FISS = self.GEO['SOUDURE']['L_FOND_FISS']

        FINENESS = self.MESH_PARAM['PROFIL']['fineness']
        min_height = self.MESH_PARAM['PROFIL']['min_height']
        max_height = min(self.DIM_H / 4, self.DIM_V / 10)

        # Contenu
        SoudureProfilMesh = smesh.Mesh(self.GEO['PROFIL_SOUDURE'])
        smesh.SetName(SoudureProfilMesh.GetMesh(), '_SOUDURE_AND_PROFIL')

        # Importation des sous-maillage existant
        PROFIL_Proj1D2D = SoudureProfilMesh.Projection1D2D(geom=PROFIL_SECTION)
        PROFIL_Proj1D2D_H = PROFIL_Proj1D2D.SourceFace(PROFIL_SECTION,
                                                       PROFIL_SECT_MESH)
        smesh.SetName(PROFIL_Proj1D2D, 'PROFIL_Proj')
        smesh.SetName(PROFIL_Proj1D2D_H, 'PROFIL_Proj')
        smesh.SetName(PROFIL_Proj1D2D.GetSubMesh(), 'PROFIL_Proj')

        SOUDURE_Proj1D2D = SoudureProfilMesh.Projection1D2D(
            geom=SOUDURE_SECTION)
        SOUDURE_Proj1D2D_H = SOUDURE_Proj1D2D.SourceFace(SOUDURE_SECTION,
                                                         SOUDURE_SECT_MESH)
        smesh.SetName(SOUDURE_Proj1D2D, 'SOUDURE_Proj')
        smesh.SetName(SOUDURE_Proj1D2D_H, 'SOUDURE_Proj')
        smesh.SetName(SOUDURE_Proj1D2D.GetSubMesh(), 'SOUDURE_Proj')

        # Creation de sous-maillage
        ProfilHeight = SoudureProfilMesh.Segment(geom=PROFIL_VEDGES)
        ProfilHeight_H = ProfilHeight.StartEndLength(min_height, max_height)
        reversedEdges_ID = self.GetReversedVertEdgesIDOnProfil()
        ProfilHeight_H.SetReversedEdges(reversedEdges_ID)
        smesh.SetName(ProfilHeight, 'ProfilHeight')
        smesh.SetName(ProfilHeight_H, 'ProfilHeight')
        smesh.SetName(ProfilHeight.GetSubMesh(), 'ProfilHeight')

        # Taille automatique
        AutoLength1D = SoudureProfilMesh.Segment()
        AutoLength1D_H = AutoLength1D.AutomaticLength(FINENESS)
        smesh.SetName(AutoLength1D_H, 'AutoLengthPlate')

        SoudureProfilMesh.AddHypothesis(self.algo['Prism3D'])
        SoudureProfilMesh.AddHypothesis(self.algo['Quad2D'])
        SoudureProfilMesh.AddHypothesis(self.algo['Regular1D'])
        SoudureProfilMesh.Compute()

        # Creation des groupes de CL et post-traitement
        CL_EXT_PROFIL_F = SoudureProfilMesh.GroupOnGeom(CL_EXT_PROFIL,
                                                        'F_PR_EX', SMESH.FACE)
        CL_EXT_PROFIL_N = SoudureProfilMesh.GroupOnGeom(CL_EXT_PROFIL,
                                                        'N_PR_EX', SMESH.NODE)

        POST_SOUD_VERT_F = SoudureProfilMesh.GroupOnGeom(POST_SOUD_VERT,
                                                         'F_SO_V', SMESH.FACE)
        POST_SOUD_VERT_N = SoudureProfilMesh.GroupOnGeom(POST_SOUD_VERT,
                                                         'N_SO_V', SMESH.NODE)
        POST_SOUD_HORI_F = SoudureProfilMesh.GroupOnGeom(POST_SOUD_HORI,
                                                         'F_SO_H', SMESH.FACE)
        POST_SOUD_HORI_N = SoudureProfilMesh.GroupOnGeom(POST_SOUD_HORI,
                                                         'N_SO_H', SMESH.NODE)
        POST_SOUD_APOTH_F = SoudureProfilMesh.GroupOnGeom(POST_SOUD_APOTH,
                                                          'F_SOAPOT', SMESH.FACE)
        POST_SOUD_APOTH_N = SoudureProfilMesh.GroupOnGeom(POST_SOUD_APOTH,
                                                          'N_SOAPOT', SMESH.NODE)
        POST_SOUD_FISS_F = SoudureProfilMesh.GroupOnGeom(POST_SOUD_FISS,
                                                         'F_SOFISS', SMESH.FACE)
        POST_SOUD_FISS_N = SoudureProfilMesh.GroupOnGeom(POST_SOUD_FISS,
                                                         'N_SOFISS', SMESH.NODE)
        POST_SOUD_FOND_FISS_L = SoudureProfilMesh.GroupOnGeom(POST_SOUD_FOND_FISS,
                                                              'L_SOFOND', SMESH.EDGE)
        POST_SOUD_FOND_FISS_N = SoudureProfilMesh.GroupOnGeom(POST_SOUD_FOND_FISS,
                                                              'N_SOFOND', SMESH.NODE)

        SoudureProfilMesh.GroupOnGeom(PROFIL, 'PROFIL', SMESH.VOLUME)
        SoudureProfilMesh.GroupOnGeom(SOUDURE, 'SOUDURE', SMESH.VOLUME)
        SoudureProfilMesh.GroupOnGeom(PROFIL_AND_SOUDURE, 'PROFIL_AND_SOUDURE', SMESH.VOLUME)

        # Stockage
        self.MESH['PROFIL_AND_SOUDURE'] = SoudureProfilMesh
        self.MESH['CL']['F_EXT_PROFIL'] = CL_EXT_PROFIL_F
        self.MESH['CL']['N_EXT_PROFIL'] = CL_EXT_PROFIL_N
        self.MESH['POST']['F_SOUD_VERT'] = POST_SOUD_VERT_F
        self.MESH['POST']['N_SOUD_VERT'] = POST_SOUD_VERT_N
        self.MESH['POST']['F_SOUD_HORI'] = POST_SOUD_HORI_F
        self.MESH['POST']['N_SOUD_HORI'] = POST_SOUD_HORI_N
        self.MESH['POST']['F_APOTH'] = POST_SOUD_APOTH_F
        self.MESH['POST']['N_APOTH'] = POST_SOUD_APOTH_N
        self.MESH['POST']['F_FISS'] = POST_SOUD_FISS_F
        self.MESH['POST']['N_FISS'] = POST_SOUD_FISS_N
        self.MESH['POST']['N_FOND_FISS'] = POST_SOUD_FOND_FISS_N
        self.MESH['POST']['L_FOND_FISS'] = POST_SOUD_FOND_FISS_L

        return SoudureProfilMesh

    def MakePlatePlaneMesh(self):

        # Declaration des variables raccourci pour simplifier les expressions
        F_PLANE = self.GEO['PLATE']['F_PLANE']
        F_CONTACT = self.GEO['PLATE']['F_CONTACT']
        PlateEP = self.GEO_PARAM['PLATE']['ep']
        if self.welding:
            SoudureProfilMesh = self.MESH['PROFIL_AND_SOUDURE']
        else:
            ProfilSectionMesh = self.MESH['PROFIL']['SECTION']

        # Contenu
        PlatePlaneMesh = smesh.Mesh(F_PLANE)
        smesh.SetName(PlatePlaneMesh.GetMesh(), '_PLATE_PLANE')

        # Importation des sous-maillage existant
        CONTACT_Proj1D2D = PlatePlaneMesh.Projection1D2D(geom=F_CONTACT)
        if self.welding:
            CONTACT_Proj1D2D_H = CONTACT_Proj1D2D.SourceFace(F_CONTACT,
                                                             SoudureProfilMesh)
        else:
            CONTACT_Proj1D2D_H = CONTACT_Proj1D2D.SourceFace(F_CONTACT,
                                                             ProfilSectionMesh)

        smesh.SetName(CONTACT_Proj1D2D, 'CONTACT_Proj')
        smesh.SetName(CONTACT_Proj1D2D_H, 'CONTACT_Proj')
        smesh.SetName(CONTACT_Proj1D2D.GetSubMesh(), 'CONTACT_Proj')

        # Hypotheses
        NETGEN_2D = smesh.CreateHypothesis('NETGEN_2D', 'NETGENEngine')
        NETGEN_2D_H = smesh.CreateHypothesis('NETGEN_Parameters_2D',
                                             'NETGENEngine')
        NETGEN_2D_H.SetSecondOrder(0)
        NETGEN_2D_H.SetOptimize(1)
        NETGEN_2D_H.SetFineness(2)
        NETGEN_2D_H.SetFuseEdges(1)
        NETGEN_2D_H.SetQuadAllowed(1)
        NETGEN_2D_H.SetMaxSize(PlateEP)
        NETGEN_2D_H.SetMinSize(PlateEP / 5)
        NETGEN_2D_H.SetUseSurfaceCurvature(0)

        PlatePlaneMesh.AddHypothesis(NETGEN_2D)
        PlatePlaneMesh.AddHypothesis(NETGEN_2D_H)
        PlatePlaneMesh.Compute()

        # Stockage
        self.MESH['PLATE']['PLANE'] = PlatePlaneMesh

        return PlatePlaneMesh

    def MakePlateMesh(self):
        PLATE_VEDGES = self.GEO['PLATE']['E_VERT']
        PLATE = self.GEO['PLATE']['MAIN']
        F_PLANE = self.GEO['PLATE']['F_PLANE']
        PlatePlaneMesh = self.MESH['PLATE']['PLANE']
        LAYER_PLATE = self.MESH_PARAM['PLATE']['layer']
        CL_BASE_PLATE = self.GEO['CL']['BASE_PLATE']

        # Contenu
        PlateMesh = smesh.Mesh(PLATE)
        smesh.SetName(PlateMesh.GetMesh(), '_PLATE')

        # Importation des sous-maillage existant
        PLANE_Proj1D2D = PlateMesh.Projection1D2D(geom=F_PLANE)
        PLANE_Proj1D2D_H = PLANE_Proj1D2D.SourceFace(F_PLANE,
                                                     PlatePlaneMesh)
        smesh.SetName(PLANE_Proj1D2D, 'PLATE_PLANE_Proj')
        smesh.SetName(PLANE_Proj1D2D_H, 'PLATE_PLANE_Proj')
        smesh.SetName(PLANE_Proj1D2D.GetSubMesh(), 'PLATE_PLANE_Proj')

        # Hypotheses
        NbLayerPlate = PlateMesh.Segment(geom=PLATE_VEDGES)
        NbLayerPlate_H = NbLayerPlate.NumberOfSegments(LAYER_PLATE)
        NbLayerPlate_H.SetDistrType(0)
        smesh.SetName(NbLayerPlate_H, 'NbLayerPlate')

        PlateMesh.AddHypothesis(self.algo['Prism3D'])
        PlateMesh.AddHypothesis(self.algo['Quad2D'])
        PlateMesh.Compute()

        # Creation des groupes
        CL_BASE_PLATE_F = PlateMesh.GroupOnGeom(CL_BASE_PLATE, 'F_PL_BAS',
                                                SMESH.FACE)
        CL_BASE_PLATE_N = PlateMesh.GroupOnGeom(CL_BASE_PLATE, 'N_PL_BAS',
                                                SMESH.NODE)
        PlateMesh.GroupOnGeom(PLATE, 'PLATE', SMESH.VOLUME)

        # Stockage
        self.MESH['PLATE']['MAIN'] = PlateMesh
        self.MESH['CL']['F_BASE_PLATE'] = CL_BASE_PLATE_F
        self.MESH['CL']['N_BASE_PLATE'] = CL_BASE_PLATE_N

        return PlatePlaneMesh

    def MakeAssemblageMesh(self):

        # Contenu
        PlateMesh = self.MESH['PLATE']['MAIN']

        if self.welding:
            SoudureProfilMesh = self.MESH['PROFIL_AND_SOUDURE']
            l_meshes = [SoudureProfilMesh.GetMesh(), PlateMesh.GetMesh()]
        else:
            ProfilMesh = self.MESH['PROFIL']['MAIN']
            l_meshes = [ProfilMesh.GetMesh(), PlateMesh.GetMesh()]

        AssemblageMesh = smesh.Concatenate(l_meshes, 1, 1, 1e-005)
        smesh.SetName(AssemblageMesh.GetMesh(), self.GetName())

        # Creation du noeud a lextremite du profil pour imposition torseur
        Px, Py, Pz = gp.PointCoordinates(self.GEO['PROFIL']['V_EXT'])
        nodeID = AssemblageMesh.AddNode(Px, Py, Pz)
        N_PR_CEX = AssemblageMesh.CreateEmptyGroup(SMESH.NODE, 'N_PR_CEX')
        N_PR_CEX.Add([nodeID])

        PR_EX_0D_ID = AssemblageMesh.Add0DElement(nodeID)
        PR_EX_0D = AssemblageMesh.CreateEmptyGroup(SMESH.ELEM0D, '0D_PR_EX')
        PR_EX_0D.Add([PR_EX_0D_ID])

        # Stockage
        self.MESH['MAIN'] = AssemblageMesh
        self.MESH['CL']['N_EXT_CENTER_PROFIL'] = N_PR_CEX

        return AssemblageMesh

    def ExportMesh(self, path):
        try:
            return self.MESH['MAIN'].ExportMED(path, False, SMESH.MED_V2_2, True, None, False)
        except:
            print("Erreur dans l'exportation du maillage")
            return True

# ***************************************************************************
# ***************************************************************************
# ***************************************************************************

# ***************************************************************************
# ******************************** CARC *************************************
# ***************************************************************************


class ROND(CantileverBeam3D):

    def GetName(self):
        # Declaration des variables raccourci pour simplifier les expressions
        R = self.GEO_PARAM['PROFIL']['R']
        ep = self.GEO_PARAM['PROFIL']['ep']
        return 'ROND %sx%s' % (R, ep)

    def AutoParameters(self):
        ep = self.GEO_PARAM['PROFIL']['ep']
        layer = self.MESH_PARAM['PROFIL']['layer']
        self.MESH_PARAM['PROFIL']['min_height'] = ep / layer

    def MakeProfilSection(self, addToStudy=False):
        # Declaration des variables raccourci pour simplifier les expressions
        R = self.GEO_PARAM['PROFIL']['R']
        ep = self.GEO_PARAM['PROFIL']['ep']

        # Creation des points
        V_CENTER = gp.MakeVertexWithRef(self.O, 0, 0, 0)
        V_HAUT = gp.MakeVertexWithRef(self.O, 0, R / 2, 0)
        V_HAUT_INT = gp.MakeVertexWithRef(self.O, 0, R / 2 - ep, 0)
        V_DROIT = gp.MakeVertexWithRef(self.O, R / 2, 0, 0)
        V_DROIT_INT = gp.MakeVertexWithRef(self.O, R / 2 - ep, 0, 0)
        V_GAUCHE = gp.MakeVertexWithRef(self.O, -R / 2, 0, 0)

        # Creation vecteur
        OX_NEG = gp.MakeVectorDXDYDZ(-1, 0, 0)

        # Creation des contours
        CIRCLE_EXT = gp.MakeCircleCenter2Pnt(V_CENTER, V_DROIT, V_HAUT)
        CIRCLE_INT = gp.MakeCircleCenter2Pnt(V_CENTER, V_DROIT_INT, V_HAUT_INT)

        # Creation de la surface
        SECTION = gp.MakeFaceWires([CIRCLE_EXT, CIRCLE_INT], 1)

        # Stockage
        self.GEO['PROFIL']['SECTION'] = SECTION
        self.GEO['PROFIL']['CONTOUR_EXT'] = CIRCLE_EXT
        self.GEO['SOUDURE']['START_POINT'] = V_DROIT
        self.GEO['SOUDURE']['PLATE_VEC'] = self.OX

        # Impression
        if addToStudy:
            AddToStudyCurrentObjects(locals(), FOLDER="ProfilSection",
                                     inDIR=self.DIR)

        return SECTION, CIRCLE_EXT, V_DROIT, self.OX

    def GetProfilPartitionPlanes(self, addToStudy=True):

        PartitionPlanes = super(ROND, self).GetProfilPartitionPlanes()

        # Declaration des variables raccourci pour simplifier les expressions
        R = self.GEO_PARAM['PROFIL']['R']
        ep = self.GEO_PARAM['PROFIL']['ep']

        # Variables dependantes
        self.GetMaxDIM()

        # Contenu
        PLANE_X = gp.MakePlane(self.O, self.OX, self.DIM * 2.1)
        PLANE_Y = gp.MakePlane(self.O, self.OY, self.DIM * 2.1)

        PartitionPlanes += [PLANE_X, PLANE_Y]

        # Impression
        if addToStudy:
            AddToStudyCurrentObjects(locals(), FOLDER="PARTITION_TOOLS",
                                     inDIR=self.DIR)

        return PartitionPlanes

    def GetReversedVertEdgesIDOnProfil(self):
        VEDGES = self.GEO['PROFIL']['E_VERT']
        PROFIL_SOUDURE = self.GEO['PROFIL_SOUDURE']
        EDGE_TYPE = gp.ShapeType["EDGE"]
        R = self.GEO_PARAM['PROFIL']['R']
        ep = self.GEO_PARAM['PROFIL']['ep']

        E1 = CreateGroupFromCDG(VEDGES, PROFIL_SOUDURE, EDGE_TYPE,
                                X=-R / 2, CS=self.REPERE_PROFIL)
        E1 = E1.GetSubShapeIndices()
        E2 = CreateGroupFromCDG(VEDGES, PROFIL_SOUDURE, EDGE_TYPE,
                                X=-R / 2 + ep, CS=self.REPERE_PROFIL)
        E2 = E2.GetSubShapeIndices()
        E3 = CreateGroupFromCDG(VEDGES, PROFIL_SOUDURE, EDGE_TYPE,
                                Y=-R / 2, CS=self.REPERE_PROFIL)
        E3 = E3.GetSubShapeIndices()
        E4 = CreateGroupFromCDG(VEDGES, PROFIL_SOUDURE, EDGE_TYPE,
                                Y=-R / 2 + ep, CS=self.REPERE_PROFIL)
        E4 = E4.GetSubShapeIndices()
        E5 = CreateGroupFromCDG(VEDGES, PROFIL_SOUDURE, EDGE_TYPE,
                                Y=R / 2, CS=self.REPERE_PROFIL)
        E5 = E5.GetSubShapeIndices()
        E6 = CreateGroupFromCDG(VEDGES, PROFIL_SOUDURE, EDGE_TYPE,
                                Y=R / 2 - ep, CS=self.REPERE_PROFIL)
        E6 = E6.GetSubShapeIndices()

        ReversedVertEdgesIDOnProfil = E1 + E2 + E3 + E4 + E5 + E6

        return ReversedVertEdgesIDOnProfil


class CARC(CantileverBeam3D):

    def GetName(self):
        # Declaration des variables raccourci pour simplifier les expressions
        L = self.GEO_PARAM['PROFIL']['L']
        ep = self.GEO_PARAM['PROFIL']['ep']
        return 'CARC %sx%s' % (L, ep)

    def AutoParameters(self):
        ep = self.GEO_PARAM['PROFIL']['ep']
        layer = self.MESH_PARAM['PROFIL']['layer']
        self.MESH_PARAM['PROFIL']['min_height'] = ep / layer

    def MakeProfilSection(self, addToStudy=False):
        # Declaration des variables raccourci pour simplifier les expressions
        L = self.GEO_PARAM['PROFIL']['L']
        ep = self.GEO_PARAM['PROFIL']['ep']

        # Variables dependantes
        LpartPos = L / 2 - 2 * ep
        LpartNeg = -L / 2 + 2 * ep

        # Creation des points centre des conges
        HDC = gp.MakeVertexWithRef(self.O, LpartPos, LpartPos, 0)
        BDC = gp.MakeVertexWithRef(self.O, LpartPos, LpartNeg, 0)
        BGC = gp.MakeVertexWithRef(self.O, LpartNeg, LpartNeg, 0)
        HGC = gp.MakeVertexWithRef(self.O, LpartNeg, LpartPos, 0)

        # Creation des points extremite du conge Haut Droit
        HDD1 = gp.MakeVertexWithRef(self.O, L / 2 - ep, LpartPos, 0)
        HDD2 = gp.MakeVertexWithRef(self.O, L / 2, LpartPos, 0)
        HDH1 = gp.MakeVertexWithRef(self.O, LpartPos, L / 2 - ep, 0)
        HDH2 = gp.MakeVertexWithRef(self.O, LpartPos, L / 2, 0)

        # Creation des points extremite du conge Bas Droit
        BDD1 = gp.MakeVertexWithRef(self.O, L / 2 - ep, LpartNeg, 0)
        BDD2 = gp.MakeVertexWithRef(self.O, L / 2, LpartNeg, 0)
        BDB1 = gp.MakeVertexWithRef(self.O, LpartPos, -L / 2 + ep, 0)
        BDB2 = gp.MakeVertexWithRef(self.O, LpartPos, -L / 2, 0)

        # Creation des points extremite du conge Bas Gauche
        BGB1 = gp.MakeVertexWithRef(self.O, LpartNeg, -L / 2 + ep, 0)
        BGB2 = gp.MakeVertexWithRef(self.O, LpartNeg, -L / 2, 0)
        BGG1 = gp.MakeVertexWithRef(self.O, -L / 2 + ep, LpartNeg, 0)
        BGG2 = gp.MakeVertexWithRef(self.O, -L / 2, LpartNeg, 0)

        # Creation des points extremite du conge Haut Gauche
        HGG1 = gp.MakeVertexWithRef(self.O, -L / 2 + ep, LpartPos, 0)
        HGG2 = gp.MakeVertexWithRef(self.O, -L / 2, LpartPos, 0)
        HGH1 = gp.MakeVertexWithRef(self.O, LpartNeg, L / 2 - ep, 0)
        HGH2 = gp.MakeVertexWithRef(self.O, LpartNeg, L / 2, 0)

        # Creation des lignes interieures
        LH1 = gp.MakeLineTwoPnt(HDH1, HGH1)
        LG1 = gp.MakeLineTwoPnt(HGG1, BGG1)
        LB1 = gp.MakeLineTwoPnt(BGB1, BDB1)
        LD1 = gp.MakeLineTwoPnt(BDD1, HDD1)

        # Creation des lignes interieures
        LH2 = gp.MakeLineTwoPnt(HDH2, HGH2)
        LG2 = gp.MakeLineTwoPnt(HGG2, BGG2)
        LB2 = gp.MakeLineTwoPnt(BGB2, BDB2)
        LD2 = gp.MakeLineTwoPnt(BDD2, HDD2)

        # Creation des lignes des conges interieures
        CHD1 = gp.MakeArcCenter(HDC, HDD1, HDH1, False)
        CHG1 = gp.MakeArcCenter(HGC, HGH1, HGG1, False)
        CBG1 = gp.MakeArcCenter(BGC, BGG1, BGB1, False)
        CBD1 = gp.MakeArcCenter(BDC, BDB1, BDD1, False)

        # Creation des lignes des conges exterieures
        CHD2 = gp.MakeArcCenter(HDC, HDD2, HDH2, False)
        CHG2 = gp.MakeArcCenter(HGC, HGH2, HGG2, False)
        CBG2 = gp.MakeArcCenter(BGC, BGG2, BGB2, False)
        CBD2 = gp.MakeArcCenter(BDC, BDB2, BDD2, False)

        # Creation des contours
        CONTOUR_EXT = gp.MakeWire(
            [LH2, LG2, LB2, LD2, CHD2, CHG2, CBG2, CBD2], 1e-007)
        CONTOUR_INT = gp.MakeWire(
            [LH1, LG1, LB1, LD1, CHD1, CHG1, CBG1, CBD1], 1e-007)

        # Creation de la surface
        SECTION = gp.MakeFaceWires([CONTOUR_EXT, CONTOUR_INT], 1)

        # Stockage
        self.GEO['PROFIL']['SECTION'] = SECTION
        self.GEO['PROFIL']['CONTOUR_EXT'] = CONTOUR_EXT
        self.GEO['SOUDURE']['START_POINT'] = HDD2
        self.GEO['SOUDURE']['PLATE_VEC'] = self.OX

        # Impression
        if addToStudy:
            AddToStudyCurrentObjects(locals(), FOLDER="ProfilSection",
                                     inDIR=self.DIR)

        return SECTION, CONTOUR_EXT, HDD2, self.OX

    def GetProfilPartitionPlanes(self, addToStudy=False):

        PartitionPlanes = super(CARC, self).GetProfilPartitionPlanes()

        # Declaration des variables raccourci pour simplifier les expressions
        L = self.GEO_PARAM['PROFIL']['L']
        ep = self.GEO_PARAM['PROFIL']['ep']

        # Variables dependantes
        self.GetMaxDIM()
        Lpart = L / 2 - 2 * ep

        # Contenu
        PLANE_X = gp.MakePlane(self.O, self.OX, self.DIM * 2.1)
        PLANE_Y = gp.MakePlane(self.O, self.OY, self.DIM * 2.1)
        PLANE_XP = gp.MakeTranslationVectorDistance(PLANE_X, self.OX, Lpart)
        PLANE_XN = gp.MakeTranslationVectorDistance(PLANE_X, self.OX, -Lpart)
        PLANE_YP = gp.MakeTranslationVectorDistance(PLANE_Y, self.OY, Lpart)
        PLANE_YN = gp.MakeTranslationVectorDistance(PLANE_Y, self.OY, -Lpart)

        PartitionPlanes += [PLANE_XP, PLANE_XN, PLANE_YP, PLANE_YN]

        # Impression
        if addToStudy:
            AddToStudyCurrentObjects(locals(), FOLDER="PARTITION_TOOLS",
                                     inDIR=self.DIR)

        return PartitionPlanes

# ***************************************************************************
# ***************************************************************************
# ***************************************************************************

# ***************************************************************************
# ******************************* MAIN **************************************
# ***************************************************************************

if __name__ == "__main__":

    GEO_PARAM = {
        'SOUDURE': {
            'a': {{a_soudure}},
        },
        'PROFIL': {
            'L': {{L}},
            'ep': {{ep_profil}},
            'H': {{H}},
        },
        'PLATE': {
            'Lx': {{Lx_plate}},
            'Ly': {{Ly_plate}},
            'ep': {{ep_plate}},
        }
    }
    MESH_PARAM = {
        'PROFIL': {
            'layer': {{nb_elem_ep_profil}},
            'fineness': 0.3,
        },
        'SOUDURE': {
            'pfiss': {{a_soudure}} * 2**0.5 / 2,
        },
        'PLATE': {
            'layer': {{nb_elem_ep_plate}},
        }
    }

    # Geometrie

    ALPHA = {{alpha}}
    O = gp.MakeVertex({{P_X}}, {{P_Y}}, {{P_Z}})
    OX = gp.MakeVectorDXDYDZ(cos(radians(ALPHA)), sin(radians(ALPHA)), 0)
    OY = gp.MakeVectorDXDYDZ(-sin(radians(ALPHA)), cos(radians(ALPHA)), 0)
    OZ = gp.MakeVectorDXDYDZ(0, 0, 1)

    REPERE_PROFIL = gp.MakeMarkerPntTwoVec(O, OX, OY)
    gp.addToStudy(REPERE_PROFIL, "REPERE_PROFIL")

    Tube = CARC(REPERE_PROFIL, GEO_PARAM, MESH_PARAM, welding=True)
    Tube.MakeGeometry(addToStudy=False)
    Tube.MakeMesh()
    Tube.ExportMesh("{{mesh_file}}")

    if salome.sg.hasDesktop():
        salome.sg.updateObjBrowser(1)

    # ***************************************************************************
    # ***************************************************************************
    # ***************************************************************************
